//
//  BaseClassVC.swift
//  ESound
//
//  Created by kirti on 17/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class BaseClassVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 11.0, *) {
          //  self.navigationItem.largeTitleDisplayMode = .always
            self.navigationController?.navigationBar.prefersLargeTitles = true
        }
    }

  

}
