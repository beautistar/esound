//
//  Constant.swift
//  ESound
//
//  Created by kirti on 13/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import Foundation
import UIKit

let objAppdelegate = UIApplication.shared.delegate as! AppDelegate
let AppDelegateGet = AppDelegate.getdefaultvalue()

let kDownloadFolderName = "Songs"
let kFileExtension = ".mp4"

let isLogged = "isLogged"
let kUserid = "kUserid"
let kisExplictOff = "kisExplictOff"
let kYoutubeKey = "kYoutubeKey"
let kYoutubeKeyLoaded = "kYoutubeKeyLoaded"


let RobotoRegular = "Roboto-Regular"
let RobotoMedium = "Roboto-Medium"
let RobotoBold  = "Roboto-Bold"
let FontSizeNormal = CGFloat(17)
let FontSizeSmall = CGFloat(14)
let FontSizeBig = CGFloat(19)

let colorGreen = "146150"
let colorGray = "808080"

let kforgotPassword =  "https://esound.app/recovery"
let ksupport = "http://m.me/esoundapp/"
let kterms = "https://esound.app/termsAndConditions"
let kprivacy =  "https://esound.app/privacy"
let ksocialIG =  "https://www.instagram.com/esound_music/"
let ksocialFB = "https://www.facebook.com/esoundapp/"
let ksocialTW = "https://twitter.com/esound_music"
let kstoreLink = "http://onelink.to/mwjyrc"
let kwebsite =  "https://esound.app"
let kfullVersion = "https://esound.app/fullVersion"


let kHostPath = "https://development.esound.app/api/"
//let kHostPath = "https://esound.app/api/"
struct kWebApi {
    static let klogin = "user/Login"
    static let kregsiter = "user/Register"
    
    static let kCreatePlaylist = "music/CreatePlaylist"
    static let kRenamePlaylist = "music/ChangePlaylistMetadata/"

    static let kDeletePlaylist = "music/DeletePlaylist"
    static let kUserPlaylist = "music/UserPlaylists/"
    static let kDeezerPlaylist = "music/DeezerPlaylists"
    static let kReportSongs = "music/ReportSong"
    static let kDailyMixTopNSongs = "music/TopNSongsFromUser/"
    static let kCheckDeezerTrackExistUniversal = "music/CheckDeezerTrackExistsUniversal/"
    static let ksaveTrackRequest = "music/SaveTrack"
    static let ksaveTrackUserRequest = "music/SaveTrackUser"
    static let kinsertMultipleinPlaylist = "music/InsertTrackPlaylists"
    static let kinsertSingleinPlaylist = "music/InsertTracksPlaylist"
    static let kRenameSonag = " music/AddSongRename/"
    static let kSavePlayListOffline = "music/SavePlaylistOffline/"
    static let kRemoveSongFromPlaylist = "music/DeletePlaylistSong/"
    static let kYoutubeAPIKey = "music/CurrentYTApiKeyNew"
    static let kUpdateYoutubeAPIKey = "music/UpdateCurrentYTApiKeyNew"
    static let kDeleteLibrarySong = "music/DeleteLibrarySong"
    static let kSaveTrackOffline = "music/SaveTrackOffline"
    
    static let kChangeTrackPosition = "music/ChangeTrackPosition"
    
    
    
   
 
    static let kUserToggleExplicit = "user/ToggleExplicit"
    
    
   // /api/user/ToggleExplicit/{userID}/{off}
    
  
    
}



struct kFAQ {
   
    static let faqIntro = "Below you can find answers/solutions to many common questions/problems.\nIt is very likely that there is also the answer to your problem.\nIf you don\'t find it yet, you are free to contact us using the button at the bottom."
    static let faqFull = "Frequently asked questions"
    static let faqSection1 = "Downloads"
    static let faqSection2 = "Streaming"
    static let faqSection3 = "Search"
    static let faqSection4 = "Library"
    static let faqSection5 = "Premium"
    static let faq1 = "Can I download the songs to listen to them offline?"
    static let faq1c = "Unfortunately, we cannot allow to download songs due to YouTube and Apple terms of service."
    static let faq2 = "I can’t find a song, what can I do?"
    static let faq2c = "You can try to switch to Youtube search by tapping the button at the bottom of the search screen.\nIf you can’t find it even in this way, copy the link of the song from Youtube and paste it in the search bar."
    static let faq3 = "I can’t listen to a song or the app is playing the wrong song."
    static let faq3c = "You can report the song by holding on it (or tapping the three dots near the song) and clicking on “Report a problem”.\nIn this way, we’ll fix the song on daily bases.\nIf a song is wrong even after 48 hours, contact us on the support and specify the song which gives you problems.\nMeanwhile, to listen the song immediately, copy the link of the song from Youtube and paste it in the search bar."
    static let faq4 = "The Youtube search isn’t working, what can I do?'"
    static let faq4c = "Sometimes, especially during certain time windows, due to the huge load on our servers, Youtube search is not available.\nPlease be patient, the problem will be solved automatically after some hours."
    static let faq5 = "I bought the Premium but I still see advertisements, what should I do?"
    static let faq5c = "Try to click “Restore purchases” in the settings of the app (reachable by tapping the user icon on the top right corner).\nIf the problem persists, contact us on the support."
    static let faq6 = "If I reinstall the app, will I lose my songs/playlists?"
    static let faq6c = "No, songs’ metadata are saved in cloud and are associated with your account.\nWhen you login in the app again, it will synchronize your data automatically, without losing anything."
    static let faq7 = "Are there limits in the library?"
    static let faq7c = "No. At the moment you can save and listen to unlimited songs, and have unlimited playlists."
    static let faq8 = "What does the clock icon (near the songs) means?"
    static let faq8c = "It means that the song is waiting to be synchronized offline.\nIn the meanwhile, you can listen to it using internet connection."
    static let faq9 = "Does eSound use my internet data?"
    static let faq9c = "eSound will use internet data when playing songs on a cellular connection.\nIf you are not connected to WiFi and are using eSound, your home data will be used.\neSound uses 10-15 mb of data per song played, on average."
    static let faq10 = "My songs keep cutting out and lagging, what is the problem?"
    static let faq10c = "eSound requires a fairly strong internet connection to stream songs without slowing down or lagging.\nIf you are using eSound on a slower WiFi or cellular connection, this may be why the app is slowing down.\nTry changing the connection to something stronger and play your music again."
    static let faq11 = "How can I transfer my eSound to a new device?"
    
    static let faq11c = "In order to move all your library, songs and playlists, you simply have to login with the same account on the new device."
    static let faq12 = "Can I export my songs or playlists to another app?"
    static let faq12c = "No, it is not allowed to export or distribute songs outside eSound."
    
  
}

struct kProfile {
    static let enableExplicit = "Enable explicit content"
    static let disableExplicit = "Disable explicit content"
    static let enableVolumeSlider =  "Enable volume slider"
    static let disableVolumeSlider =  "Disable volume slider"
    static let disableOptimizations =  "Disable optimization"
    static let enableOptimizations = "Enable optimization"
    static let toggleExplicitTitle =  "Enable explicit content?"
    static let welcomeback = "Welcome back"
    static let toggledarktheme1 = "Toggle"
    static let toggledarktheme2 = "dark"
    static let toggledarktheme3 = "light"
    static let toggledarktheme4 = "theme"
    static let reloadandsynclib = "Reload and sync library"
    static let reviewapp = "Review the app"
    static let facebookpage = "Facebook page"
    static let instagrampage = "Instagram page"
    static let twitterpage = "Twitter page"
    static let contactdevs = "Contact the developers"
    static let contactus = "Contact us"
    static let toggleExplicitDesc = "In this way, you may see content that is not suitable for you or may impress you."
    static let reloadDescription = "We will reload your library by resetting it and download it again.This will sync all your songs and fix any error."
}

let regionDict = [
  "global": "https://esound.app/images/charts/viralGlobal.png",
  "ar": "https://esound.app/images/charts/viralAr.png",
  "at": "https://esound.app/images/charts/viralAt.png",
  "au": "https://esound.app/images/charts/viralAu.png",
  "be": "https://esound.app/images/charts/viralBe.png",
  "bg": "https://esound.app/images/charts/viralBg.png",
  "bh": "https://esound.app/images/charts/viralBh.png",
  "br": "https://esound.app/images/charts/viralBr.png",
  "by": "https://esound.app/images/charts/viralBy.png",
  "ca": "https://esound.app/images/charts/viralCa.png",
  "ch": "https://esound.app/images/charts/viralCh.png",
  "cl": "https://esound.app/images/charts/viralCl.png",
  "co": "https://esound.app/images/charts/viralCo.png",
  "cz": "https://esound.app/images/charts/viralCz.png",
  "de": "https://esound.app/images/charts/viralDe.png",
  "dk": "https://esound.app/images/charts/viralDk.png",
  "es": "https://esound.app/images/charts/viralEs.png",
  "fi": "https://esound.app/images/charts/viralFi.png",
  "fr": "https://esound.app/images/charts/viralFr.png",
  "gb": "https://esound.app/images/charts/viralGb.png",
  "gr": "https://esound.app/images/charts/viralGr.png",
  "hn": "https://esound.app/images/charts/viralHn.png",
  "ie": "https://esound.app/images/charts/viralIe.png",
  "il": "https://esound.app/images/charts/viralIl.png",
  "in": "https://esound.app/images/charts/viralIn.png",
  "is": "https://esound.app/images/charts/viralIs.png",
  "it": "https://esound.app/images/charts/viralIt.png",
  "jp": "https://esound.app/images/charts/viralJp.png",
  "lu": "https://esound.app/images/charts/viralLu.png",
  "ma": "https://esound.app/images/charts/viralMa.png",
  "mx": "https://esound.app/images/charts/viralMx.png",
  "nl": "https://esound.app/images/charts/viralNl.png",
  "no": "https://esound.app/images/charts/viralNo.png",
  "nz": "https://esound.app/images/charts/viralNz.png",
  "pe": "https://esound.app/images/charts/viralPe.png",
  "pl": "https://esound.app/images/charts/viralPl.png",
  "pt": "https://esound.app/images/charts/viralPt.png",
  "ro": "https://esound.app/images/charts/viralRo.png",
  "rs": "https://esound.app/images/charts/viralRs.png",
  "ru": "https://esound.app/images/charts/viralRu.png",
  "se": "https://esound.app/images/charts/viralSe.png",
  "si": "https://esound.app/images/charts/viralSi.png",
  "sk": "https://esound.app/images/charts/viralSk.png",
  "tr": "https://esound.app/images/charts/viralTr.png",
  "ua": "https://esound.app/images/charts/viralUa.png",
  "uy": "https://esound.app/images/charts/viralUy.png",
  "za": "https://esound.app/images/charts/viralZa.png",
  "ae": "https://esound.app/images/charts/viralAe.png",
  "bo": "https://esound.app/images/charts/viralBo.png",
  "cr": "https://esound.app/images/charts/viralCr.png",
  "dz": "https://esound.app/images/charts/viralDz.png",
  "ec": "https://esound.app/images/charts/viralEc.png",
  "ee": "https://esound.app/images/charts/viralEe.png",
  "eg": "https://esound.app/images/charts/viralEg.png",
  "gt": "https://esound.app/images/charts/viralGt.png",
  "hr": "https://esound.app/images/charts/viralHr.png",
  "hu": "https://esound.app/images/charts/viralHu.png",
  "id": "https://esound.app/images/charts/viralId.png",
  "jm": "https://esound.app/images/charts/viralJm.png",
  "jo": "https://esound.app/images/charts/viralJo.png",
  "kr": "https://esound.app/images/charts/viralKr.png",
  "lb": "https://esound.app/images/charts/viralLb.png",
  "lt": "https://esound.app/images/charts/viralLt.png",
  "lv": "https://esound.app/images/charts/viralLv.png",
  "my": "https://esound.app/images/charts/viralMy.png",
  "ph": "https://esound.app/images/charts/viralPh.png",
  "py": "https://esound.app/images/charts/viralPy.png",
  "sa": "https://esound.app/images/charts/viralSa.png",
  "sn": "https://esound.app/images/charts/viralSn.png",
  "sv": "https://esound.app/images/charts/viralSv.png",
  "th": "https://esound.app/images/charts/viralTh.png",
  "tn": "https://esound.app/images/charts/viralTn.png",
  "us": "https://esound.app/images/charts/viralUs.png",
  "ve": "https://esound.app/images/charts/viralVe.png",
]
struct kConstnatLbl {
    static let kalloverworld = "All the world"
    static let ktopviral =  "Top viral songs from all the world"
    static let ktopviralfrom = "Top viral songs from"
    static let kglobalrankings = "Global rankings"
    static let krankings = "Rankings"
    
    static let dm1 = "A playlist based on your saved music and the top from all the world"
    static let dm2 = "We have mixed songs from your library and from your country"
    static let dm3 = "A beautiful mix based on your songs and something suggested by us"
}

/* regions */
let regionName = ["global": "All the world",
  "ar": "Argentina",
  "at": "Austria",
  "au": "Australia",
  "be": "Belgium",
  "bg": "Bulgaria",
  "bh": "Bahrain",
  "br": "Brazil",
  "by": "Belarus",
  "ca": "Canada",
  "ch": "Switzerland",
  "cl": "Chile",
  "co": "Colombia",
  "cz": "Czechia",
  "de": "Germany",
  "dk": "Denmark",
  "es": "Spain",
  "fi": "Finland",
  "fr": "France",
  "gb": "United Kingdom",
  "gr": "Greece",
  "hn": "Honduras",
  "ie": "Ireland",
  "il": "Israel",
  "in": "India",
  "is": "Iceland",
  "it": "Italy",
  "jp": "Japan",
  "lu": "Luxemburg",
  "ma": "Morocco",
  "mx": "Mexico",
  "nl": "Netherlands",
  "no": "Norway",
  "nz": "New Zealand",
  "pe": "Peru",
  "pt": "Portugal",
  "ro": "Romania",
  "rs": "Serbia",
  "ru": "Russian Federation",
  "se": "Sweden",
  "si": "Slovenia",
  "sk": "Slovakia",
  "tr": "Turkey",
  "ua": "Ukraine",
  "uy": "Uruguay",
  "za": "South Africa",
  "ae": "United Arab Emirates",
  "bo": "Bolivia",
  "cr": "Costa Rica",
  "dz": "Algeria",
  "ec": "Ecuador",
  "ee": "Estonia",
  "eg": "Egypt",
  "gt": "Guatemala",
  "hr": "Croatia",
  "hu": "Hungary",
  "id": "Indonesia",
  "jm": "Jamaica",
  "jo": "Jordan",
  "kr": "South Korea",
  "lb": "Lebanon",
  "lt": "Lithuania",
  "lv": "Latvia",
  "my": "Malaysia",
  "ph": "Philippines",
  "pl": "Poland",
  "py": "Paraguay",
  "sa": "Saudi Arabia",
  "sn": "Senegal",
  "sv": "El Salvador",
  "th": "Thailand",
  "tn": "Tunisia",
  "us": "USA",
  "ve": "Venezuela"
]
/* errors */
let errgeneric = "An error occurred"
let errLibraryGeneric = "An error has occurred. You can contact us to report the problem."
let errofflineLogin = "Please connect and try again"
let errTermsNotAccepted = "Terms and privacy not accepted"
let errAcceptTermsAndPrivacy = "Please accept our privacy and terms before registering."
let errgetFBError = "Cannot get your facebook user"
let errinsertFBError = "An error with facebook occurred"
let errmissingParams = "There was an error with the app system"
let errwrongCredentials = "Your credentials aren't correct"
let erremailAlreadyTaken = "Your email is alredy used"
let errinvalidEmailAddress = "Your email seems invalid"
let errinsertRegistrationError = "An error with your registration occurred"
let erremailSent = "We have sent an email to your address"
let errerrorSendingEmail = "The was an error sending the email"
let erraccountNotExists = "This account doesn't exist"
let errsessionExpired = "There was an error. Please log in again"
let errnotExistsAfterDownload = "There was an error with the app system"
let errerrorEmpty = "Something is missing!"
let errmissingParamsofflinePlayer = "To listen this song you must be connected. If you download it, it will be available also offline."
let errofflinePlayerNoDownloads = "To listen this song you must be connected to internet."
let errtrackNotAvailable = "The song is not available."
let errtrackNotAvailableDesc = "The track is not available, try to re-download it."
let errtrackNotAvailableDescNoDownloads = "The track is not available, try with a different one."
let errNoSpaceLeft = "There is no space left on the device to download the songs, please free some space and reopen the app."
let errofflinePlayer = "To listen this song, you must be connected. If you download it, it will be available also offline."
let errTrackWaitingDownload = "This song is waiting for be downloaded"
let trackWaitingDownload = "eSound still have to download and sync this song. Please wait until it's ready."
let errTrackTempNotAvailable = "This song is currently not available"
let errTrackTempNotAvailableDesc = "This song is currently not reproducible or downloadable. Probably the song does not exist in our records at the moment, but it could be added in the future. For more informations contact the developers."
let errTrackTempNotAvailableDescNoDownloads = "This song is currently not reproducible. Probably the song does not exist in our records at the moment, but it could be added in the future. For more informations contact the developers."
let errOfflineDownload = "You must be connected to download a song"
let errOfflineDownloadDesc = "When you are connected, you can download this song and listen to it offline."
let errChangedLibraryOffline = "To save songs, change your library or your playlists you need to login. Return online to complete this operation."
let syncNotification = "eSound is downloading your songs, keep the application in foreground if you want the operation to proceed."
let errOops = "Oops ... There was a problem"
let errOopsDesc = "There was an error communicating with the server, please try again later"
let errrackRemovedFromYt = "The video has been removed from YouTube by its creator or the streaming is not available on this platform, please remove it from the library and find a new one through the search screen."
let alreadyWorkingOnIt = "\nSERVER IS DOWN\n We are already aware of the problem and are working on it. It will be solved as soon as possible."
let erruserNotExists = "There is still no user with this email. If this is your first login, you must first register."
let errOfflinetitle = "You are offline"

