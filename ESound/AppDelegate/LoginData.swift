//
//  LoginData.swift
//  ESound
//
//  Created by kirti on 08/10/19.
//  Copyright © 2019 kirti. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let loginData = try? newJSONDecoder().decode(LoginData.self, from: jsonData)

import Foundation

// MARK: - LoginData
class LoginData {
    let status: Bool
    let data: DataClass

    init(status: Bool, data: DataClass) {
        self.status = status
        self.data = data
    }
}

// MARK: - DataClass
class DataClass {
    let user: User
    let token, refreshToken: String
    let cloudTokens: [Any?]

    init(user: User, token: String, refreshToken: String, cloudTokens: [Any?]) {
        self.user = user
        self.token = token
        self.refreshToken = refreshToken
        self.cloudTokens = cloudTokens
    }
}

// MARK: - User
class User {
    
    let id: Int
    let username, name, surname, email: String
    let premium, explicitOff: Bool

    init(id: Int, username: String, name: String, surname: String, email: String, premium: Bool, explicitOff: Bool) {
        self.id = id
        self.username = username
        self.name = name
        self.surname = surname
        self.email = email
        self.premium = premium
        self.explicitOff = explicitOff
    }
   
}
class PlayListModel {
    
    var title, url,subtitle: String?
    var isOffline: String = ""
    let id: String
    var arr_songs = [Music]()
    init(title: String?,subtitle: String?, url: String?, id: String, isOffline: String = "") {
        self.title = title ?? ""
        self.subtitle = subtitle ?? ""
        self.url = url ?? ""
        self.id = id
    }
}
//struct TrackListData {
//    let trackReference: Int?
//    let trackID, deezerTrackID, title: String?
//    let titleRename: NSNull?
//    let artwork: String?
//    let authorID, authorName: String?
//    let authorRenameName: NSNull?
//    let authorImageURL: String?
//    let isOffline: Int?
//    let addedDate, listenDate: String?
//    let listenTimes, trackUserID: Int?
//    let playlistPos: NSNull?
//    let type: Int?
//}


//class SongListModel {
//    let title, Coverurl,subtitle,playUrl, id: String?
//     var isLocal,isDownloading : Bool?
//    var musicAuthor :String?
//     var musicAlbum :String?
//     var musicNum:Int?
//     var isActive:Bool=false
//    init(title:String?,subtitle:String?,Coverurl: String?,playUrl:String?,id: String,isLocal:Bool = false,isDownloading:Bool = false) {
//        self.title = title ?? ""
//        self.subtitle = subtitle ?? ""
//        self.Coverurl = Coverurl ?? ""
//        self.playUrl = playUrl ?? ""
//        self.id = id
//        self.isLocal = isLocal
//        self.isDownloading = isDownloading
//    }
//    
//}
