//
//  HelperClass.swift
//  NEO
//
//  Created by kirti on 05/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit
import Kanna
import Alamofire
class HelperClass: NSObject {
 
    static var instance: HelperClass!
    
    class func shared() -> HelperClass {
        
        self.instance = (self.instance ?? HelperClass())
        return self.instance
    }
    class func showAlertMessage(title:String = "",message:String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        var rootViewController = UIApplication.shared.keyWindow?.rootViewController
        if let navigationController = rootViewController as? UINavigationController {
            rootViewController = navigationController.viewControllers.first
        }
        if let tabBarController = rootViewController as? UITabBarController {
            rootViewController = tabBarController.selectedViewController
        }
        alertController .addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
        }))
        rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
  
   class func getVideoiDFromApi(Deezerid:String,completionHandler: @escaping VideoIdHandler) {
        
        WebserviceHelper.postWebServiceCall(urlString:kHostPath+kWebApi.kCheckDeezerTrackExistUniversal + Deezerid, parameters: [:], encodingType: DefaultEncoding, ShowProgress: false) { (isSuccess, dictData) in
//
//            authorId = 2668241;
//                       deezerTrackId = 680897122;
//                       id = 3548256;
//                       imageUrl = "https://e-cdns-images.dzcdn.net/images/cover/3efa6c25a81a6a049fc6883c5664b073/1000x1000-000000-80-0-0.jpg";
//                       title = "Always (Recorded at Deezer, Sao Paulo)";
//                       trackId = XS0shsrTEZQ;
//                       type = 1;
//                       userstracks =             (
//                       );
//                       ytMusic = "<null>";
            
            print("get Video id :",dictData)
            if isSuccess == true {
                if let arrdata = dictData .value(forKey: "data") as? [[String:AnyObject]],arrdata.count > 0 {
                  //  let dict = arrdata[0]
                    completionHandler(arrdata)
                } else {
                    completionHandler([[String:AnyObject]]())
                }
            } else {
                completionHandler([[String:AnyObject]]())
            }
            
          
        }
   
         
//     Example Value
//     Model
//     {
//       "trackID": "string",
//       "trackTitle": "string",
//       "trackImageUrl": "string",
//       "authorID": "string",
//       "authorName": "string",
//       "authorImageUrl": "string",
//       "deezerTrackID": "string",
//       "ytMusic": 0
//     }
    }
       class func getVideoiDFromYoutubeSearch(objPlayList:Music,completionHandler: @escaping VideoIdHandler) {
            
        var strUrl = "https://www.youtube.com/results?&q=\(objPlayList.authorName+"+"+objPlayList.trackTitle)".replacingOccurrences(of:" ", with: "+")
        print(strUrl)
        strUrl = strUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        guard let url = URL(string:strUrl) else {
                   completionHandler([[String:AnyObject]]())
                  return
               }
        
        
               do {
                   // content of url
                   let html = try String.init(contentsOf: url)
                 
                   if let doc = try? HTML(html: html, encoding: .utf8) {
                       print(doc.title)
                    var arr_title = [[String:AnyObject]]()
                       // Search for nodes by CSS
                       for link in doc.css("a, h3") {
                           print("1",link.text)
                           print("2",link["href"])
                           print("3",link["yt-lockup-title"])
                           print("4",link.content)
                           print("5",link.innerHTML)
                           print("6",link.toHTML)
                        if (link["href"]?.youtubeID?.count) ?? 0 > 0 {
                            arr_title .append(["trackId":link["href"]!.youtubeID! as AnyObject])
                        }
                       }
                       completionHandler(arr_title)
                       return
//                       // Search for nodes by XPath
//                       for link in doc.xpath("//a | //link | //h3") {
//                           print(link.text)
//                           print(link["href"])
//                           print(link["yt-lockup-title"])
//                       }
                   }
                  // parse()
               } catch let error {
                   // an error occurred
                  completionHandler([[String:AnyObject]]())
               }
            
    }
    class func SaveAllTrackFrom(album:[Music], index: Int, completionHandler: @escaping ConnectivityMusicObject) {
        
        if album.count > index{
            
            let objMusic = album[index]
            HelperClass.SaveTrackData(objMusic: objMusic, showProgress: false) { (sucess, objmusic) in
                
                HelperClass.SaveAllTrackFrom(album: album, index: index + 1, completionHandler: completionHandler)
                completionHandler(sucess,objmusic)
            }
        }
    }
    class func SaveTrackData(objMusic:Music, showProgress: Bool,completionHandler: @escaping ConnectivityMusicObject) {
        
        if (objMusic.trackID.count > 0) {
            
            HelperClass.saveTrackApi(obj: objMusic, inUserAccount: true, showProgress: showProgress) { (sucess, objmusic) in
                completionHandler(sucess,objmusic)
            }
        }else {
            HelperClass.getVideoiDFromApi(Deezerid: objMusic.deezerTrackID ?? "0") { (arrdict) in
                if (arrdict.count > 0 ) {
                    objMusic.trackID = arrdict.first!["trackId"] as? String ?? ""
                    HelperClass.saveTrackApi(obj: objMusic, inUserAccount: true, showProgress: showProgress) { (sucess, objmusic) in
                        completionHandler(sucess,objmusic)
                    }
                }else {
                    HelperClass.getVideoiDFromYoutubeSearch(objPlayList: objMusic) { (arrdict) in
                        if (arrdict.count > 0 ) {
                            objMusic.trackID = arrdict.first!["trackId"] as? String ?? ""
                            HelperClass.saveTrackApi(obj: objMusic, inUserAccount: true, showProgress: showProgress) { (sucess, objmusic) in
                                completionHandler(sucess,objmusic)
                            }
                        }
                    }
                }
            }
        }
    }
    class func saveTrackApi(obj:Music,inUserAccount:Bool, showProgress:Bool,completionHandler: @escaping ConnectivityMusicObject) {
        let common = [
            "trackID": obj.trackID,
            "trackTitle":obj.trackTitle,
            "trackImageUrl": obj.trackImageUrl!,
            "authorID":obj.authorID ,
            "authorName":obj.authorName,
            "authorImageUrl":obj.authorImageUrl ,
            "deezerTrackID":obj.deezerTrackID ?? "0",
            "ytMusic":0] as [String : AnyObject]
        
        if inUserAccount {
            let parameter = ["pos": "0",
                             "userId":UserDefaults.standard.value(forKey: kUserid) as Any,
                             "trackInfo":common,
                             "isOffline":"0"] as [String : AnyObject]
            
            WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.ksaveTrackUserRequest, parameters:parameter, encodingType: DefaultEncoding,ShowProgress:showProgress) { (isSuccess, dictData) in
                if isSuccess == true {
                    let dict = dictData .value(forKey: "data") as! [String : Any]
                    obj.isOffline = "0"
                    obj.trackReference = DataChecker .get(dict: dict, key: "trackReference")
                    obj.trackUserID = DataChecker .get(dict: dict, key: "trackUserID")
                    DownloadManager.shared().downloadSong(track: obj, isDownload: false) { (sucess, music, error) in
                        
                    }
                    HelperClass.shared().addObjectinGlobal(objMusic:obj) { (sucess) in
                        
                    }
                    completionHandler(true, obj)
                }else{
                    HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
                    completionHandler(false, obj)
                }
            }
        }else {
            WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.ksaveTrackRequest, parameters:common, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
                if isSuccess == true {
                    // objAppdelegate.arr_LocaltrackLists .append(obj)
                    completionHandler(true, obj)
                }else{
                    completionHandler(false, obj)
                    HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
                }
            }
        }
    }
    class func getValidationstring(dict: [String: Any], key: String) -> String {
        var str : String = ""
        
        if let nm = dict[key] as? NSNumber {
            str = nm.stringValue
        } else if let int = dict[key] as? Int {
            str = String(format: "%d", int)
        } else if let st = dict[key] as? String {
            str = st
        }
        
        return str
    }
    class func getUserPlayList(completionHandler: @escaping ConnectivityHandler){
        
          DispatchQueue.global().async {
         let parameters = [:] as [String : AnyObject]
              //  self.view .showBlurLoader()
                WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.kUserPlaylist+UserDefaults.standard.string(forKey: kUserid)!, parameters:parameters, encodingType: DefaultEncoding,ShowProgress:false) { (isSuccess, dictData) in
                   // self.view .removeBluerLoader()
                    if isSuccess == true {
                        print(dictData)
                       // DispatchQueue.global().async {
                            if let arrData = dictData .object(forKey: "data") as? NSDictionary {
                                SyncManager.shared().sync(data: arrData as! [String : Any])
                            }
                            HelperClass .getlocalTrack()
                             completionHandler(true)
                      //  }
                    }else{
                        HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
                    }
                }
        }
           
       }
    class func getYoutubeApiKey(isFirstTime:Bool){

        let parameters = [:] as [String : AnyObject]
        WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.kYoutubeAPIKey, parameters:parameters, encodingType: DefaultEncoding,ShowProgress:false) { (isSuccess, dictData) in
            if isSuccess == true {
                if let arrdata = dictData["data"] as? [[String:AnyObject]] {
                    if isFirstTime {
                        let dict = arrdata.first
                        UserDefaults.standard .set(dict!["ytKey"], forKey:kYoutubeKey)
                        UserDefaults.standard .set(true, forKey:kYoutubeKeyLoaded)
                        UserDefaults.standard .synchronize()
                    }else {
                          UserDefaults.standard .set(false, forKey:kYoutubeKeyLoaded)
                        HelperClass .testYoutubeKey(arrkeys: arrdata, index: 1)
                    }
                 
                }
            }
        }
        
    }
    class func testYoutubeKey(arrkeys:[[String:AnyObject]],index:Int) {
        if arrkeys.count == 0 {
              UserDefaults.standard .set(false, forKey:kYoutubeKeyLoaded)
              UserDefaults.standard .synchronize()
        }else {
            var IndexCount = index
           
            if index < arrkeys.count {
                
                let dict = arrkeys[index]
                let parameter = ["part":"snippet",
                                       "type":"video",
                                       "maxResults":"30",
                                       "order":"viewCount",
                                       "videoCategoryId":"10",
                                       "safeSearch":"none",
                                       "key":dict["ytKey"]!,
                                       "q":""] as! [String:String]
                      
                      Alamofire.request(URL(string:"https://www.googleapis.com/youtube/v3/search")!, method: .get, parameters: parameter, encoding:URLEncoding.default, headers: nil).responseJSON { (response) in
                          if response.result.isSuccess {
                              if let dictBig = response.result.value as? [String:AnyObject] {
                                UserDefaults.standard .set(dict["ytKey"], forKey:kYoutubeKey)
                                UserDefaults.standard .set(true, forKey:kYoutubeKeyLoaded)
                                UserDefaults.standard .synchronize()
                                HelperClass .updateYoutubeKey(key:dict["ytKey"] as! String)
                              }
                          }else {
                            IndexCount = IndexCount + 1
                            self .testYoutubeKey(arrkeys:arrkeys, index:IndexCount)
                            
                        }
                      }
                
            }else {
                UserDefaults.standard .set(false, forKey:kYoutubeKeyLoaded)
                UserDefaults.standard .synchronize()
            }
        }
    }
    class func updateYoutubeKey(key:String){
        let parameters = ["ytKey":key] as [String : AnyObject]
        WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.kUpdateYoutubeAPIKey, parameters:parameters, encodingType: DefaultEncoding,ShowProgress:false) { (isSuccess, dictData) in
            if isSuccess == true {
                if let arrdata = dictData["data"] as? [[String:AnyObject]] {
                    print("Update youtube Key:",arrdata)
                }
            }
        }
    }
    class func getPlaylistArray()->[PlayListModel] {
        let str_PlayLists = String(format: "Select * from tbl_playlist where id != '0' order by  cast (id as int) DESC")
        let arr_playLists = DataManager.initDB().RETRIEVE_PlayList(query: str_PlayLists)
        for infoPl in arr_playLists {
            let str_QuerySongs = String(format: "Select s.* from tbl_songs as s left join tbl_playlist_songs as pl on pl.trackReference = s.trackReference  where pl.p_id = '%@'", infoPl.id)
            infoPl.arr_songs = DataManager.initDB().RETRIEVE_Songs(query: str_QuerySongs)
        }
          
        return arr_playLists
    }
    class func getTracklistArray()->[Music] {
           let str_QuerySongs = String(format: "Select s.* from tbl_songs as s left join tbl_playlist_songs as pl on pl.trackReference = s.trackReference  where pl.p_id = '0'")
        let arr_trackLists = DataManager.initDB().RETRIEVE_Songs(query: str_QuerySongs)
        return arr_trackLists;
    }
    class func getlocalTrack() {
        
        
       let str_QuerySongs = String(format:"Select s.* from tbl_songs as s left join tbl_playlist_songs as pl on pl.trackReference = s.trackReference")
        objAppdelegate.arr_LocaltrackLists = DataManager.initDB().RETRIEVE_Songs(query: str_QuerySongs)
        
    }
    class func getTrackList(arrtrack:[[String:AnyObject]]) -> [Music] {
        var arrayData = [Music]()
        for i in 0 ..< arrtrack.count {
            let dict = arrtrack[i]
          
            let obj = Music()
            obj.trackTitle = HelperClass .getValidationstring(dict: dict, key: "title")
            obj.titleRename = HelperClass .getValidationstring(dict: dict, key: "titleRename")
            obj.trackImageUrl = HelperClass .getValidationstring(dict: dict, key: "artwork")
              obj.deezerTrackID = HelperClass .getValidationstring(dict: dict, key: "deezerTrackID")
            obj.musictype = .Deezer
            obj.musicNum = i
            
            obj.authorName = HelperClass .getValidationstring(dict: dict, key: "authorName")
            obj.authorImageUrl = HelperClass .getValidationstring(dict: dict, key: "authorImageUrl")
            obj.authorID = HelperClass .getValidationstring(dict: dict, key: "authorID")
            obj.authorRenameName = HelperClass .getValidationstring(dict: dict, key: "authorRenameName")
            
            obj.addedDate = HelperClass .getValidationstring(dict: dict, key: "addedDate")
            obj.listenDate = HelperClass .getValidationstring(dict: dict, key: "listenDate")
            obj.listenTimes = HelperClass .getValidationstring(dict: dict, key: "listenTimes")
            
            obj.trackReference = HelperClass .getValidationstring(dict: dict, key: "trackReference")
            obj.trackUserID = HelperClass .getValidationstring(dict: dict, key: "trackUserID")
            obj.trackID = HelperClass .getValidationstring(dict: dict, key: "trackID")
         
            obj.isOffline = HelperClass .getValidationstring(dict: dict, key: "isOffline")
            obj.tracktype = dict["type"] as! Int
            obj.playlistPos = HelperClass .getValidationstring(dict: dict, key: "playlistPos")
            
           arrayData .append(obj)
            
        }
        return arrayData
    }/*
   class func saveTrackApi(obj:Music,inUserAccount:Bool ,completionHandler: @escaping ConnectivityMusicObject) {
        let common = [
            "trackID": obj.trackID,
            "trackTitle":obj.trackTitle,
            "trackImageUrl": obj.trackImageUrl!,
            "authorID":obj.authorID ,
            "authorName":obj.authorName,
            "authorImageUrl":obj.authorImageUrl ,
            "deezerTrackID":obj.deezerTrackID ?? "0",
            "ytMusic":0] as [String : AnyObject]
    
        if inUserAccount {
            let parameter = ["pos": "0",
                             "userId":UserDefaults.standard.value(forKey: kUserid) as Any,
                             "trackInfo":common,
                             "isOffline":"0"] as [String : AnyObject]
            
            WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.ksaveTrackUserRequest, parameters:parameter, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
                if isSuccess == true {
                    let dict = dictData .value(forKey: "data") as! [String : Any]
                    obj.isOffline = "0"
                    obj.trackReference = DataChecker .get(dict: dict, key: "trackReference")
                    obj.trackUserID = DataChecker .get(dict: dict, key: "trackUserID")
                    DownloadManager.shared().downloadSong(track: obj, isDownload: false) { (sucess, music, error) in
                    }
                   
                   
                    HelperClass.shared().addObjectinGlobal(objMusic:obj) { (sucess) in
                        
                    }
                    
                    //objAppdelegate.arr_LocaltrackLists .append(obj)
                    completionHandler(true, obj)
                }else{
                    HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
                     completionHandler(false, obj)
                }
            }
        }else {
            WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.ksaveTrackRequest, parameters:common, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
                if isSuccess == true {
                    //  objAppdelegate.arr_LocaltrackLists .append(obj)
                     completionHandler(true, obj)
                }else{
                    completionHandler(false, obj)
                    HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
                }
            }
        }
        
        
        
        
    }
 */
    
    func addObjectinGlobal(objMusic:Music,completionHandler: @escaping ConnectivityHandler){
          let index = objAppdelegate.arr_LocaltrackLists .firstIndex { (obj) -> Bool in
              return objMusic.deezerTrackID == obj.deezerTrackID
              } ?? -1
          if index != -1 {
              objAppdelegate.arr_LocaltrackLists[index] = objMusic
          }
          else {
             objAppdelegate.arr_LocaltrackLists .append(objMusic)
        }
          completionHandler(true)
          
      }
        
    
}
extension UIViewController
{
    
     func showAlertMessage(title:String = "",message:String) {
        let alertController = UIAlertController(title:"", message: message, preferredStyle: .alert)
        alertController .addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
  
    func addProfilebutton() {
        navigationController?.view.backgroundColor = UIColor.white
        let menuButton = UIBarButtonItem(image: UIImage(named: "user"), style: .plain, target: self, action: #selector(btnActionMenu))
        self.navigationItem.rightBarButtonItem  = menuButton
    }
     func showAlertWithContact(message:String) {
          
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        alertController .addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
        }))
        alertController .addAction(UIAlertAction(title: kProfile.contactdevs, style: .default, handler: { (action) in
            UIApplication.shared.open(URL(string: ksupport)!, options: [:]) { (true) in
                           }
        }))
        self.present(alertController, animated: true, completion: nil)
      }
    func addBackbutton() {
        
        navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.interactivePopGestureRecognizer!.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer!.delegate = self as? UIGestureRecognizerDelegate;
        let menuButton = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(btnActionBack))
        self.navigationItem.leftBarButtonItem  = menuButton
    }
    @objc(gestureRecognizer:shouldBeRequiredToFailByGestureRecognizer:) func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
             return true
         }
    @objc func btnActionMenu() {
        
         self.view .endEditing(true)
         let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
         let obj = storyboardMain.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
         self.navigationController?.pushViewController(obj, animated: true)
    }
    @objc func btnActionBack() {
        
       self.navigationController?.popViewController(animated: true)
        
        // self.panel?.openLeft(animated: true)
    }
    func getValidationstring(dict: [String: Any], key: String) -> String {
        var str : String = ""
        
        if let nm = dict[key] as? NSNumber {
            str = nm.stringValue
        } else if let int = dict[key] as? Int {
            str = String(format: "%d", int)
        } else if let st = dict[key] as? String {
            str = st
        }
        
        return str
    }
    func setNavigationBarController()->UINavigationController {
        let navbar = UINavigationController(rootViewController: self)
        navbar.view.backgroundColor = UIColor.white
        navbar.modalPresentationStyle = .fullScreen
        navbar.navigationBar.isTranslucent = false
        return navbar;
    }
    func tabbarStoryBoard()->UIStoryboard {
        let storyboardRider = UIStoryboard(name: "Tabbar", bundle: nil)
        return storyboardRider;
    }
    func libraryStoryBoard()->UIStoryboard {
        let storyboardRider = UIStoryboard(name: "Library", bundle: nil)
        return storyboardRider;
    }
    func showAlerMessage(message:String)
    {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (aciton) in
            
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    func getEyeButton()->UIButton
    {
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn .setImage(UIImage(named: "eyeselected"), for: .normal)
        btn .setImage(UIImage(named: "eye"), for: .selected)
        return btn;
    }
    func setCustomNavigationBar() {
        let colors: [UIColor] = [UIColor(hexString: "439f2a"), UIColor(hexString: "F5B60D")]
        self.navigationController?.navigationBar.setGradientBackground(colors: colors)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    func getCollectionSize()->CGFloat {
        
        if UIDevice.isPad {
            let padding: CGFloat =  90
            let collectionViewSize = UIScreen.main.bounds.width - padding
            return  collectionViewSize/4
        }else {
            let padding: CGFloat =  45
            let collectionViewSize = UIScreen.main.bounds.width - padding
            return  collectionViewSize/2
        }
        
    }
    func timeFormatted(totalSeconds:Int)->String{
        var seconds : String = String(totalSeconds % 60)
        var minutes :String = String((totalSeconds / 60) % 60);
        if seconds.count == 1 {
            seconds="0"+seconds
        }
        if minutes.count == 1{
            minutes="0"+minutes
        }
        //获取字符串长度
        return minutes+":"+seconds
    }
    func getImageWithColor(color: UIColor, size: CGSize) -> UIImage {
           let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
           UIGraphicsBeginImageContextWithOptions(size, false, 0)
           color.setFill()
           UIRectFill(rect)
           let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
           UIGraphicsEndImageContext()
           return image
       }
 func setTextColor(objModel:Music,label:UILabel) {
     if objModel.deezerTrackID == objAppdelegate.objActiveMusic?.deezerTrackID {
         label.textColor = UIColor(hexString: colorGreen)
     }else{
         label.textColor = UIColor.black
     }
 }
    func setButtomImage(objModel:Music,btn:UIImageView)-> Music {
        var objModel = objModel
        let objMusic = objAppdelegate.arr_LocaltrackLists.first { (objMusic) -> Bool in
            return objMusic.deezerTrackID == objModel.deezerTrackID
        }
        if objMusic ==  nil {
            btn.image = UIImage(named: "add")
         
        }else {
            if objMusic?.isOffline == "1"  && objMusic?.download_status == "1" {
              //  btn .setImage(UIImage(named: "tick"), for: .normal)
                btn.image = UIImage(named: "tick")
            }
            else if objMusic?.isOffline == "1"  && objMusic?.download_status == "0" {
             //   btn .setImage(UIImage(named: "clock"), for: .normal)
                if DownloadManager.shared().getTrackInDownloding() .contains(objModel.trackID) {
                    btn.image = UIImage(named: "spiner")
                }else {
                     btn.image = UIImage(named: "clock")
                }
                
            } else {
               // btn .setImage(UIImage(named: "download"), for: .normal)
                  btn.image = UIImage(named: "download")
            }
            objModel = objMusic ?? Music()


        }
        return objModel
    }
    func saveTrackOffline(objMusic:Music) {
        let parameters = ["trackUserID":objMusic.trackUserID,"isOffline":1] as [String : AnyObject]
        WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.kSaveTrackOffline, parameters:parameters, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
            if isSuccess == true {
            }else{
                HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
            }
        }
    }
  
//
 //   func showIndicar(isShow:Bool)
//    {
//        if isShow {
//            let activityView = UIActivityIndicatorView(style: .whiteLarge)
//            activityView.tintColor = UIColor.red
//            activityView.center = self.view.center
//            activityView.tag = 789
//            self.view.addSubview(activityView)
//            activityView.startAnimating()
//            activityView .bringSubviewToFront(self.view)
//        }else {
//            let activityIndicator =  self.view.viewWithTag(789) as? UIActivityIndicatorView
//            activityIndicator?.stopAnimating()
//            activityIndicator?.removeFromSuperview()
//        }
//
//    }
    func json(from object:[[String:AnyObject]]) -> String? {
       guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
           return nil
       }
       return String(data: data, encoding: String.Encoding.utf8)
   }
    
    
    
   
}
extension MutableCollection {
    /// Shuffles the contents of this collection.
    mutating func shuffle() {
        let c = count
        guard c > 1 else { return }
        
        for (firstUnshuffled, unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
            // Change `Int` in the next line to `IndexDistance` in < Swift 4.1
            let d: Int = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
            let i = index(firstUnshuffled, offsetBy: d)
            swapAt(firstUnshuffled, i)
        }
    }
}
extension UITextField
{
    func LeftviewImage(name:String) {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 40))
        let frame = CGRect(x: 15, y: 10, width: 20, height: 20)
        let imgeview = UIImageView(frame:frame)
        imgeview.image = UIImage(named: name)
        imgeview.contentMode = .scaleAspectFit;
        view .addSubview(imgeview)
        self.leftView = view;
        self.leftViewMode = .always;
    }
    func Leftview() {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 35))
        self.leftView = view;
        self.leftViewMode = .always;
    }
    func addButtonWithCornerBorder() {
        
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 20
        self.layer.masksToBounds = true
        self.borderStyle = .none
    }
    func addBottomBorder(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect.init(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        bottomLine.backgroundColor = UIColor.lightGray.cgColor
        self.borderStyle = .none
        self.layer.addSublayer(bottomLine)
        
    }
}
extension Dictionary {

    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }

   

}
extension UIImageView
{
    func makeBlurImage()
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.prominent)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds

        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
}
extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    class var getWhiteColor:UIColor {
        return UIColor.white
    }
}
extension UISearchBar {
    func getTextField() -> UITextField? { return value(forKey: "searchField") as? UITextField }
    func setTextField(color: UIColor) {
        guard let textField = getTextField() else { return }
        switch searchBarStyle {
        case .minimal:
            textField.layer.backgroundColor = color.cgColor
            textField.layer.cornerRadius = 6
        case .prominent, .default: textField.backgroundColor = color
        @unknown default: break
        }
    }
   
}
extension UIButton {
    func setButtonWithBorderAndCorner() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(hexString: colorGreen).cgColor
        self.setTitleColor(UIColor(hexString: colorGreen), for: .normal)
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = 18
        self.clipsToBounds = true
        self.titleLabel?.font = UIFont(name: RobotoMedium, size: FontSizeNormal)
    }
    func setButtonWithGreenColor() {
        self.layer.cornerRadius = 18
        self.clipsToBounds = true
        self.setTitleColor(UIColor.getWhiteColor, for: .normal)
        self.backgroundColor = UIColor(hexString:colorGreen)
        self.titleLabel?.font = UIFont(name: RobotoMedium, size: FontSizeNormal)
    }
    func setCornerRound() {
        self.layer.cornerRadius = 25
        self.clipsToBounds = true
        self.titleLabel?.font = UIFont(name: RobotoMedium, size: FontSizeBig)
    }
}
extension UILabel {
    func SetcategoryTitle(){
        self.font = UIFont(name: RobotoBold, size: FontSizeNormal)
        self.textColor = UIColor.black
       
    }
    func SetcategorySubTitle(){
         self.font = UIFont(name: RobotoRegular, size: FontSizeSmall)
         self.textColor = UIColor(hexString: colorGray)
    }
    
}
extension UIView
{
    func setRound() {
        self.layer.cornerRadius = self.frame.width/2
        self.clipsToBounds = true
    }
   
    func setImageCornerRound() {
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }
    func setSmallImageCornerRound() {
        self.layer.cornerRadius = 7
        self.clipsToBounds = true
    }
    
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0,y: 0, width:self.frame.size.width, height:width)
        self.layer.addSublayer(border)
    }
    
    


    func setGradientForButton() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor(hexString: "389E47").cgColor, UIColor(hexString: "A0AF28").cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
     //   gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    func setGradientBackground(colorTop: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    func showBlurLoader() {
        let blurLoader = BlurLoader(frame:UIScreen.main.bounds)
        self.addSubview(blurLoader)
    }

    func removeBluerLoader() {
        if let blurLoader = subviews.first(where: { $0 is BlurLoader }) {
            blurLoader.removeFromSuperview()
        }
    }
}
extension UINavigationBar {
    func setGradientBackground(colors: [UIColor], startPoint: CAGradientLayer.Point = .topLeft, endPoint: CAGradientLayer.Point = .bottomLeft) {
        var updatedFrame = bounds
        updatedFrame.size.height += self.frame.origin.y
        let gradientLayer = CAGradientLayer(frame: updatedFrame, colors: colors, startPoint: startPoint, endPoint: endPoint)
        setBackgroundImage(gradientLayer.createGradientImage(), for: UIBarMetrics.default)
    }
}
extension CAGradientLayer {
    
    enum Point {
        case topRight, topLeft
        case bottomRight, bottomLeft
        case custion(point: CGPoint)
        
        var point: CGPoint {
            switch self {
            case .topRight: return CGPoint(x: 1, y: 0)
            case .topLeft: return CGPoint(x: 0, y: 0)
            case .bottomRight: return CGPoint(x: 1, y: 1)
            case .bottomLeft: return CGPoint(x: 0, y: 1)
            case .custion(let point): return point
            }
        }
    }
    
    convenience init(frame: CGRect, colors: [UIColor], startPoint: CGPoint, endPoint: CGPoint) {
        self.init()
        self.frame = frame
        self.colors = colors.map { $0.cgColor }
        self.startPoint = startPoint
        self.endPoint = endPoint
    }
    
    convenience init(frame: CGRect, colors: [UIColor], startPoint: Point, endPoint: Point) {
        self.init(frame: frame, colors: colors, startPoint: startPoint.point, endPoint: endPoint.point)
    }
    
    func createGradientImage() -> UIImage? {
        defer { UIGraphicsEndImageContext() }
        UIGraphicsBeginImageContext(bounds.size)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        render(in: context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

extension String
{
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    func getdateFromString(formate:String)->Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        return dateFormatter.date(from:self)!
    }
    func convertStringToStandardDateFormat() -> NSDate {
        
        
        
        let strMessage = (self as NSString).doubleValue
        
        // if let timeResult = (dict["last_message_timestamp"] as? Float) {
        let date = Date(timeIntervalSince1970:strMessage)
        //        let dateFormatter = DateFormatter()
        //        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        //        dateFormatter.dateStyle = DateFormatter.Style.short //Set date style
        //        dateFormatter.timeZone = .current
        //        return dateFormatter.string(from: date)
        
        //        let dateFormatter = DateFormatter()
        //        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        //        let dateString = dateFormatter.date(from: self)! as NSDate
        //        return dateString as NSDate
        
        return date as NSDate
    }
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    func isValidUsername() -> Bool {
        let RegEx = "^[0-9a-zA-Z\\_.]{5,32}$"
        //  let RegEx = "^(?=.*[A-Z])(?=.*[0-9])[A-Za-z\\d$@$#!%*?&]{8,}"
        
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: self)
    }
    func isValidPassword() -> Bool {
        //        let RegEx = "\\w{5,32}"
        let RegEx = "^(?=.*[A-Z])(?=.*[0-9])[A-Za-z\\d$@$#!%*?&]{8,}"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: self)
    }
   
    var youtubeID: String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)
        
        guard let result = regex?.firstMatch(in: self, range: range) else {
            return nil
        }
        
        return (self as NSString).substring(with: result.range)
    }
    
}
extension Date {
    
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970)
    }
    
    init(millis: Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(millis / 1000))
        self.addTimeInterval(TimeInterval(Double(millis % 1000) / 1000 ))
    }
    
}
public extension UIDevice {
    
    class var isPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    class var isPad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    class var isTV: Bool {
        return UIDevice.current.userInterfaceIdiom == .tv
    }
    class var isCarPlay: Bool {
        return UIDevice.current.userInterfaceIdiom == .carPlay
    }
    
}
class BlurLoader: UIView {

    var blurEffectView: UIView?

    override init(frame: CGRect) {
      
        let blurEffectView = UIView()
        blurEffectView.frame = frame
        self.blurEffectView = blurEffectView
        super.init(frame: frame)
        addSubview(blurEffectView)
        addLoader()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addLoader() {
        guard let blurEffectView = blurEffectView else { return }
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.color = UIColor.black
        
        if blurEffectView.frame.origin.y == 0 {
            activityIndicator.frame = CGRect(x: (blurEffectView.frame.size.width/2)-25, y: (blurEffectView.frame.size.height/2)-(25+64), width: 50, height: 50)
        }else {
              activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            activityIndicator.center = blurEffectView.center
        }
      
        blurEffectView.addSubview(activityIndicator)
    
        activityIndicator.startAnimating()
    }
}
