//
//  DownloadManager.swift
//  ESound
//
//  Created by Shiv on 08/12/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit
import Alamofire

class DownloadManager: NSObject {
    
    fileprivate var ary_TempFail : [String] = []
    fileprivate var muary_Downloading = NSMutableArray()
    
    fileprivate var isDownloading : Bool = false
    
    static var instance: DownloadManager!
    
    class func shared() -> DownloadManager {
        
        self.instance = (self.instance ?? DownloadManager())
        return self.instance
    }
    
    func getTrackInDownloding() -> NSMutableArray {
        
        return self.muary_Downloading
    }
    
    func downloadAll() {
        
        if self.isDownloading == false {
         
            self.isDownloading = true
            self.ary_TempFail = []
            self.startAllDownloadSong()
        }
    }
    
    fileprivate func startAllDownloadSong() {
        
        let str_TrackIDs = self.ary_TempFail.joined(separator: "','")
        let str_CheckSong = String(format: "Select * from tbl_songs where isOffline = '1' and download_status = '0' and trackID not in('%@') limit 1", str_TrackIDs)
        DataManager.initDB().databaseQueue.inDatabase { (db) in
            
            let resultset = db?.executeQuery(str_CheckSong, withArgumentsIn: nil)

            if resultset!.next() {
             
                let info_Music = Music()
                info_Music.trackReference = resultset!.string(forColumn: "trackReference")
                info_Music.trackID = resultset!.string(forColumn: "trackID")
                info_Music.deezerTrackID = resultset!.string(forColumn: "deezerTrackID")
                info_Music.trackTitle = resultset!.string(forColumn: "title")
                info_Music.titleRename = resultset!.string(forColumn: "titleRename")
                info_Music.trackImageUrl = resultset!.string(forColumn: "artwork")
                info_Music.authorID = resultset!.string(forColumn: "authorID")
                info_Music.authorName = resultset!.string(forColumn: "authorName")
                info_Music.authorRenameName = resultset!.string(forColumn: "authorRenameName")
                info_Music.authorImageUrl = resultset!.string(forColumn: "authorImageUrl")
                info_Music.isOffline = resultset!.string(forColumn: "isOffline")
                info_Music.addedDate = resultset!.string(forColumn: "addedDate")
                info_Music.listenDate = resultset!.string(forColumn: "listenDate")
                info_Music.listenTimes = resultset!.string(forColumn: "listenTimes")
                info_Music.trackUserID = resultset!.string(forColumn: "trackUserID")
                info_Music.playlistPos = resultset!.string(forColumn: "playlistPos")
                info_Music.tracktype = Int(resultset!.string(forColumn: "type"))!
                info_Music.download_status = resultset!.string(forColumn: "download_status")
                info_Music.file = resultset!.string(forColumn: "file")
                
                self.parserDownloadURL(track: info_Music) { (code, track, error) in
                     self.startAllDownloadSong()
                }
            }
            else {
                
                self.isDownloading = false
            }
            
            resultset?.close()
        }
    }
    
    func downloadSong(track: Music, isDownload:Bool = true, Handler:@escaping (_ code: Bool, _ track: Music, _ error: Error? ) -> Void) {
                
        let trackReference = DataChecker.replace(str: track.trackReference)
        let trackID = DataChecker.replace(str: track.trackID)
        let deezerTrackID = DataChecker.replace(str: track.deezerTrackID!)
        let title = DataChecker.replace(str: track.trackTitle)
        let titleRename = DataChecker.replace(str: track.titleRename)
        let artwork = DataChecker.replace(str: track.trackImageUrl!)
        let authorID = DataChecker.replace(str: track.authorID)
        let authorName = DataChecker.replace(str: track.authorName)
        let authorRenameName = DataChecker.replace(str: track.authorRenameName)
        let authorImageUrl = DataChecker.replace(str: track.authorImageUrl)
        let isOffline = DataChecker.replace(str: track.isOffline)
        let addedDate = DataChecker.replace(str: track.addedDate)
        let listenDate = DataChecker.replace(str: track.listenDate)
        let listenTimes = DataChecker.replace(str: track.listenTimes)
        let trackUserID = DataChecker.replace(str: track.trackUserID)
        let playlistPos = DataChecker.replace(str: track.playlistPos)
        let type = String(format: "%d", track.tracktype)
        
        let str_CheckSong = String(format: "Select * from tbl_songs where trackReference = '%@'", trackReference)
        DataManager.initDB().databaseQueue.inDatabase { (db) in
            
            let result = db?.executeQuery(str_CheckSong, withArgumentsIn: nil)
            
            var str_InsertUpdate : String = ""
            if result!.next() {
             
                str_InsertUpdate = String (format: "Update tbl_songs set trackID = '%@', deezerTrackID = '%@', title = '%@', titleRename = '%@', artwork = '%@', authorID = '%@', authorName = '%@', authorRenameName = '%@', authorImageUrl = '%@', isOffline = '%@', addedDate = '%@', listenDate = '%@', listenTimes = '%@', trackUserID = '%@', playlistPos = '%@', type = '%@' where trackReference = '%@'", trackID, deezerTrackID, title, titleRename, artwork, authorID, authorName, authorRenameName, authorImageUrl, isOffline, addedDate, listenDate, listenTimes, trackUserID, playlistPos, type, trackReference)
            }
            else {
                
                str_InsertUpdate = String (format: "Insert into tbl_songs (trackReference, trackID, deezerTrackID, title, titleRename, artwork, authorID, authorName, authorRenameName, authorImageUrl, isOffline, addedDate, listenDate, listenTimes, trackUserID, playlistPos, type) values('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", trackReference, trackID, deezerTrackID, title, titleRename, artwork, authorID, authorName, authorRenameName, authorImageUrl, isOffline, addedDate, listenDate, listenTimes, trackUserID, playlistPos, type)
            }
            
            result?.close()
            db!.executeUpdate(str_InsertUpdate, withArgumentsIn: nil)
        
        }
        if isDownload {
             self.parserDownloadURL(track: track, Handler: Handler)
        }
       
    }
    
    fileprivate func parserDownloadURL(track: Music, Handler:@escaping (_ code: Bool, _ track: Music, _ error: Error? ) -> Void) {
               
        self.muary_Downloading.add(track.trackID)
        DaiYoutubeParser.parse(track.trackID, screenSize: CGSize.zero, videoQuality: DaiYoutubeParserQualitySmall) { (status, url, videotitle, duration) in
            
            if (url != nil) {
                
                self.startDownloadSongFrom(url: url!, With: track, Handler: Handler)
            }
            else {
               
                self.downloadSongFail(track: track, With: nil, Handler: Handler)
            }
        }
    }
    
    fileprivate func startDownloadSongFrom(url: String, With track: Music, Handler:@escaping (_ code: Bool, _ track: Music, _ error: Error? ) -> Void) {
        
        let str_FileName = String(format: "Song-%@-%@%@", track.trackReference, track.trackID, kFileExtension)
        let str_FilePath = self.getPath(filename: str_FileName, With: kDownloadFolderName)
        
        let destination : DownloadRequest.DownloadFileDestination = { _, _ in
            
            let fileURL = URL(fileURLWithPath: str_FilePath)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(url, to: destination).response { (response) in
            
            if response.error == nil, let file = response.destinationURL?.path {
                
                print(file)
                self.downloadSongSuccess(fileName: str_FileName, track: track, Handler: Handler)
            }
            else {
                
                self.downloadSongFail(track: track, With: response.error, Handler: Handler)
            }
        }
    }
    
    fileprivate func downloadSongSuccess(fileName: String, track: Music, Handler:@escaping (_ code: Bool, _ track: Music, _ error: Error? ) -> Void) {
        
        DataManager.initDB().databaseQueue.inDatabase { (db) in
                        
            let str_UpdateSong = String (format: "Update tbl_songs set download_status = '1', file = '%@' where trackReference = '%@' and trackID = '%@'", fileName, track.trackReference, track.trackID)
            db!.executeUpdate(str_UpdateSong, withArgumentsIn: nil)
        }
        
        self.muary_Downloading.remove(track.trackID)
        track.file = fileName
        track.download_status = "1"
        Handler(true, track, nil)
    }
    
    fileprivate func downloadSongFail(track: Music, With error: Error?, Handler:@escaping (_ code: Bool, _ track: Music, _ error: Error? ) -> Void) {
        
        DataManager.initDB().databaseQueue.inDatabase { (db) in
            let str_UpdateSong = String (format: "Update tbl_songs set download_failed = '1' where trackReference = '%@' and trackID = '%@'", track.trackReference, track.trackID)
            db!.executeUpdate(str_UpdateSong, withArgumentsIn: nil)
        }
        
        self.muary_Downloading.remove(track.trackID)
        self.ary_TempFail.append(track.trackID)
        Handler(false, track, error)
        
        print("downloadSongFail: \(self.ary_TempFail)")
    }
    //MARK: - FilePath with Directory
    func getPath(filename: String?, With folder: String = "") -> String{
        
        let documentsPath: NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        
        if folder != "" {
            
            let forderPath = (documentsPath[0] as AnyObject).appendingPathComponent(folder) as String
            
            do {
                try FileManager.default.createDirectory(atPath: forderPath, withIntermediateDirectories: true, attributes: nil)
            }
            catch {
                
                NSLog("Unable to create directory")
            }
            
            if filename != "" {
             
                let filePath = (forderPath as AnyObject).appendingPathComponent(filename!) as String
                return filePath
            }
            else {
             
                return forderPath
            }
        }
        else {
            
            let filePath = (documentsPath[0] as AnyObject).appendingPathComponent(filename!) as String
            return filePath
        }
    }
}
