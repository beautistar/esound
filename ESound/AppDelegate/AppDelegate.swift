//
//  AppDelegate.swift
//  ESound
//
//  Created by kirti on 13/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit
import Firebase
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var arr_topPlayList = [PlayListModel]()
    var dict_TempMemory = [String:Any]()
    var arr_LocaltrackLists = [Music]()
    var objActiveMusic:Music?
    var timerSleep = Timer()
    var arr_EntirePlayList = [String]()
    // User Defaults preferance store
    class func getdefaultvalue() -> UserDefaults {
        
        return UserDefaults.standard
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Local database configuration
        DataManager.copyDBIfNeeded()
        
        // Local database upgrade version mapping
        DataManager.upgradeDatabaseIfRequired()
        
       // UINavigationBar.appearance().barTintColor = UIColor(hexString: colorGreen) //UIColor(red: 234.0/255.0, green: 46.0/255.0, blue: 73.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor(hexString: colorGreen)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor :UIColor.black]
        if #available(iOS 11.0, *) {
        UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor :UIColor(hexString: colorGreen)]
        UINavigationBar.appearance().backgroundColor = UIColor.white
        }
        FirebaseApp.configure()
        Fabric.with([Crashlytics.self])
        
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.shadowImage = UIImage()
            navBarAppearance.shadowColor = UIColor.clear
            navBarAppearance.backgroundColor = UIColor.white
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.black]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor(hexString: colorGreen)]
            navBarAppearance.backgroundColor = UIColor.white
            UINavigationBar.appearance().standardAppearance = navBarAppearance
            UINavigationBar.appearance().scrollEdgeAppearance = navBarAppearance
            
            
        }else {
            UINavigationBar.appearance().shadowImage = UIImage()
        }
        
        
      if #available(iOS 13.0, *) {
        window?.overrideUserInterfaceStyle = .light
             } else {
                 // Fallback on earlier versions
             }
       
        
       
        
        if UserDefaults.standard.bool(forKey: isLogged) {
              let storyboardRider = UIStoryboard(name: "Tabbar", bundle: nil)
            let objtabbar = storyboardRider.instantiateViewController(withIdentifier:"tabbar")
            objAppdelegate.window?.rootViewController  = objtabbar
            HelperClass .getUserPlayList { (success) in
            }
            HelperClass .getYoutubeApiKey(isFirstTime: true)
        }
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

