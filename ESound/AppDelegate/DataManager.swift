//
//  WebServiceManager.swift
//  QED
//
//  Created by Apple on 11/07/16.
//  Copyright © 2016 whitelotuscorporation. All rights reserved.
//

import UIKit

let VERSION_KEY = "VERSION_KEY"
let DB_NAME = "db_eSound"
let DB_TYPE = "sqlite"
let DB_UPGRADE_NAME = "Upgrade_db_eSound"
let DB_UPGRADE_TYPE = "plist"

class DataManager: NSObject {

    var db = FMDatabase()
    var rs = FMResultSet()
    var databaseQueue = FMDatabaseQueue()
    
    static var instance: DataManager!
        
    //MARK: - Copy DataBase
    
    func getDBPath() -> String {
        
        let documentsPath: NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray;
        let filePath = (documentsPath[0] as AnyObject).appendingPathComponent(String(format: "%@.%@", DB_NAME, DB_TYPE)) as String
        return filePath
    }
    
    class func copyDBIfNeeded() {
        
        let filemanager = FileManager.default
        
        var success = Bool()
        success = filemanager.fileExists(atPath: DataManager.init().getDBPath())
        print(DataManager.init().getDBPath())
        if success == false {
            
            let defaultDBPath = Bundle.main.path(forResource: DB_NAME, ofType: DB_TYPE)
            let data = NSData(contentsOfFile: defaultDBPath!)
            
            data?.write(toFile: DataManager.init().getDBPath(), atomically: true)
        }
    }
    
    //MARK: - database update if needed in ios
    
    func versionNumberString() -> String? {
        
        let infoDictionary = Bundle.main.infoDictionary
        let majorVersion = infoDictionary?["CFBundleVersion"] as? String
        return majorVersion
    }
    
   class func upgradeDatabaseIfRequired() {
        
        var previousVersion : String = ""
        let currentVersion : String = DataManager.init().versionNumberString()!
        
        if let mversion = AppDelegateGet.string(forKey: VERSION_KEY) {
            
            previousVersion = mversion
        }
        
        print(previousVersion)
        
        if previousVersion == "" || previousVersion.compare(currentVersion, options: .numeric) == .orderedAscending {
            
            let plistPath = Bundle.main.path(forResource: DB_UPGRADE_NAME, ofType: DB_UPGRADE_TYPE)
            let plist : NSArray = NSArray(contentsOfFile: plistPath!)!
            
            if previousVersion == "" {
                
                for value in plist {
                    
                    let dict = value as! NSDictionary
                    
                    let version = dict.value(forKey: "version") as! String
                    print(version)
                    let sqlQueries = dict.value(forKey: "sql") as! NSArray
                    
                    for query in sqlQueries {
                        
                        DataManager.initDB().EXECUTE_Query(query: query as! String)
                    }
                }
            }
            else {
                
                for value in plist {
                    
                    let dict = value as! NSDictionary
                    let version = dict.value(forKey: "version") as! String
                    print(version)
                    
                    if previousVersion.compare(version, options: .numeric) == .orderedAscending {
                        
                        let sqlQueries = dict.value(forKey: "sql") as! NSArray
                        
                        for query in sqlQueries {
                            
                            DataManager.initDB().EXECUTE_Query(query: query as! String)
                        }
                    }
                }
            }
            
            AppDelegateGet.set(currentVersion, forKey: VERSION_KEY)
            AppDelegateGet.synchronize()
        }
    }
    
    //MARK: - init Database
    
    class func initDB() -> DataManager {
        
        var isInit : Bool = true
        if (self.instance != nil) {
         
            isInit = false
        }
        
        self.instance = (self.instance ?? DataManager())
        
        if isInit {
            
            self.instance.databaseQueue = FMDatabaseQueue(path: self.instance.getDBPath())
        }
        return self.instance
    }
    
    func initFMDB() {
        
        self.db.close()
        self.db = FMDatabase(path: self.getDBPath())
    }
    
    func stringNullCheck(str: String) -> String {
        
        if str.count == 0 {
            
            return ""
        }
        else {
            
            return str
        }
    }
    
    
    //MARK: - Query
    // EXECUTE update type query
    func EXECUTE_Query(query: String) {
     
        self.initFMDB()
        
        if self.db.open() {
          
           self.db.executeUpdate(query, withArgumentsIn: nil)
        }
        self.db.close()
    }
    
   
    
    // Retrieve Songs List
    func RETRIEVE_Songs(query: String) -> [Music] {

        self.initFMDB()
        var muary_Data : [Music] = []
        if self.db.open() {

            self.rs = self.db.executeQuery(query, withArgumentsIn: nil)

            while self.rs.next() {
                let info_Music = Music()
                info_Music.trackReference = self.rs.string(forColumn: "trackReference")
                info_Music.trackID = self.rs.string(forColumn: "trackID")
                info_Music.deezerTrackID = self.rs.string(forColumn: "deezerTrackID")
                info_Music.trackTitle = self.rs.string(forColumn: "title")
                info_Music.titleRename = self.rs.string(forColumn: "titleRename")
                info_Music.trackImageUrl = self.rs.string(forColumn: "artwork")
                info_Music.authorID = self.rs.string(forColumn: "authorID")
                info_Music.authorName = self.rs.string(forColumn: "authorName")
                info_Music.authorRenameName = self.rs.string(forColumn: "authorRenameName")
                info_Music.authorImageUrl = self.rs.string(forColumn: "authorImageUrl")
                info_Music.isOffline = self.rs.string(forColumn: "isOffline")
                info_Music.addedDate = self.rs.string(forColumn: "addedDate")
                info_Music.listenDate = self.rs.string(forColumn: "listenDate")
                info_Music.listenTimes = self.rs.string(forColumn: "listenTimes")
                info_Music.trackUserID = self.rs.string(forColumn: "trackUserID")
                info_Music.playlistPos = self.rs.string(forColumn: "playlistPos")
                info_Music.tracktype = Int(self.rs.string(forColumn: "type"))!
                info_Music.download_status = self.rs.string(forColumn: "download_status")
                info_Music.file = self.rs.string(forColumn: "file")
                muary_Data.append(info_Music)
            }
        }
        self.db.close()
        return muary_Data
    }
    // Retrieve Artist
    func RETRIEVE_Artist(query: String) -> [PlayListModel] {
        self.initFMDB()
               var muary_Data : [PlayListModel] = []
               if self.db.open() {
                   self.rs = self.db.executeQuery(query, withArgumentsIn: nil)
                   while self.rs.next() {
                       let info_PlayList = PlayListModel(title:self.rs.string(forColumn: "authorName"),
                                                     subtitle:self.rs.string(forColumn: "COUNT(*)"),
                                                     url:self.rs.string(forColumn: "authorImageUrl"),
                                                     id:self.rs.string(forColumn: "authorID"))
                       muary_Data.append(info_PlayList)
                   }
               }
               self.db.close()
               return muary_Data
        
    }
   // SELECT authorID , COUNT(*) ,authorName,authorImageUrl FROM tbl_songs GROUP BY authorID
    
    // Retrieve PlayList
    func RETRIEVE_PlayList(query: String) -> [PlayListModel] {

        self.initFMDB()
        var muary_Data : [PlayListModel] = []
        
        
//            DataManager.initDB().databaseQueue.inDatabase { (db) in
//        }
        
        if self.db.open() {

            self.rs = self.db.executeQuery(query, withArgumentsIn: nil)

            while self.rs.next() {

                let info_PlayList = PlayListModel(title: self.rs.string(forColumn: "title"),
                                              subtitle: self.rs.string(forColumn: "description"),
                                              url: self.rs.string(forColumn: "thumb"),
                                              id: self.rs.string(forColumn: "id"),
                                              isOffline: self.rs.string(forColumn: "isOffline"))
                
                muary_Data.append(info_PlayList)
            }
        }
        self.db.close()
        return muary_Data
    }
    
    
}
