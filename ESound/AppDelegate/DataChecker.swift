//
//  DataChecker.swift
//  Wedding
//
//  Created by Shiv on 09/02/18.
//  Copyright © 2018 Shiv. All rights reserved.
//

import UIKit


class DataChecker: NSObject {
    
    // Data checker for api nil data to reaplce black or cast data
    class func get(dict: [String: Any], key: String) -> String {
        
        var str : String = ""
        
        if let nm = dict[key] as? NSNumber {
            str = nm.stringValue
        } else if let int = dict[key] as? Int {
            str = String(format: "%d", int)
        } else if let st = dict[key] as? String {
            str = st
        }
        
        return str
    }
    
    // replace data which data conflict for databse insert or update
    class func replace(str: String) -> String {
        
        var str_Text : String = ""
        if str.count > 0 {
        
            str_Text = str.replacingOccurrences(of: "'", with: "''")
            str_Text = str_Text.replacingOccurrences(of: "\"", with: "")
            str_Text = str_Text.replacingOccurrences(of: "\\", with: "")
            str_Text = str_Text.replacingOccurrences(of: "₹", with: "")
        }
        
        return str_Text
    }
    
    class func getAndReplace(dict: [String: Any], key: String) -> String {
        
        var str : String = ""
        
        if let nm = dict[key] as? NSNumber {
            str = nm.stringValue
        } else if let int = dict[key] as? Int {
            str = String(format: "%d", int)
        } else if let st = dict[key] as? String {
            str = st
        }
        
        var str_Text : String = ""
        if str.count > 0 {
        
            str_Text = str.replacingOccurrences(of: "'", with: "''")
            str_Text = str_Text.replacingOccurrences(of: "\"", with: "")
            str_Text = str_Text.replacingOccurrences(of: "\\", with: "")
            str_Text = str_Text.replacingOccurrences(of: "₹", with: "")
        }
        
        return str_Text
    }
}
