//
//  SyncManager.swift
//  ESound
//
//  Created by Shiv on 07/12/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class SyncManager: NSObject {

    var ary_PlayListIDs : [String] = []
    var ary_SongIDs : [String] = []
    
    static var instance: SyncManager!
    
    class func shared() -> SyncManager {
        
        self.instance = (self.instance ?? SyncManager())
        return self.instance
    }
    
    func sync(data: [String : Any]) {
                
        self.ary_PlayListIDs = []
        self.ary_SongIDs = []
        
        self.syncData(data: data, index: 0) { (success) in
            
            DownloadManager.shared().downloadAll()
        }
    }
    
    fileprivate func syncData(data: [String : Any], index: Int, A_Handler:@escaping (_ code: Bool) -> Void) {
        
        let ary_Keys = data.keys
        
        if ary_Keys.count > index {
            
            let str_Key = Array(ary_Keys)[index]
            let dict_List = data[str_Key] as! [String : Any]
            
            self.syncPlayListData(p_data: dict_List, With: str_Key) { (succes) in
                self.syncData(data: data, index: index + 1, A_Handler: A_Handler)
            }
        }
        else {
                        
            DataManager.initDB().databaseQueue.inDatabase { (db) in
                
                let str_DeletePlayListIds = self.ary_PlayListIDs.joined(separator: "','")
                let str_DeletePlayLists = String (format: "Delete from tbl_playlist where id not in('%@')", str_DeletePlayListIds)
                db!.executeUpdate(str_DeletePlayLists, withArgumentsIn: nil)
                
                let str_DeleteTrackIds = self.ary_SongIDs.joined(separator: "','")
                let str_DeleteTracks = String(format: "Delete from tbl_songs where trackReference not in('%@')", str_DeleteTrackIds)
                db!.executeUpdate(str_DeleteTracks, withArgumentsIn: nil)
            }
            
            A_Handler(true)
        }
    }
    
    fileprivate func syncPlayListData(p_data: [String : Any],With pid: String , P_Handler:@escaping (_ code: Bool) -> Void) {
        
        let dict_PlayList = p_data["playlist"] as! [String : Any]
        
        let id = pid//DataChecker.getAndReplace(dict: dict_PlayList, key: "id")
        let title = DataChecker.getAndReplace(dict: dict_PlayList, key: "title")
        let thumb = DataChecker.getAndReplace(dict: dict_PlayList, key: "thumb")
        let description = DataChecker.getAndReplace(dict: dict_PlayList, key: "description")
        let isOffline = DataChecker.getAndReplace(dict: dict_PlayList, key: "isOffline")
        let orderType = DataChecker.getAndReplace(dict: dict_PlayList, key: "orderType")
        
        DataManager.initDB().databaseQueue.inDatabase { (db) in
            
            let str_CheckPlayList = String(format: "Select * from tbl_playlist where id = '%@'", id)
            let result = db?.executeQuery(str_CheckPlayList, withArgumentsIn: nil)
            
            var str_InsertUpdate : String = ""
            if result!.next() {
             
                str_InsertUpdate = String (format: "Update tbl_playlist set title = '%@', thumb = '%@', description = '%@', isOffline = '%@', orderType = '%@' where id = '%@'", title, thumb, description, isOffline, orderType, id)
            }
            else {
                
                str_InsertUpdate = String (format: "Insert into tbl_playlist (id, title, thumb, description, isOffline, orderType) values('%@', '%@', '%@', '%@', '%@', '%@')", id, title, thumb, description, isOffline, orderType)
            }
            
            result?.close()
            db!.executeUpdate(str_InsertUpdate, withArgumentsIn: nil)
                        
            let str_DeletePlayList = String(format: "Delete from tbl_playlist_songs where p_id = '%@'", id)
            db!.executeUpdate(str_DeletePlayList, withArgumentsIn: nil)
        }
        
        self.ary_PlayListIDs.append(id)
        let ary_Songs = p_data["songs"] as! [[String : Any]]
        
        self.syncSongData(s_Array: ary_Songs, WithPlayList: id, index: 0) { (success) in
                       
            P_Handler(true)
        }
    }
    
    fileprivate func syncSongData(s_Array: [[String : Any]], WithPlayList id: String, index: Int, S_Handler:@escaping (_ code: Bool) -> Void) {
        
        if s_Array.count > index {
         
            let dict_Song = s_Array[index]
            
            let trackReference = DataChecker.getAndReplace(dict: dict_Song, key: "trackReference")
            let trackID = DataChecker.getAndReplace(dict: dict_Song, key: "trackID")
            let deezerTrackID = DataChecker.getAndReplace(dict: dict_Song, key: "deezerTrackID")
            let title = DataChecker.getAndReplace(dict: dict_Song, key: "title")
            let titleRename = DataChecker.getAndReplace(dict: dict_Song, key: "titleRename")
            let artwork = DataChecker.getAndReplace(dict: dict_Song, key: "artwork")
            let authorID = DataChecker.getAndReplace(dict: dict_Song, key: "authorID")
            let authorName = DataChecker.getAndReplace(dict: dict_Song, key: "authorName")
            let authorRenameName = DataChecker.getAndReplace(dict: dict_Song, key: "authorRenameName")
            let authorImageUrl = DataChecker.getAndReplace(dict: dict_Song, key: "authorImageUrl")
            let isOffline = DataChecker.getAndReplace(dict: dict_Song, key: "isOffline")
            let addedDate = DataChecker.getAndReplace(dict: dict_Song, key: "addedDate")
            let listenDate = DataChecker.getAndReplace(dict: dict_Song, key: "listenDate")
            let listenTimes = DataChecker.getAndReplace(dict: dict_Song, key: "listenTimes")
            let trackUserID = DataChecker.getAndReplace(dict: dict_Song, key: "trackUserID")
            let playlistPos = DataChecker.getAndReplace(dict: dict_Song, key: "playlistPos")
            let type = DataChecker.getAndReplace(dict: dict_Song, key: "type")
            
            let str_CheckSong = String(format: "Select * from tbl_songs where trackReference = '%@'", trackReference)
            DataManager.initDB().databaseQueue.inDatabase { (db) in
                
                let result = db?.executeQuery(str_CheckSong, withArgumentsIn: nil)
                
                var str_InsertUpdate : String = ""
                if result!.next() {
                 
                    str_InsertUpdate = String (format: "Update tbl_songs set trackID = '%@', deezerTrackID = '%@', title = '%@', titleRename = '%@', artwork = '%@', authorID = '%@', authorName = '%@', authorRenameName = '%@', authorImageUrl = '%@', isOffline = '%@', addedDate = '%@', listenDate = '%@', listenTimes = '%@', trackUserID = '%@', playlistPos = '%@', type = '%@' where trackReference = '%@'", trackID, deezerTrackID, title, titleRename, artwork, authorID, authorName, authorRenameName, authorImageUrl, isOffline, addedDate, listenDate, listenTimes, trackUserID, playlistPos, type, trackReference)
                }
                else {
                    
                    str_InsertUpdate = String (format: "Insert into tbl_songs (trackReference, trackID, deezerTrackID, title, titleRename, artwork, authorID, authorName, authorRenameName, authorImageUrl, isOffline, addedDate, listenDate, listenTimes, trackUserID, playlistPos, type) values('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", trackReference, trackID, deezerTrackID, title, titleRename, artwork, authorID, authorName, authorRenameName, authorImageUrl, isOffline, addedDate, listenDate, listenTimes, trackUserID, playlistPos, type)
                }
                
                result?.close()
                db!.executeUpdate(str_InsertUpdate, withArgumentsIn: nil)
                
                let str_InsertPlayList = String (format: "Insert into tbl_playlist_songs (p_id, trackReference) values('%@', '%@')", id, trackReference)
                db!.executeUpdate(str_InsertPlayList, withArgumentsIn: nil)
            }
            
            self.ary_SongIDs.append(trackReference)
            self.syncSongData(s_Array: s_Array, WithPlayList: id, index: index + 1, S_Handler: S_Handler)
        }
        else{
            
            S_Handler(true)
        }
    }
    func insertIntoPlaylist(playlistID:String,trackReference:String) {
        DataManager.initDB().databaseQueue.inDatabase { (db) in
            let str_InsertPlayList = String (format: "INSERT OR REPLACE into tbl_playlist_songs (p_id, trackReference) values('%@', '%@')", playlistID, trackReference)
            db!.executeUpdate(str_InsertPlayList, withArgumentsIn: nil)
        }
    }
}
