//
//  PageControlVC.swift
//  ESound
//
//  Created by kirti on 19.12.19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class PageControlVC: UIPageViewController {
    
    fileprivate lazy var pages: [UIViewController] = {
        return [
            self.getViewController(withIdentifier: "navhome"),
            self.getViewController(withIdentifier: "navbrowse"),
            self.getViewController(withIdentifier: "navlibrary"),
            self.getViewController(withIdentifier: "navsearch")
        ]
    }()
    
    fileprivate func getViewController(withIdentifier identifier: String) -> UIViewController {
        return UIStoryboard(name:"Tabbar", bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewDidLoad()
      //  self.dataSource = self
       // self.delegate   = self
        self.isPagingEnabled = false
        
        if let firstVC = pages.first {
            setViewControllers([firstVC], direction: .forward, animated: false, completion: nil)
        }
    
    }
    func setPageIndex(index:Int){
        if let firstVC = pages[index] as? UINavigationController  {
            setViewControllers([firstVC], direction: .forward, animated: false, completion: nil)
        }
    }

}
extension PageControlVC: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0          else { return pages.last }
        
        guard pages.count > previousIndex else { return nil        }
        
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < pages.count else { return pages.first }
        
        guard pages.count > nextIndex else { return nil         }
        
        return pages[nextIndex]
    }
}
extension PageControlVC: UIPageViewControllerDelegate {
    
}
extension UIPageViewController {
    var isPagingEnabled: Bool {
        get {
            var isEnabled: Bool = true
            for view in view.subviews {
                if let subView = view as? UIScrollView {
                    isEnabled = subView.isScrollEnabled
                }
            }
            return isEnabled
        }
        set {
            for view in view.subviews {
                if let subView = view as? UIScrollView {
                    subView.isScrollEnabled = newValue
                }
            }
        }
    }
}


