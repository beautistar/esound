//
//  DetailMusicViewController.swift
//  SwiftMusic
//
//  Created by 科文 on 2017/5/3.
//  Copyright © 2017年 科文. All rights reserved.
//
import MediaPlayer
import UIKit
//import DropDown


let kAddtoPlayerQueue = "Add to player queue"
let kAddtoPlayList = "Add to playlist"
let ksaveSong = "save"
let kreportproblem = "Report a problem"
let kViewArtistPage = "View artist page"
let kSleepTimer = "Sleep timer"
let kRemove = "Remove"
let kRenameSongs = "Rename song"
let kremoveFromThisPlaylist = "Remove from this playlist"
let kfixTheSongs = "Fix this song"
let kRemoveFromLIbrary = "Remove from Library"



let kAlphabetically = "Alphabetically"
let kFromnewest = "From newest"
let kByTracksCount = "By tracks' count"
let kDefault = "Default"

let kAddSongs = "Add songs"
let kSortSongs = "Sort songs"
let kRenamePlaylist = "Rename playlist"
let kRemovePlaylist = "Remove this playlist"


let kCancel = "Cancel"


class MusicPlayerVC: UIViewController ,UIGestureRecognizerDelegate,UIScrollViewDelegate{
    @IBOutlet weak var randomPlay: UIButton!
    @IBOutlet weak var playMusicBtn:LoadingButton!
    @IBOutlet weak var leftTime: UILabel!
    @IBOutlet weak var rightTime: UILabel!
    @IBOutlet weak var view_tapgesture: UIView!
    @IBOutlet  var tbl_musicPlayer: UITableView!
    @IBOutlet weak var albumImg: UIImageView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var nextMusicBtn: UIButton!
    @IBOutlet weak var MusicAuthor: UILabel!
    @IBOutlet weak var MusicName: UILabel!
    @IBOutlet weak var musicProgress: UISlider!
    @IBOutlet weak var volumeSlider: UISlider!
    
    @IBOutlet weak var imgvw_status: UIImageView!
    var arr_musicList:Array<Music>  = []
    
    var nowNum:Int!
    var model : Music!
    var musicArry:Array<Music>  = []
    var timer:Timer!
    var isNewIndex = Bool()
    var isManualluyStop = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        musicProgress.value=0 //初始化进度条为0
        let cell = UINib(nibName: "cell_songslist", bundle: nil)
        self.tbl_musicPlayer.register(cell, forCellReuseIdentifier: "cell_songslist")
        
        let cell_header = UINib(nibName: "Cell_tblheader", bundle: nil)
        self.tbl_musicPlayer.register(cell_header, forCellReuseIdentifier: "Cell_tblheader")
        
        self.tbl_musicPlayer.estimatedRowHeight = 60
        self.tbl_musicPlayer.rowHeight = 60
        
        self.playMusicBtn.layer.cornerRadius = self.playMusicBtn.frame.width/2
        self.playMusicBtn.layer.borderWidth = 1
        self.playMusicBtn.layer.borderColor = UIColor.black.cgColor
        
        self.MusicName.font = UIFont(name: RobotoBold, size: 23)
        
        self.MusicAuthor.font = UIFont(name: RobotoRegular, size: FontSizeNormal)
        self.MusicAuthor.textColor = UIColor(hexString: colorGreen)
        
        let view  = self.tbl_musicPlayer.tableHeaderView!
        view.frame = self.view.frame
      
        var height =  (view.frame.height)
        if UIDevice.isPhone {
            if #available(iOS 11.0, *) {
                height =  view.frame.height - (((UIApplication.shared.keyWindow?.safeAreaInsets.bottom) ?? 0) + ((UIApplication.shared.keyWindow?.safeAreaInsets.top) ?? 0))
            }
        }
        
        let width = view.frame.height
        view.frame = CGRect(x: 0, y: 0, width: width, height: height)
        self.tbl_musicPlayer.tableHeaderView = view
    
        self.tbl_musicPlayer.delaysContentTouches = false
        self.tbl_musicPlayer.bounces = false
        
        self.albumImg.layer.cornerRadius = 10
        self.albumImg.layer.masksToBounds = true

        NotificationCenter.default.addObserver(self, selector:#selector(volumeDidChange(notification:)), name: NSNotification.Name(rawValue:"AVSystemController_SystemVolumeDidChangeNotification"), object: nil)
        
        NotificationCenter.default.addObserver(forName:ChangedSongSucessPlayerPage, object: nil, queue: nil) { (objNotification) in
            if self.timer == nil {
                self.timer=Timer .scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.playProgress), userInfo: nil, repeats: true)
            }
        }
        
        if (playMusicBtn != nil) {
           // playMusicBtn .showLoading()
        }
        
     
       
        
//        if #available(iOS 13.0, *) {
//            self.view.backgroundColor = UIColor.link
//        } else {
//            // Fallback on earlier versions
//        }
        //self.endAppearanceTransition()
    }
    @objc func volumeDidChange(notification: NSNotification) {
        
      let volume = notification.userInfo!["AVSystemController_AudioVolumeNotificationParameter"] as! Float
        AudioPlayer.voice(num: volume)
        if (volumeSlider != nil) {
            volumeSlider .setValue(volume, animated: true)
        }
      // Volume at your service
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    //随机播放按钮
    @IBAction func musicRandomPlay(_ sender: UIButton) {
        if AudioPlayer.musicRandomPlay(){
            sender.setImage(#imageLiteral(resourceName: "randomPlay"), for: .normal)
        }else{
            sender.setImage(#imageLiteral(resourceName: "sequencePlay"), for: .normal)
        }
        
    }
    //播放音乐
    @IBAction func musicPlayBtn(_ sender: Any) {
        let sender=sender as! UIButton
        if AudioPlayer.play() {
             sender.setImage(#imageLiteral(resourceName: "stopMusic"), for: .normal)
             self.playMusicBtn.imageEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
             isManualluyStop = false
        } else {
             sender.setImage(#imageLiteral(resourceName:"musicPlay"), for: .normal)
             self.playMusicBtn.imageEdgeInsets = .init(top: 0, left: 3, bottom: 0, right: 0)
              isManualluyStop = true
        }
        
    }
    
   
    //左上角关闭窗口
    @IBAction func closePlayer(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //播放下一曲方法
    @IBAction func nextTrack(_ sender: UIButton) {
        self.nowNum=self.nowNum+1
        if self.nowNum>=self.musicArry.count{
            self.nowNum=0
        }
        isManualluyStop = false
        showDetail(index: self.nowNum)
        let object = ["index":self.nowNum!,kactiveMusic:self.musicArry[self.nowNum!]] as [String : Any]
        NotificationCenter.default.post(name:ChangeSonagNotification, object: nil, userInfo: object)
       playMusicBtn .showLoading()
        AudioPlayer.nextsongMusic(num: self.nowNum) { (sucess) in
            if sucess {
//                self.playMusicBtn.setImage(#imageLiteral(resourceName: "stopMusic"), for: .normal)
//                self.playMusicBtn.imageEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
                self.reflashView()
                NotificationCenter.default.post(name:ChangedSongSucess, object: nil, userInfo: nil)
              
            }
        }
//        if AudioPlayer.nextsong(num: self.nowNum){
//            self.playMusicBtn.setImage(#imageLiteral(resourceName: "stopMusic"), for: .normal)
//             self.playMusicBtn.imageEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
//            self.timeStop()
//            reflashView()
//         
//        }
    }
    func showDetail(index:Int) {
        //self.timeStop()
        AudioPlayer .stop()
        self.musicProgress.setValue(0.0, animated: true)
        self.leftTime.text = "00:00"
        self.rightTime.text = "00:00"
        
        let model = Music.getALL()[index]
        if let strUrl = model.trackImageUrl {
            self.albumImg .sd_setImage(with:URL(string:strUrl)) { (image, error, cacheType, url) in
            }
        }
        
        self.MusicName.text=model.trackTitle
        self.MusicAuthor.text=model.authorName
    }
    
    //停止计时器方法
    func timeStop(){
        if timer != nil{
            self.timer.invalidate()
            self.timer=nil
        }
    }
    // 上一曲
    
    @IBAction func prevTrack(_ sender: UIButton) {
        
        self.nowNum=self.nowNum-1
        if self.nowNum == -1{
            self.nowNum=self.musicArry.count - 1
        }
        isManualluyStop = false
         showDetail(index: self.nowNum)
        let object = ["index":self.nowNum!,kactiveMusic:self.musicArry[self.nowNum!]] as [String : Any]
        NotificationCenter.default.post(name:ChangeSonagNotification, object: nil, userInfo: object)
        playMusicBtn .showLoading()
        AudioPlayer.prevsongMusic(num: self.nowNum) { (sucess) in
            if sucess {
                self.playMusicBtn.setImage(#imageLiteral(resourceName: "stopMusic"), for: .normal)
                self.playMusicBtn.imageEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
                self.reflashView()
                NotificationCenter.default.post(name:ChangedSongSucess, object: nil, userInfo: nil)
            }
        }

      
    }
    //声音控制
    @IBAction func sliderValueChange(_ sender: UISlider) {
        AudioPlayer.voice(num: sender.value)
    }
    @IBAction func sliderValueChangeForwardBackward(_ sender: UISlider) {
        print(sender.value)
        if let value = AudioPlayer.musicDuration() as? Double ,value > 0 {
             AudioPlayer.skipTime(time:Double(sender.value) * AudioPlayer.musicDuration())
        }
      
        
       // AudioPlayer.voice(num: sender.value)
    }
   
   //界面被拉起时，刷新界面
    override func viewWillAppear(_ animated: Bool) {
        
        isManualluyStop = false
        reflashView()
        if (volumeSlider != nil) {
            volumeSlider .setValue(AVAudioSession.sharedInstance().outputVolume, animated: true)
        }
    }

    //刷新界面，用 新model 重新赋值
    func reflashView(){
        self.model=AudioPlayer.activeSong()
        if model != nil {
            if  self.nowNum == self.model.musicNum {
                self.nowNum=self.model.musicNum!
                if timer == nil {
                    self.timer=Timer .scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.playProgress), userInfo: nil, repeats: true)
                }
                if  self.albumImg != nil {
                    self .setSongPlay()
                    self.musicProgress.isContinuous = false
                    self.refreshRightButtn()
                }
              
               
               
                
            }else {
                if (self.nowNum != nil),(self.isNewIndex) {
                    self .showDetail(index:self.nowNum)
                    self.isNewIndex = false
                }else {
                    if timer == nil { // kirti test
                      //  self .setSongPlay()
                        self.timer=Timer .scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.playProgress), userInfo: nil, repeats: true)
                    }
                }
            }
       
        }else {
           // self.playMusicBtn .showLoading()
//            print("Model nil")
            if (self.nowNum != nil) {
                 self .showDetail(index:self.nowNum)
                 self.playMusicBtn .showLoading()
            }
        }
        
        
    }
    func setSongPlay(){
        if let strUrl = self.model.trackImageUrl ,self.albumImg != nil  {
            self.albumImg .sd_setImage(with:URL(string:strUrl)) { (image, error, cacheType, url) in
            }
        }
        //self.albumImg .sd_setImage(with:self.model.trackImageUrl) { (image, error, cacheType, url) in}
        self.MusicName.text=self.model.trackTitle
        self.MusicAuthor.text=self.model.authorName
        if AudioPlayer.isPlaying(){
            self.playMusicBtn .hideLoading()
            self.playMusicBtn.setImage(#imageLiteral(resourceName: "stopMusic"), for: .normal)
            self.playMusicBtn.imageEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
        }else{
            self.playMusicBtn.setImage(#imageLiteral(resourceName: "musicPlay"), for: .normal)
            self.playMusicBtn.imageEdgeInsets = .init(top: 0, left: 3, bottom: 0, right: 0)
        }
    }
    // 关闭MV

    //歌曲进度条展示
    @objc func playProgress(){
        
        if self.musicProgress .isHighlighted == false {
            
           
            self.leftTime.text = self.timeFormatted(totalSeconds: Int(AudioPlayer.currentTime()))
            if AudioPlayer.isPlaying(){
                self.playMusicBtn.setImage(#imageLiteral(resourceName: "stopMusic"), for: .normal)
                
                if (self.playMusicBtn != nil) {
                    self.playMusicBtn .hideLoading()
                }
                
            }else{
                self.playMusicBtn.setImage(#imageLiteral(resourceName: "musicPlay"), for: .normal)
               
            }
            
            if AudioPlayer.isPlaying() == false,isManualluyStop == false{
                leftTime.text = "00:00"
                rightTime.text = "00:00"
            }else {
                if let value = AudioPlayer.musicDuration() as? Double ,value > 0 {
                    self.rightTime.text = self.timeFormatted(totalSeconds: Int(value))
                }
                self.musicProgress.value=Float(AudioPlayer.progress())
            }
            
            
          //  print(AudioPlayer.activeSong()?.musicName,self.model.musicName)
            if AudioPlayer.activeSong() != self.model{
                reflashView()
               
            }
        }
    }
    

    //窗口被拉下时 停止计时器
    override func viewDidDisappear(_ animated: Bool) {
        self.timeStop()
    }
    @IBAction func btnActionDownloadClicked(_ sender: UIButton) {
        if self.model.isOffline == "0" ,self.model.download_status == "0" {
            
            if Connectivity.connetivityAvailable() == false {
                HelperClass .showAlertMessage(title: errOfflinetitle, message: errOfflineDownload)
                return
            }
            self.saveTrackOffline(objMusic: self.model)
            self.model.isOffline = "1"
            
            HelperClass.shared().addObjectinGlobal(objMusic: self.model) { (sucess) in
                self .refreshRightButtn()
            }
            DownloadManager.shared().downloadSong(track: self.model) { (sucess, objmusic, error) in
                self.model.download_status = "1"
                
                 HelperClass.shared().addObjectinGlobal(objMusic: self.model) { (sucess) in
                     self .refreshRightButtn()
                }
                
            }
        }
        else if self.model.isOffline == "1" ,self.model.download_status == "0" {
            // waiting to download
        }
        else {
            HelperClass .SaveTrackData(objMusic: self.model, showProgress: true) { (sucess, objmusic) in
                self .refreshRightButtn()
            }
        }
    }
    func refreshRightButtn() {
         self.model = self.setButtomImage(objModel: self.model, btn: self.imgvw_status)
    }
    @IBAction func btnActionMenuClicked(_ sender: UIButton) {
           
           if self.arr_musicList.count > 0 {
               self.tbl_musicPlayer .scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
           }
       }
       @IBAction func btnActionMoreClicked(_ sender: UIButton) {
           if self.model ==  nil {
               return
           }

           let actionsheet = UIAlertController(title:  self.model.trackTitle, message: nil, preferredStyle:.actionSheet)
           
//           actionsheet.addAction(UIAlertAction(title: kAddtoPlayerQueue, style: .default, handler: { (action) -> Void in
//           }))
           
           actionsheet.addAction(UIAlertAction(title: kAddtoPlayList, style:.default, handler: { (action) -> Void in
               
               let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "AddToPlaylistVC") as! AddToPlaylistVC
               obj.objMusic = self.model
               self.present(obj.setNavigationBarController(), animated: true, completion: nil)
               
           }))
           if self.model.trackUserID.count < 1 {
               actionsheet.addAction(UIAlertAction(title: ksaveSong, style: .default, handler: { (action) -> Void in
                   HelperClass .SaveTrackData(objMusic: self.model, showProgress: true) { (sucess, objMusic) in
                       self.refreshRightButtn()
                   }
                    }))
           }
           if self.model.musictype == .Library , self.model.trackUserID.count > 0 {
               actionsheet.addAction(UIAlertAction(title: kRemoveFromLIbrary, style: .default, handler: { (action) -> Void in
                   if Connectivity.connetivityAvailable() == false {
                       HelperClass .showAlertMessage(title: errOfflinetitle, message: errChangedLibraryOffline)
                       return
                   }
                   let parameters = ["trackUserID":self.model.trackUserID] as [String : AnyObject]
                      WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.kDeleteLibrarySong, parameters:parameters, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
                          if isSuccess == true {
                              // need remove from local DB
                          }else{
                              HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
                          }
                      }
                }))
           }
           if self.model.musictype == .Deezer {
               actionsheet.addAction(UIAlertAction(title: kreportproblem, style: .default, handler: { (action) -> Void in
                   
                   let objPlayList = self.libraryStoryBoard().instantiateViewController(withIdentifier: "ReportProblemVC")as! ReportProblemVC
                   objPlayList.objActiveMusic = self.model
                   self.present(objPlayList.setNavigationBarController(), animated: true, completion: nil)
                   
               }))
           }
           if self.model.musictype == .Library {
               actionsheet.addAction(UIAlertAction(title: kRenameSongs, style: .default, handler: { (action) -> Void in
                   let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "AddPlaylistVC") as! AddPlaylistVC
                   obj.fromtype = .Songs
                   obj.objmusic = self.model
                   self.present(obj.setNavigationBarController(), animated: true, completion: nil)
                   
               }))
           }
           
           actionsheet.addAction(UIAlertAction(title: kViewArtistPage, style: .default, handler: { (action) -> Void in
               let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "ArtistDetailVC") as! ArtistDetailVC
            obj.objPlayList = PlayListModel(title: self.model.authorName, subtitle: "", url: self.model.authorImageUrl, id: self.model.authorID )
              obj.isFromPlayer = true
               self.present(obj.setNavigationBarController(), animated: true, completion: nil)
              // self.navigationController?.pushViewController(obj, animated: true)
           }))
        actionsheet.addAction(UIAlertAction(title: kSleepTimer, style: .default, handler: { (action) -> Void in
            let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "SleepTimerVC") as! SleepTimerVC
            self.present(obj.setNavigationBarController(), animated: true, completion: nil)
            
        }))
           actionsheet.addAction(UIAlertAction(title: kCancel, style: .cancel, handler: { (action) -> Void in
               
           }))
           self.present(actionsheet, animated: true, completion: nil)
           
           
       }

}
extension MusicPlayerVC:UITableViewDelegate,UITableViewDataSource
{
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_musicList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellsong = tableView.dequeueReusableCell(withIdentifier: "cell_songslist", for: indexPath) as! cell_songslist
        cellsong.btn_add .setImage(UIImage(named: "download"), for: .normal)
        let objData = self.arr_musicList[indexPath.row]
        cellsong.lbl_title.text =  objData.trackTitle
        cellsong.lbl_subtitle.text = objData.authorName
        if let strUrl = objData.trackImageUrl {
            cellsong.imgvw_photo .sd_setImage(with:URL(string:strUrl)) { (image, error, cacheType, url) in
            }
        }
        self.setTextColor(objModel: objData, label: cellsong.lbl_title)
        return cellsong
    }
    func  tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cellheader = tableView.dequeueReusableCell(withIdentifier: "Cell_tblheader") as! Cell_tblheader
        cellheader.lbl_header.text = "Listening"
        cellheader.btn_add.isHidden = true
        cellheader.lbl_header.font =  UIFont(name: RobotoBold, size: FontSizeBig)
        cellheader.lbl_header.textColor = UIColor(hexString:colorGreen)
        return cellheader;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let object = ["index":indexPath.row,kactiveMusic:self.musicArry[self.nowNum!]] as [String : Any]
        NotificationCenter.default.post(name:ChangeSonagNotification, object: nil, userInfo: object)
        self.nowNum = indexPath.row-1
        self.nextMusicBtn .sendActions(for: .touchUpInside)
        self.tbl_musicPlayer .reloadData()
    }
}
