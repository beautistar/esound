//
//  MainMusicViewController.swift
//  SwiftMusic
//
//  Created by 科文 on 2017/5/3.
//  Copyright © 2017年 科文. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
let kactiveMusic = "kactiveMusic"

let OpenPlayerNotification = Notification.Name("OpenPlayerNotification")
let ChangeSonagNotification = Notification.Name("ChangeSonagNotification")
let ChangedSongSucess = Notification.Name("ChangedSongSucess")
let ChangedSongSucessPlayerPage = Notification.Name("ChangedSongSucessPlayerPage")

let TimerforSleep = Notification.Name("TimerforSleep")

class MainMusicViewController: UIViewController,UITabBarDelegate {

    @IBOutlet weak var consHeightMiniPlayer: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var miniPlayerView: UIView!
    @IBOutlet weak var miniPlayerContainerView: UIView!
    @IBOutlet weak var miniPlayerButton: UIButton!
    @IBOutlet weak var tabBar: UITabBar!
    var lbl_currentValue:UILabel!
    var isDraging = Bool()
    
    var nowNum:Int!
    var Maintimer:Timer!
//    var objHomeNav:UINavigationController!
//    var objbrowse:UINavigationController!
//    var objlibrary:UINavigationController!
//    var objSearch:UINavigationController!
    var objpage:PageControlVC!
    
    
    //static var instance: MainMusicViewController!
    
   
    
   fileprivate var modalVC :MusicPlayerVC!
    private var animator : ARNTransitionAnimator?
    var musicModel:Music?
    var musicArry:Array<Music>!
 //   var isPlay : Bool!=false
    
    @IBAction func ClickMiniPlayerBtn(_ sender: Any) {
        if self.musicModel != nil {
            self.present(self.modalVC, animated: true, completion: nil)
        }
       
    }
//    class func shared() -> MainMusicViewController {
//
//           self.instance = (self.instance ?? MainMusicViewController())
//           return self.instance
//       }
    
    
    @IBAction func musicPlayBtn(_ sender: Any) {
        let sender=sender as! UIButton
//        if isPlay {
//            AudioPlayer.pause()
//            isPlay=false
//            sender.setImage(#imageLiteral(resourceName: "musicPlay"), for: .normal)
//        }else{
//            AudioPlayer.play()
//            isPlay=true
//            sender.setImage(#imageLiteral(resourceName: "stopMusic"), for: .normal)
//        }
        if AudioPlayer.play(){
            sender.setImage(#imageLiteral(resourceName: "stopMusic"), for: .normal)
            self.modalVC.isManualluyStop = false
        }else{
            sender.setImage(#imageLiteral(resourceName: "musicPlay"), for: .normal)
            self.modalVC.isManualluyStop = true
        }
    }
    
    func setStatusBarBackgroundColor(color : UIColor) {
        let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as? UIView)?.value(forKey: "statusBar") as? UIView
        statusBar?.backgroundColor=color
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let session:AVAudioSession=AVAudioSession.sharedInstance()
        do{
            try session.setCategory(AVAudioSession.Category.playback)
        try session.setActive(true)
        }
        catch{
        }
        
       
         self.tabBar.selectedItem = self.tabBar.items?.first;
        
        //1.告诉系统接受远程响应事件，并注册成为第一响应者
        UIApplication.shared.beginReceivingRemoteControlEvents()
       // self.becomeFirstResponder()
        self.tabBar.unselectedItemTintColor = UIColor.gray
        self.isDraging = false
      
       UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: RobotoBold, size: 12)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: RobotoBold, size: 12)!], for: .selected)
        
        self.consHeightMiniPlayer.constant = 0
       // self.miniPlayerView.backgroundColor = UIColor.lightGray
        self.miniPlayerView.isHidden = false
        self.miniPlayerView .layoutIfNeeded()
        
        if UIDevice.isPhone{
             self.miniPlayerContainerView.isHidden = true
            self.modalVC = self.libraryStoryBoard().instantiateViewController(withIdentifier: "DetailMusicViewController") as? MusicPlayerVC
        }else{
           
             self.modalVC.tbl_musicPlayer.isScrollEnabled = false
        }
    
      
        self.modalVC.modalPresentationStyle = .overFullScreen//弹出属性
       
        self.miniPlayerWork()
      //  self.musicArry=Music.getALL()
        self.tabBar.delegate=self
        
        //self.objHomeNav = self.tabbarStoryBoard().instantiateViewController(withIdentifier: "navhome") as? UINavigationController
//        self.objbrowse = self.tabbarStoryBoard().instantiateViewController(withIdentifier: "navbrowse") as? UINavigationController
//        self.objlibrary = self.tabbarStoryBoard().instantiateViewController(withIdentifier: "navlibrary") as? UINavigationController
//        self.objSearch = self.tabbarStoryBoard().instantiateViewController(withIdentifier: "navsearch") as? UINavigationController
        if UIDevice.isPad {
            self.consHeightMiniPlayer.constant = 150
            miniPlayerButton.isHidden = true
            
//            let listVc = self.libraryStoryBoard().instantiateViewController(withIdentifier: "MusicPlayerVC") as! MusicPlayerVC
//            self.addChild(listVc)
//            listVc.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
//            self.miniPlayerView.addSubview(listVc.view)
//            listVc.didMove(toParent: self)
        }else {
             self.setupAnimator()
        }
        NotificationCenter.default.addObserver(forName:OpenPlayerNotification, object: nil, queue: nil) { (objNotification) in
            self.consHeightMiniPlayer.constant = 50
            self.miniPlayerView.isHidden = false
            self.miniPlayerView .layoutIfNeeded()
            
            let obj = objNotification.userInfo
            let index = obj!["index"] as! Int
            let arrObject = obj!["object"]
            let arrobJectNew = arrObject as! Array<Music>
         
            for i in 0 ..< arrobJectNew.count {
                let music = arrobJectNew[i]
                music.musicNum = i
                
            }
                
            
            Music.shared().arr_musicArry = arrobJectNew
            self.musicArry = arrobJectNew
            self.modalVC.arr_musicList = arrobJectNew
            self.modalVC.musicArry = arrobJectNew
            self.setPlayerIndex(index: index)
            self.miniPlayerWork()
           // self.modalVC.isNewIndex = true
            self.miniPlayerButton .sendActions(for: .touchUpInside)
        }
        
        NotificationCenter.default.addObserver(forName:ChangeSonagNotification, object: nil, queue: nil) { (objNotification) in
            let obj = objNotification.userInfo
            let index = obj!["index"] as! Int
            self.setPlayerIndex(index: index)
        }
        NotificationCenter.default.addObserver(forName:TimerforSleep, object: nil, queue: nil) { (objNotification) in
            let second = objNotification.userInfo!["second"] as! Double
            objAppdelegate.timerSleep .invalidate()
            objAppdelegate.timerSleep = Timer.scheduledTimer(timeInterval:second, target:self, selector: #selector(self.stopPlayerfromSleepTimer), userInfo: nil, repeats: false)
        }
        
        NotificationCenter.default.addObserver(forName:ChangedSongSucess, object: nil, queue: nil) { (objNotification) in
            self.startTimer()
        }
        
        self.tabBar.shadowImage = UIImage()
        self.tabBar.backgroundImage = UIImage()
        
        if #available(iOS 13.0, *) {
            let appearance = self.tabBar.standardAppearance
            appearance.shadowImage = nil
            appearance.shadowColor = nil
            self.tabBar.standardAppearance = appearance
        } else {
            self.tabBar.shadowImage = UIImage()
            self.tabBar.backgroundImage = UIImage()
        }
        
       
    }
    func setPlayerIndex(index:Int) {
        self.musicModel =  self.modalVC.musicArry[index]
        self.modalVC.model = self.musicModel
        self.modalVC.nowNum = index
        self.nowNum = index
        
        let musicImg = self.miniPlayerView.viewWithTag(1) as! UIImageView
        let musicName = self.miniPlayerView.viewWithTag(2) as! UILabel
        
        musicName.text=self.musicModel?.trackTitle
        
        if let strUrl = self.musicModel?.trackImageUrl {
            musicImg .sd_setImage(with:URL(string:strUrl)) { (image, error, cacheType, url) in
            }
        }
        
        if self.isDraging == false {
           self.lbl_currentValue.text = "00:00"
        }

         self.modalVC.isNewIndex = true
       
    }
    @objc func stopPlayerfromSleepTimer() {
        if self.modalVC != nil {
            self.modalVC .dismiss(animated: true, completion: nil)
            AudioPlayer.stop()
            self.consHeightMiniPlayer.constant = 0
            self.miniPlayerView.isHidden = true
            self.miniPlayerView .layoutIfNeeded()
            objAppdelegate.objActiveMusic = nil;
        }
    }
    func setupAnimator(){
        let animation=MusicPlayerTransitionAnimation(rootVC: self, modalVC: self.modalVC)
        animation.completion = { [weak self] isPresenting in
            if isPresenting {
                guard let _self=self else{ return }//再次强引用
                let modalGestureHandler=TransitionGestureHandler (targetVC: _self, direction: .bottom)
                modalGestureHandler.registerGesture(_self.modalVC.view_tapgesture)
                modalGestureHandler.panCompletionThreshold=15.0
                _self.animator?.registerInteractiveTransitioning(.dismiss, gestureHandler: modalGestureHandler)
            }else{
                
                self?.setupAnimator()
            }
        }
        
            let miniPlayerGestureHandler = TransitionGestureHandler(targetVC: self, direction: .top)
            miniPlayerGestureHandler.registerGesture(self.miniPlayerView)
            miniPlayerGestureHandler.panCompletionThreshold=15.0
            
            self.animator=ARNTransitionAnimator(duration: 0.5, animation: animation)
            self.animator?.registerInteractiveTransitioning(.present, gestureHandler: miniPlayerGestureHandler)
            self.modalVC.transitioningDelegate=self.animator
    }

    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        objpage.setPageIndex(index: item.tag - 1)
        return;
        /*
        if item.tag == 1 {
            let newController = self.objHomeNav!
            
            let oldController = self.children.first!
            if newController != oldController {
              //  self.setStatusBarBackgroundColor(color: UIColor.white)
                oldController.willMove(toParent: nil)
                addChild(newController)
                newController.view.frame = oldController.view.frame
                
                
                
                transition(from: oldController, to: newController, duration: 0.0, options: .init(), animations: nil, completion: { (finished) -> Void in
                    oldController.removeFromParent()
                    newController.didMove(toParent: self)                
                })
            }
        }
        else if item.tag == 2 {
            let newController = self.objbrowse!
            
            let oldController = self.children.first!
            if newController != oldController {
                oldController.willMove(toParent: nil)
                addChild(newController)
                newController.view.frame = oldController.view.frame
                
                //isAnimating = true
                transition(from: oldController, to: newController, duration: 0.0, options: .init(), animations: nil, completion: { (finished) -> Void in
                    oldController.removeFromParent()
                    newController.didMove(toParent: self)
                })
            }
        }
        else if item.tag == 3 {
            let newController = self.objlibrary!
            
            let oldController = self.children.first!
            if newController != oldController {
                oldController.willMove(toParent: nil)
                addChild(newController)
                newController.view.frame = oldController.view.frame
                
                //isAnimating = true
                transition(from: oldController, to: newController, duration: 0.0, options: .init(), animations: nil, completion: { (finished) -> Void in
                    oldController.removeFromParent()
                    newController.didMove(toParent: self)
                })
            }
        }
        else{
            let newController = self.objSearch!
            let oldController = self.children.first!
             if newController != oldController {
           //     self.setStatusBarBackgroundColor(color: UIColor.clear)
                oldController.willMove(toParent: nil)
                addChild(newController)
                newController.view.frame = oldController.view.frame
                
                transition(from: oldController, to: newController, duration: 0.0, options:.init(), animations: nil, completion: { (finished) -> Void in
                    oldController.removeFromParent()
                    newController.didMove(toParent: self)
                })
            }
        }
 */
    }
    
    // MARK: - Navigation

   //  In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "gohome" {
//            self.objHomeNav = segue.destination as? UINavigationController
//        }
        if segue.identifier == "musicplayer" {
            self.modalVC = segue.destination as? MusicPlayerVC
           
         }
        if segue.identifier == "gopage" {
            self.objpage = segue.destination as? PageControlVC
        }
        
    }
    
    func miniPlayerWork(){
        let musicImg = self.miniPlayerView.viewWithTag(1) as! UIImageView
     //   let musicName = self.miniPlayerView.viewWithTag(2) as! UILabel
        let musicPlayBtn = self.miniPlayerView.viewWithTag(3) as! UIButton
   //    let nextMusicBtn = self.miniPlayerView.viewWithTag(4) as! UIButton
        lbl_currentValue = self.miniPlayerView.viewWithTag(5) as? UILabel
        lbl_currentValue.font = UIFont(name: RobotoRegular, size: FontSizeSmall)
        lbl_currentValue.textColor = UIColor(hexString: colorGreen)
        
        musicImg.layer.cornerRadius = 8
        musicImg.layer.masksToBounds = true
        
       
       
        
        if self.musicModel != nil {
//            musicName.text=self.musicModel?.musicName
//            musicImg.sd_setImage(with:self.musicModel?.urlimage) { (image, error, cacheType, url) in
//            }
            if ((self.modalVC?.playMusicBtn) != nil) {
                self.modalVC.playMusicBtn .showLoading()
            }
            
            AudioPlayer.shareMusic(model: self.musicModel!) { (sucess) in
                if sucess {
                    self.modalVC.model=self.musicModel
                    if AudioPlayer.isPlaying(){
                        musicPlayBtn.setImage(#imageLiteral(resourceName: "stopMusic"), for: .normal)
                        if ((self.modalVC?.playMusicBtn) != nil) {
                            self.modalVC.playMusicBtn .hideLoading()
                        }
                    }else{
                        musicPlayBtn.setImage(#imageLiteral(resourceName: "musicPlay"), for: .normal)
                      
                    }
                   // if UIDevice.isPad {
                        self.modalVC .reflashView()
                      NotificationCenter.default.post(name:ChangedSongSucessPlayerPage, object: nil, userInfo: nil)
                   // }
                   
                   self .startTimer()
                }
            }
            

        }
        
    }
    func startTimer() {
        if self.Maintimer == nil{
            self.Maintimer=Timer .scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.setBackground), userInfo: nil, repeats: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
        
       
       
    }
    override func viewWillDisappear(_ animated: Bool) {
     //   refreashView()
        UIApplication.shared.endReceivingRemoteControlEvents()
        self.resignFirstResponder()
        
    }
    
    override var canBecomeFirstResponder: Bool{
        return true
    }
    
    func refreashView(){
        self.musicModel=AudioPlayer.activeSong()
        if self.musicModel != nil {
      //  DispatchQueue.main.async(execute: {
            
            self.nowNum=(self.musicModel?.musicNum)!
            let musicImg=self.miniPlayerView.viewWithTag(1) as! UIImageView
            let musicName = self.miniPlayerView.viewWithTag(2) as! UILabel
            let musicPlayBtn = self.miniPlayerView.viewWithTag(3) as! UIButton
            musicName.text=self.musicModel?.trackTitle
            musicImg .sd_setImage(with:URL(string:self.musicModel!.trackImageUrl!)) { (image, error, cacheType, url) in
                       }
            
            if let strUrl = self.musicModel?.trackImageUrl {
                musicImg .sd_setImage(with:URL(string:strUrl)) { (image, error, cacheType, url) in
                }
            }
         
            if AudioPlayer.isPlaying(){
                musicPlayBtn.setImage(#imageLiteral(resourceName: "stopMusic"), for: .normal)
            }else{
                musicPlayBtn.setImage(#imageLiteral(resourceName: "musicPlay"), for: .normal)
            }
           
            if self.Maintimer == nil{ // new kirti
                self.Maintimer=Timer .scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.setBackground), userInfo: nil, repeats: true)
            }
        }
    }
    
}
extension MainMusicViewController {
    @objc func setBackground() {
        //大标题 - 小标题  - 歌曲总时长 - 歌曲当前播放时长 - 封面
        self.musicModel=AudioPlayer.activeSong()
        var settings = [MPMediaItemPropertyTitle: self.musicModel!.trackTitle,
                        MPMediaItemPropertyArtist: self.musicModel!.authorName ,
                        MPNowPlayingInfoPropertyElapsedPlaybackTime: "\(AudioPlayer.currentTime())",
            MPMediaItemPropertyPlaybackDuration: 90,
            MPMediaItemPropertyArtwork: MPMediaItemArtwork.init(image:UIImage(named: "moon")!)
            ] as [String : Any]
        
        
        
//        let imageview = UIImageView()
//        imageview .sd_setImage(with:self.musicModel?.urlimage) { (image, error, cache, url) in
//            settings = [MPMediaItemPropertyArtwork: MPMediaItemArtwork.init(image:image!)]
//        }
//        if let value = AudioPlayer.musicDuration() as? Double ,value > 0 {
//              settings = [MPMediaItemPropertyPlaybackDuration: value]
//        }
        
//        var settings = [MPMediaItemPropertyTitle: self.musicModel?.musicName,
//                               MPMediaItemPropertyArtist: self.musicModel?.musicAuthor ,
//                               MPMediaItemPropertyPlaybackDuration: "\(AudioPlayer.musicDuration())",
//                   MPNowPlayingInfoPropertyElapsedPlaybackTime: "\(AudioPlayer.currentTime())"] as [String : Any]
//
        MPNowPlayingInfoCenter.default().setValue(settings, forKey: "nowPlayingInfo")
        if self.isDraging == false {
            self.lbl_currentValue.text = self.timeFormatted(totalSeconds: Int(AudioPlayer.currentTime()))
            if AudioPlayer.isPlaying() == false,self.modalVC.isManualluyStop == false {
                self.lbl_currentValue.text = "00:00"
            }
               let musicPlayBtn = self.miniPlayerView.viewWithTag(3) as! UIButton
            if AudioPlayer.isPlaying(){
                musicPlayBtn.setImage(#imageLiteral(resourceName: "stopMusic"), for: .normal)
            }else{
                musicPlayBtn.setImage(#imageLiteral(resourceName: "musicPlay"), for: .normal)
            }
        }

        if AudioPlayer.progress() > 0.99 {
            self.nowNum=self.nowNum+1
            if self.nowNum>=self.musicArry.count{
                self.nowNum=0
            }
            print("Number:",self.nowNum!)
             self.timeStop()
            let object = ["index":self.nowNum!,kactiveMusic:self.musicArry[self.nowNum!]] as [String : Any]
            NotificationCenter.default.post(name:ChangeSonagNotification, object: nil, userInfo: object)
            self.modalVC.isNewIndex = true
            AudioPlayer .nextsongMusic(num: self.nowNum) { (sucess) in
                if sucess {
                    NotificationCenter.default.post(name:ChangedSongSucessPlayerPage, object: nil, userInfo: nil)
                    self.refreashView()
                }
            }
            
//            if AudioPlayer.nextsong(num: self.nowNum){
//                refreashView()
//            }
        }
               
        
    }
    func timeStop(){
        if Maintimer != nil{
            self.Maintimer.invalidate()
            self.Maintimer=nil
        }
    }
}
extension MainMusicViewController {
    override func remoteControlReceived(with event: UIEvent?) {
        
        if event?.type == UIEvent.EventType.remoteControl {
            switch event!.subtype {
            case .remoteControlTogglePlayPause:
                 if AudioPlayer.play(){
                }
            case .remoteControlPreviousTrack: // ##  <-  ##
                self.nowNum=self.nowNum-1
                if self.nowNum == -1{
                    self.nowNum=self.musicArry.count - 1
                }
                
                AudioPlayer.prevsongMusic(num: self.nowNum) { (sucess) in
                    if sucess {
                        self.musicModel=AudioPlayer.activeSong()
                        self.refreashView()
                    }
                }
                
//                if AudioPlayer.prevsong(num: self.nowNum){
//                    self.musicModel=AudioPlayer.activeSong()
//                    refreashView()
//                }

            case .remoteControlNextTrack: // ## -> ##
                self.nowNum=self.nowNum+1
                if self.nowNum>=self.musicArry.count{
                    self.nowNum=0
                }
                
                AudioPlayer.nextsongMusic(num: self.nowNum) { (sucess) in
                    if sucess {
                        self.musicModel=AudioPlayer.activeSong()
                        self.refreashView()
                    }
                }
                
//                if AudioPlayer.nextsong(num: self.nowNum){
//                    self.musicModel=AudioPlayer.activeSong()
//                    refreashView()
//                }
            case .remoteControlPlay: // ## > ##
                if AudioPlayer.play(){
                     let musicPlayBtn = self.miniPlayerView.viewWithTag(3) as! UIButton
                    if AudioPlayer.isPlaying(){
                        musicPlayBtn.setImage(#imageLiteral(resourceName: "stopMusic"), for: .normal)
                    }else{
                        musicPlayBtn.setImage(#imageLiteral(resourceName: "musicPlay"), for: .normal)
                    }
                }else{
                   
                }

            case .remoteControlPause: // ## || ##
               AudioPlayer.pause()
                 let musicPlayBtn = self.miniPlayerView.viewWithTag(3) as! UIButton
               if AudioPlayer.isPlaying(){
                musicPlayBtn.setImage(#imageLiteral(resourceName: "stopMusic"), for: .normal)
               }else{
                musicPlayBtn.setImage(#imageLiteral(resourceName: "musicPlay"), for: .normal)
                }
            default:
                break
            }
        }
    }
}
