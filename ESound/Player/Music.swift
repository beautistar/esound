//
//  Music.swift
//  SwiftMusic
//
//  Created by 科文 on 2017/5/2.
//  Copyright © 2017年 科文. All rights reserved.
//

import UIKit
import AVFoundation

enum MusicType {
    case Deezer
    case Library
    case Youtube
}

class Music: NSObject {

    static var instance: Music!
    class func shared() -> Music {
        
        self.instance = (self.instance ?? Music())
        return self.instance
    }
    var trackTitle = String()
  //  var musicURL :URL?
    var trackImageUrl :String?
    
    var authorName = String()
    var authorID =  String()
    var authorImageUrl =  String()
    
    var musicNum:Int?
    var isActive:Bool=false
    var deezerTrackID :String?
    var ytMusic = Bool()
    var musictype:MusicType?
    var trackID = String()
    var addedDate = String()
    var authorRenameName = String()
    var isOffline = String()
    var listenDate = String()
    var listenTimes = String()
    var titleRename = String()
    var trackReference = String()
    var trackUserID = String()
    var playlistPos =  String()
    var tracktype  = Int()
    
    var file: String?
    var download_status = String()
    
    var arr_musicArry = [Music]()
    private static var musicArry = [Music]()
    static func getALL()->[Music]{
        return Music.shared().arr_musicArry
    }

}
