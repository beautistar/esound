//
//  AudioPlayer.swift
//  SwiftMusic
//
//  Created by 科文 on 2017/5/4.
//  Copyright © 2017年 科文. All rights reserved.
//

import UIKit
import AVFoundation
final class AudioPlayer: NSObject {
   // private static var instance: AVAudioPlayer? = nil //static 直到被销毁 全局存在
    private static var activeMusic:Music?=nil
    private static var isRandomPlay=false
    
     private static var instance:AVPlayer?

 //  private static var instance : AVQueuePlayer? = nil
//    static func share(model:Music) -> Bool {
//
//        instance = AVPlayer(playerItem: AVPlayerItem(url:model.musicURL!))
//        instance?.play()
//        activeMusic=model
//
//        return true
//    }
    static func getVideoSearchFromYoutube(model:Music,completionHandler: @escaping ConnectivityHandler)  {
        HelperClass .getVideoiDFromYoutubeSearch(objPlayList: model) { (arrvideoid) in
            if (arrvideoid.count > 0 )  {
                let dict = arrvideoid[0]
                model.trackID = (dict["trackId"] as? String)!
                DaiYoutubeParser .parse(model.trackID, screenSize: CGSize.zero, videoQuality: DaiYoutubeParserQualitySmall) { (status, url, videotitle, duration) in
                    if (url != nil) {
                        print(url!)
                        self .getMusicObject(url:URL(string: url!)!, model: model)
                        completionHandler(true)
                    }else {
                        
                        if (arrvideoid.count > 1 )  {
                            let dict = arrvideoid[1]
                            model.trackID = (dict["trackId"] as? String)!
                            DaiYoutubeParser .parse(model.trackID, screenSize: CGSize.zero, videoQuality: DaiYoutubeParserQualityMedium) { (status, url, videotitle, duration) in
                                if (url != nil) {
                                    print(url!)
                                    self .getMusicObject(url:URL(string: url!)!, model: model)
                                    completionHandler(true)
                                }else {
                                    completionHandler(false)
                                }
                            }
                        }
                    }
                }
                
            }else {
                completionHandler(false)
            }
        }
        
    }
    static func shareMusic(model:Music,completionHandler: @escaping ConnectivityHandler)  {
    
        let obj = objAppdelegate.arr_LocaltrackLists.first { (obj) -> Bool in
            return model.deezerTrackID == obj.deezerTrackID
        }
        if obj !=  nil {
            if obj?.download_status == "1" {
               let filePath = DownloadManager.shared().getPath(filename: obj?.file, With:kDownloadFolderName)
                self .getMusicObject(url:URL(fileURLWithPath: filePath), model: obj!)
                completionHandler(true)
                return
            }
        }
      ///  Music.getALL().append(<#T##newElement: Music##Music#>)
        
        
        
        if model.musictype == MusicType.Library {
            DaiYoutubeParser .parse(model.trackID, screenSize: CGSize.zero, videoQuality: DaiYoutubeParserQualitySmall) { (status, url, videotitle, duration) in
                if (url != nil) {
                    self .getMusicObject(url:URL(string: url!)!, model: model)
                    completionHandler(true)
                }else {
                    completionHandler(false)
                }
            }
        }else {
            HelperClass .getVideoiDFromApi(Deezerid:"\(model.deezerTrackID!)") { (arrvideoid) in
                if (arrvideoid.count > 0)  {
                     let dict = arrvideoid[0]
                    model.trackID = (dict["trackId"] as? String)!
                    DaiYoutubeParser .parse(model.trackID, screenSize: CGSize.zero, videoQuality: DaiYoutubeParserQualitySmall) { (status, url, videotitle, duration) in
                        if (url != nil) {
                            self .getMusicObject(url:URL(string: url!)!, model: model)
                            completionHandler(true)
                        }else {
                            if (arrvideoid.count > 1 )  {
                                let dict = arrvideoid[1]
                                model.trackID = (dict["trackId"] as? String)!
                                DaiYoutubeParser .parse(model.trackID, screenSize: CGSize.zero, videoQuality: DaiYoutubeParserQualitySmall) { (status, url, videotitle, duration) in
                                    if (url != nil) {
                                        self .getMusicObject(url:URL(string: url!)!, model: model)
                                        completionHandler(true)
                                    }else {
                                        self .getVideoSearchFromYoutube(model: model) { (response) in
                                            completionHandler(response)
                                        }
                                    }
                                }
                            }else {
                                self .getVideoSearchFromYoutube(model: model) { (response) in
                                    completionHandler(response)
                                }
                            }
                        }
                    }
                    
                } else {
                    self .getVideoSearchFromYoutube(model: model) { (response) in
                        completionHandler(response)
                    }
                }
            }
        }
       
    }
    static func getMusicObject(url:URL,model:Music) {
        
        HelperClass .saveTrackApi(obj: model, inUserAccount: false, showProgress: true) { (sucess, objmusic) in
                 // self.tableView .reloadData()
               }
    
        instance = AVPlayer(playerItem: AVPlayerItem(url:url))
        instance?.play()
        activeMusic=model
    }
    static func nextsongMusic( num:Int,completionHandler: @escaping ConnectivityHandler){
        var num = num
        var musicArry:Array<Music>!
        musicArry=Music.getALL()
        if isRandomPlay{
            num = Int(arc4random_uniform(UInt32(musicArry.count-1)))
        }
        
        shareMusic(model: musicArry[num]) { (sucess) in
            completionHandler(sucess)
        }
        
    }
     
    static func prevsongMusic(num:Int,completionHandler: @escaping ConnectivityHandler){
           var num = num
           var musicArry:Array<Music>!
           musicArry=Music.getALL()
           if isRandomPlay{
               num = Int(arc4random_uniform(UInt32(musicArry.count-1)))
           }
        
           shareMusic(model:musicArry[num]) { (sucess) in
               completionHandler(sucess)
           }
       }
   
    //停止
    static func stop(){
         instance?.pause()
       // instance?.stop()
    }
    //播放
    static func play()->Bool{
        
        if instance?.timeControlStatus == .playing {
            instance?.pause()
            return false
            
        } else if instance?.timeControlStatus == .paused {
            instance?.play()
             return true
        }
         return false
        
//        if (instance?.isPlaying)! {
//            instance?.pause()
//            return false
//        }else{
//            instance?.play()
//            return true
//        }
    }
    //暂停
    static func pause(){
        instance?.pause()
    }
    //下一曲
//    static func nextsong( num:Int)->Bool{
//        var num = num
//        var musicArry:Array<Music>!
//        musicArry=Music.getALL()
//        if isRandomPlay{
//           num = Int(arc4random_uniform(UInt32(musicArry.count-1)))
//        }
//        if(share(model: musicArry[num])){
//            return true
//        }else{
//            return false
//        }
//      
//    }
   
//    static func prevsong(num:Int)->Bool{
//        var num = num
//        var musicArry:Array<Music>!
//        musicArry=Music.getALL()
//        if isRandomPlay{
//            num = Int(arc4random_uniform(UInt32(musicArry.count-1)))
//        }
//        if(share(model: musicArry[num])){
//            return true
//        }else{
//            return false
//        }
//    }
    //声音控制
    static func voice(num:Float){
        instance?.volume=num
    }
    //进度条相关
    static func progress()->Double{
        
        let currentItem = instance?.currentItem
        return (currentItem!.currentTime().seconds)/(currentItem!.duration.seconds)
        
        
       // return (instance?.currentTime)!/(instance?.duration)!
    }
    static func musicDuration()->Double{
        
          let currentItem = instance?.currentItem
         return currentItem!.duration.seconds
        
       // return (instance?.duration)!
       
    }
    static func skipTime(time:Double){
        
        guard let duration  = self.instance?.currentItem?.duration else{
            return
        }
       
        let playerCurrentTime = CMTimeGetSeconds((self.instance?.currentTime())!)
        
        if (playerCurrentTime > time) {
            var newTime = playerCurrentTime - time
            if newTime < 0 {
                newTime = 0
            }
            let time2: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
            self.instance?.currentItem?.seek(to: time2, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
            
        }else{
            
//            let newTime =  time - playerCurrentTime
//            if newTime < CMTimeGetSeconds(duration) {
                let time2: CMTime = CMTimeMake(value: Int64(time * 1000 as Float64), timescale: 1000)
                self.instance?.currentItem?.seek(to: time2, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
                
            //}
        }
        
        
        //instance?.currentTime = time
    }
    static func currentTime()->Double{
        
            let currentItem = instance?.currentItem
            return currentItem!.currentTime().seconds

      //  return (instance?.currentTime)!
    }
 
    //当前播放的音乐
    static func activeSong()->Music?{
        return activeMusic
    }
    //是否在播放音乐
    static func isPlaying()->Bool{
        
        if instance?.timeControlStatus == .playing {
            return true
        }else{
            return false
        }
        
        //return (instance?.isPlaying)!
    }
    //随机播放
    static func musicRandomPlay()->Bool{
        if  isRandomPlay==false{
            isRandomPlay=true
            return isRandomPlay
        }else{
            isRandomPlay=false
            return isRandomPlay
        }
    }
    
}
