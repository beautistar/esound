//
//  FaqVC.swift
//  ESound
//
//  Created by kirti on 29/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class FaqVC: BaseClassVC {

    @IBOutlet weak var tbl_faq: UITableView!
      var arr_download = [[String:String]]()
      var arr_streaming = [[String:String]]()
      var arr_search = [[String:String]]()
      var arr_library = [[String:String]]()
      var arr_premium = [[String:String]]()
    
     var selectedIndexPath = IndexPath()
    
    @IBOutlet weak var lbl_header: UILabel!
    @IBOutlet weak var btn_contactUs: UIButton!
    @IBOutlet weak var btn_cancel: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arr_download .append([kFAQ.faq1:kFAQ.faq1c])
        arr_download .append([kFAQ.faq12:kFAQ.faq12c])
        
        arr_streaming .append([kFAQ.faq3:kFAQ.faq3c])
        arr_streaming .append([kFAQ.faq10:kFAQ.faq10c])
        arr_streaming .append([kFAQ.faq9:kFAQ.faq9c])
        arr_streaming .append([kFAQ.faq8:kFAQ.faq8c])
        
        arr_search .append([kFAQ.faq2:kFAQ.faq2c])
        arr_search .append([kFAQ.faq4:kFAQ.faq4c])
        
        arr_library .append([kFAQ.faq6:kFAQ.faq6c])
        arr_library .append([kFAQ.faq7:kFAQ.faq7c])
        arr_library .append([kFAQ.faq11:kFAQ.faq11c])
        
        arr_premium .append([kFAQ.faq5:kFAQ.faq5c])
        
        let cell_header = UINib(nibName: "Cell_tblheader", bundle: nil)
        self.tbl_faq.register(cell_header, forCellReuseIdentifier: "Cell_tblheader")
        
        self.btn_cancel .setButtonWithBorderAndCorner()
        self.btn_contactUs .setButtonWithGreenColor()
        
        self.lbl_header.text = kFAQ.faqIntro
        
        self.navigationItem.title = "Frequently asked questions"

        // Do any additional setup after loading the view.
    }
    @IBAction func btnActionContactClicked(_ sender: UIButton) {
        UIApplication.shared.open(URL(string: ksupport)!, options: [:]) { (true) in
                       }
    }
    @IBAction func btnActionCancelClicked(_ sender: UIButton) {
        self .dismiss(animated: true, completion: nil)
    }
    


}
extension FaqVC:UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return arr_download.count
        }
        else if section == 1{
            return arr_streaming.count
        }
        else if section == 2{
            return arr_search.count
        }
        else if section == 3{
            return arr_library.count
        }
        else if section == 4{
            return arr_premium.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_tblheader") as! Cell_tblheader
        cell.btn_add.isHidden = true
        if section == 0 {
            cell.lbl_header.text = kFAQ.faqSection1
        }
        else if section == 1 {
            cell.lbl_header.text = kFAQ.faqSection2
        }
        else if section == 2 {
            cell.lbl_header.text = kFAQ.faqSection3
        }
        else if section == 3 {
            cell.lbl_header.text = kFAQ.faqSection4
        }
        else if section == 4 {
            cell.lbl_header.text = kFAQ.faqSection5
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let lbl_que = cell.contentView .viewWithTag(10) as! UILabel
        let lbl_ans = cell.contentView .viewWithTag(11) as! UILabel
        
        lbl_que.font =  UIFont.init(name: RobotoRegular, size: FontSizeNormal)
        lbl_ans.font =  UIFont.init(name: RobotoRegular, size: FontSizeNormal)
        var dict = [String:String]()
        if indexPath.section == 0 {
            dict = arr_download[indexPath.row]
        }
        else if indexPath.section == 1 {
             dict = arr_streaming[indexPath.row]
        }
        else if indexPath.section == 2 {
             dict = arr_search[indexPath.row]
        }
        else if indexPath.section == 3 {
             dict = arr_library[indexPath.row]
        }
        else if indexPath.section == 4 {
             dict = arr_premium[indexPath.row]
        }
        lbl_que.text = dict.keys.first
        
        if selectedIndexPath == indexPath {
            lbl_ans.text = dict.values.first
        }else {
            lbl_ans.text = ""
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        self.tbl_faq .reloadData()
    }
}
