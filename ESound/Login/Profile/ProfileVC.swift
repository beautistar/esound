//
//  ProfileVC.swift
//  ESound
//
//  Created by kirti on 13/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {

   
    var arr_profile = [String]()
    var arr_app = [String]()
    var arr_contacts = [String]()
    @IBOutlet weak var tbl_profile: UITableView!
    @IBOutlet weak var btn_premium: UIButton!
    @IBAction func btnActionPremiumClicked(_ sender: UIButton) {
    }
    @IBOutlet weak var lbl_userName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Profile"
        
        self.lbl_userName.text  = "Welcome Back,Test \n UserName"
        
        self.lbl_userName.font = UIFont(name: RobotoBold, size: 28)
        
        let menuButton = UIBarButtonItem(image: UIImage(named: "moon"), style: .plain, target: self, action: #selector(btnActionMoonClicked))
        self.navigationItem.rightBarButtonItem  = menuButton
        
        self.addBackbutton()
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        if UserDefaults.standard.bool(forKey:kisExplictOff){
             arr_profile .append(kProfile.enableExplicit)
        }else {
            arr_profile .append(kProfile.disableExplicit)
        }
        
        arr_profile .append("Log out")
        arr_profile .append("Restore purchases")
        
        arr_app .append("Toggle dark theme")
        arr_app .append(kProfile.twitterpage)
        arr_app .append(kProfile.enableVolumeSlider)
        arr_app .append(kProfile.reloadandsynclib)
        
        arr_contacts .append("Frequenlty asked questions")
        arr_contacts .append("Contact the developers")
        arr_contacts .append("Review the app")
        arr_contacts .append(kProfile.facebookpage)
        arr_contacts .append(kProfile.instagrampage)
        arr_contacts .append(kProfile.twitterpage)
        
        arr_contacts .append("Terms and Conditions")
        arr_contacts .append("Privacy Terms")
        if let text = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
          arr_contacts .append("Version \(text)")
        }
       
        self.tbl_profile.rowHeight = 55
        self.tbl_profile.estimatedRowHeight = 55
        
        let cell_header = UINib(nibName: "Cell_tblheader", bundle: nil)
        self.tbl_profile.register(cell_header, forCellReuseIdentifier: "Cell_tblheader")
        
        self.btn_premium .setButtonWithGreenColor()
        self.btn_premium.layer.cornerRadius = 10
        self.btn_premium.clipsToBounds = true
        
        

      
    }
    @objc func btnActionMoonClicked() {
        
    }
    func setExplictt() {
        arr_profile .remove(at: 0)
        if UserDefaults.standard.bool(forKey:kisExplictOff){
            arr_profile .insert(kProfile.enableExplicit, at: 0)
        }else {
            arr_profile .insert(kProfile.disableExplicit, at: 0)
        }
        tbl_profile .reloadData()
    }

    

}
extension ProfileVC:UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return arr_profile.count
        }
        else if section == 1{
             return arr_app.count
        }
        else if section == 2{
             return arr_contacts.count
        }
       return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_tblheader") as! Cell_tblheader
        cell.btn_add.isHidden = true
        if section == 0 {
            cell.lbl_header.text = "Your profile"
        }
        else if section == 1 {
            cell.lbl_header.text = "App"
        }
        else if section == 2 {
            cell.lbl_header.text = "Contacts"
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let lbl_name = cell.contentView .viewWithTag(10) as! UILabel
        lbl_name.font =  UIFont.init(name: RobotoRegular, size: FontSizeNormal)
        
        if indexPath.section == 0 {
            lbl_name.text = arr_profile[indexPath.row]
        }
        else if indexPath.section == 1 {
            lbl_name.text = arr_app[indexPath.row]
        }
        else if indexPath.section == 2 {
            lbl_name.text = arr_contacts[indexPath.row]
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            let title =  arr_profile[indexPath.row]
            if indexPath.row == 0 {
                if title == kProfile.enableExplicit {
                    let alertController = UIAlertController(title:  kProfile.enableExplicit, message: kProfile.toggleExplicitDesc, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default) { (aciton) in
                        self .gotoExplictOfflineApi(off: false)
                    }
                    let cancelAction = UIAlertAction(title: kCancel, style: .cancel) { (aciton) in
                    }
                    alertController.addAction(okAction)
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                else if title == kProfile.disableExplicit {
                      self .gotoExplictOfflineApi(off: true)
                }
            }
            else if indexPath.row == 1 {
                let alertController = UIAlertController(title: "Log out from your account", message: "eSound will not automatically log you anymore and your downloaded music will be locally removed.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default) { (aciton) in
                    UserDefaults.standard.set(false, forKey: isLogged)
                    UserDefaults.standard.synchronize()
                    let objtabbar = self.storyboard!.instantiateViewController(withIdentifier:"LoginHomeVC") as! LoginHomeVC
                    objAppdelegate.window?.rootViewController  = objtabbar
                    self.dismiss(animated: true, completion: nil)
                }
                let cancelAction = UIAlertAction(title:kCancel, style: .cancel) { (aciton) in
                }
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
            
        }
        if indexPath.section == 1 {
            let title =  arr_app[indexPath.row]
            if indexPath.row == 0 {
                // light mode
            }
            else if indexPath.row == 1 {
                // timer
            }
            else if indexPath.row == 2 {
                // timer
            }
            else if indexPath.row == 2 {
                           // timer
                       }
            
        }
        else if indexPath.section == 2 {
        
            if indexPath.row == 0 {
                let objfaq = self.storyboard?.instantiateViewController(withIdentifier: "FaqVC") as! FaqVC
                self.present(objfaq .setNavigationBarController(), animated: true, completion: nil)
            }
            else if indexPath.row == 1 {
                UIApplication.shared.open(URL(string: ksupport)!, options: [:]) { (true) in
                }
            }
            else if indexPath.row == 2 {
                UIApplication.shared.open(URL(string: kstoreLink)!, options: [:]) { (true) in
                }
            }
            else if indexPath.row == 3 {
                UIApplication.shared.open(URL(string: ksocialFB)!, options: [:]) { (true) in
                }
            }
            else if indexPath.row == 4 {
                UIApplication.shared.open(URL(string: ksocialIG)!, options: [:]) { (true) in
                }
            }
            else if indexPath.row == 5 {
                UIApplication.shared.open(URL(string: ksocialTW)!, options: [:]) { (true) in
                }
            }
            else if indexPath.row == 6 {
                UIApplication.shared.open(URL(string: kterms)!, options: [:]) { (true) in
                }
            }
            else if indexPath.row == 7 {
                UIApplication.shared.open(URL(string: kprivacy)!, options: [:]) { (true) in
                }
            }
        }
    }
    func gotoExplictOfflineApi(off:Bool){
        let parameters = [
            "userID":UserDefaults.standard.string(forKey: kUserid)!,
            "off": off] as [String : AnyObject]
        WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.kUserToggleExplicit, parameters:parameters, encodingType: DefaultEncoding,ShowProgress:false) { (isSuccess, dictData) in
            if isSuccess == true {
                if let dict = dictData .object(forKey:"data") as? [String:AnyObject]{
                    if let data = dict["user"] as? [String:AnyObject] {
                        
                    }
                    
                }
            }else{
                HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
            }
        }
        
        UserDefaults.standard .set(!off, forKey: kisExplictOff)
        self .setExplictt()
    }
}
