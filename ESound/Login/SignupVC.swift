//
//  SignupVC.swift
//  ESound
//
//  Created by kirti on 13/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class SignupVC: UIViewController {

    @IBOutlet weak var text_password: UITextField!
    @IBOutlet weak var text_name: UITextField!
    @IBOutlet weak var text_email: UITextField!
    @IBOutlet weak var text_surname: UITextField!
    @IBOutlet weak var btn_signup: UIButton!
     @IBOutlet weak var btn_acceptTerms: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
          self.navigationItem.title = "Sign up"
        
        self.text_password .addButtonWithCornerBorder()
        self.text_email .addButtonWithCornerBorder()
        
        self.text_name .addButtonWithCornerBorder()
        self.text_surname .addButtonWithCornerBorder()
        
        self.text_email .LeftviewImage(name: "email")
        self.text_password .LeftviewImage(name: "key")
        self.text_name .LeftviewImage(name: "user")
        self.text_surname .LeftviewImage(name: "user")
        
        
        self.btn_signup .setButtonWithGreenColor()
        self.btn_signup .setCornerRound()
        
        
        let barButton = UIBarButtonItem(image: UIImage(named: "down"), style: .plain, target: self, action: #selector(btnAtionBackClicked))
        self.navigationItem.leftBarButtonItem  = barButton
        

        // Do any additional setup after loading the view.
    }
    @objc func btnAtionBackClicked() {
        self .dismiss(animated: true, completion: nil)
    }

    @IBAction func btnActionAlreadyAccountClicked(_ sender: UIButton) {
        self .dismiss(animated: true, completion: nil)
    }
    @IBAction func btnActionSignUpClicked(_ sender: UIButton) {
        
//        {
//          "email": "test@gmailcom",
//          "password": "12345",
//          "userID": 0,
//          "name": "test",
//          "surname": "test",
//          "username": "user",
//          "facebookID": "string"
//        }
        
        if text_email.text?.isValidEmail() == false {
            self.showAlertMessage(message:"Enter valid email")
        }
        else if (self.text_name.text?.trim().count)! < 1 {
            self.showAlertMessage(message:"Enter name")
        }
        else if (self.text_surname.text?.trim().count)! < 1 {
            self.showAlertMessage(message:"Enter surname")
        }
        else if (self.text_password.text?.trim().count)! < 1 {
            self.showAlertMessage(message:"Enter password")
        }
        else if self.btn_acceptTerms.isSelected == false {
            self.showAlertMessage(message:"Please accept our privacy and terms before registering")
        }
            else {
                let parameters = [
                    "email": text_email.text!,
                    "name": text_name.text!,
                    "surname": text_surname.text!,
                    "password": text_password.text!] as [String : AnyObject]
                WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.kregsiter, parameters:parameters, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
                    if isSuccess == true {
                        if let dict = dictData .object(forKey: "data") as? [String:AnyObject]{
          
                            if let data = dict["user"] as? [String:AnyObject] {
                                
                               UserDefaults .standard .set(HelperClass .getValidationstring(dict: data, key: "id"), forKey: kUserid)
                                
                                UserDefaults .standard .set(true, forKey: isLogged)
                                UserDefaults.standard.synchronize()
                                let objtabbar = self.tabbarStoryBoard().instantiateViewController(withIdentifier:"tabbar")
                                objAppdelegate.window?.rootViewController  = objtabbar
                            }
                            
                        }
                    }else{
                        self.showAlertMessage(message:dictData["data"] as? String ?? "")
                    }
                }
                
                
                
            }
            
        
    }
    @IBAction func btnActionContactsClicked(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC")as! WebviewVC
        obj.strUrl = ksupport
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    @IBAction func btnActionWebsiteClicked(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC")as! WebviewVC
        obj.strUrl = kwebsite
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnActionTermsClicked(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC")as! WebviewVC
        obj.strUrl = kterms
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnActionTermsAceptedClicked(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
    }

}
