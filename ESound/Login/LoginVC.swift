//
//  LoginVC.swift
//  ESound
//
//  Created by kirti on 13/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit
//import FontAwesome_swift

class LoginVC: UIViewController {
    
    @IBOutlet weak var btn_login: UIButton!
    @IBOutlet weak var text_password: UITextField!
    @IBOutlet weak var text_email: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Sign In"
    
        self.text_password .addButtonWithCornerBorder()
        self.text_email .addButtonWithCornerBorder()
        
        self.text_email.LeftviewImage(name: "email")
        self.text_password.LeftviewImage(name: "key")
        self.btn_login.setButtonWithGreenColor()
        self.btn_login .setCornerRound()
        
        let barButton = UIBarButtonItem(image: UIImage(named: "down"), style: .plain, target: self, action: #selector(btnAtionBackClicked))
        self.navigationItem.leftBarButtonItem  = barButton
       
//        self.text_email.text = "kirti301290@gmail.com"
//        self.text_password.text = "1234"
        
        self.text_email.text = "test1@gmail.com"
        self.text_password.text = "test1"
        
//        self.text_email.text = "a@a.com"
//        self.text_password.text = "a"
   
    }
   @objc func btnAtionBackClicked() {
        self .dismiss(animated: true, completion: nil)
    }
    @IBAction func btnActionForgotClicked(_ sender: UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
        obj.strUrl = kforgotPassword
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    @IBAction func btnActionLoginClicked(_ sender: UIButton) {
        
        self.view.endEditing(true)
        if text_email.text?.isValidEmail() == false {
            self .showAlertMessage(message:"Enter valid email")
        }else if (self.text_password.text?.trim().count)! < 1 {
            HelperClass .showAlertMessage(message:"Enter password")
              self.showAlertMessage(message:"Enter password")
        }
        else {
            let parameters = [
                "email": text_email.text!,
                "password": text_password.text!] as [String : AnyObject]
            WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.klogin, parameters:parameters, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
                if isSuccess == true {
                    if let dict = dictData .object(forKey: "data") as? [String:AnyObject]{
                        if let data = dict["user"] as? [String:AnyObject] {
                            UserDefaults .standard .set(HelperClass .getValidationstring(dict: data, key: "id"), forKey: kUserid)
                            UserDefaults .standard .set(true, forKey: isLogged)
                            UserDefaults.standard.synchronize()
                            let objtabbar = self.tabbarStoryBoard().instantiateViewController(withIdentifier:"tabbar")
                            objAppdelegate.window?.rootViewController  = objtabbar
                        }
                    }
                }else{
                    if (dictData["data"] as? String ?? "") == "wrongCredentials" {
                        self .showAlertWithContact(message: "Your credentials are't correct")
                    }else {
                        HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
                    }
                    
                }
            }
        }
  
    }
    @IBAction func btnActionContactsClicked(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
        obj.strUrl = ksupport
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnActionTermsClicked(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
        obj.strUrl = kterms
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnActionPrivacyClicked(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
        obj.strUrl = kprivacy
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnActionWebsiteClicked(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
        obj.strUrl = kwebsite
        self.navigationController?.pushViewController(obj, animated: true)
        
    }

}
