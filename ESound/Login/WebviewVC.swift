//
//  WebviewVC.swift
//  ESound
//
//  Created by kirti on 13/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class WebviewVC: UIViewController {

    @IBOutlet weak var webview: UIWebView!
    var strUrl = String()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.webview .loadRequest(URLRequest(url: URL(string: strUrl)!))
        self.addBackbutton()
        
    }
   

}
