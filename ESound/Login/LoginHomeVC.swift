//
//  ViewController.swift
//  ESound
//
//  Created by kirti on 13/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit
import AVFoundation
import FacebookLogin
import FBSDKLoginKit


class LoginHomeVC: UIViewController {

   
    var objplayer =  AVPlayer()
    @IBOutlet weak var btn_register: UIButton!
    @IBOutlet weak var btn_facebook: UIButton!
    @IBOutlet weak var btn_login: UIButton!
   
    @IBOutlet weak var view_bg: UIView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
           let path   = Bundle.main.path(forResource: "authBg", ofType: "mp4")
            objplayer = AVPlayer(url: URL(fileURLWithPath: path!))
        
            let layer = AVPlayerLayer(player: objplayer)
            layer.frame = self.view.bounds
            layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            view_bg.layer.addSublayer(layer)
            objplayer.play()
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime,
                                               object: nil,
                                               queue: nil) { [weak self] note in
                                                self?.objplayer.seek(to: CMTime.zero)
                                                self?.objplayer.play()
        }
        
          self.btn_login .setCornerRound()
          self.btn_facebook .setCornerRound()
          self.btn_register .setCornerRound()
        
//        UserDefaults .standard .set(false, forKey: isLogged)
//        UserDefaults.standard.synchronize()
            
      
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if UserDefaults.standard.bool(forKey: isLogged) {
            let objtabbar = self.tabbarStoryBoard().instantiateViewController(withIdentifier:"tabbar")
            objAppdelegate.window?.rootViewController  = objtabbar
            HelperClass .getUserPlayList { (success) in
            }
             HelperClass .getYoutubeApiKey(isFirstTime: true)
        }
        
    }
    @IBAction func btnActionLoginClicked(_ sender: UIButton) {
        let objregister = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.present(objregister.setNavigationBarController(), animated: true, completion: nil)
    }
    @IBAction func btnActionFacebookClicked(_ sender: UIButton) {
        
        let loginManager: LoginManager = LoginManager()
        loginManager.logOut()
        
        let fbLoginManager = LoginManager()
        let fbPermissions : [String] = ["public_profile", "email", "user_hometown", "user_location"]
        fbLoginManager.logIn(permissions: fbPermissions , from: self) { (result, error) in
            if (result?.isCancelled)! {
                
            }
            else if ((error) != nil) {
                // AppSingletonObj.alertShow(title: kst_TitleFill, message: error?.localizedDescription, From: self, btnTitle: kst_btn_OK)
            }
            else {
                self.get_FBUserData()
            }
        }
    }
    @IBAction func btnActionRegisterClicked(_ sender: UIButton) {
        
        let objregister = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        objregister.modalPresentationStyle = .fullScreen
        self.present(objregister.setNavigationBarController(), animated: true, completion: nil)
       
    }
    func get_FBUserData() {
        
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, hometown, location, email, gender, picture.type(large)"])
          graphRequest.start { (connection, result, error) in
              
              if ((error) != nil) {
                 // AppSingletonObj.alertShow(title: kst_TitleFill, message: error?.localizedDescription, From: self, btnTitle: kst_btn_OK)
              }
              else {
                  
                  let dict_FBData = result as! NSDictionary
                  let dict_AllKey = NSArray(array: dict_FBData.allKeys)
                
                  let str_FBID : String = dict_FBData.value(forKey: "id") as! String
                  var str_Fname : String = ""
                  var str_Lname : String = ""
                  var str_Email : String = ""
                 
                  if (dict_AllKey.contains("first_name")) {
                      str_Fname = dict_FBData.value(forKey: "first_name") as! String
                  }
                  if (dict_AllKey.contains("last_name")) {
                      str_Lname = dict_FBData.value(forKey: "last_name") as! String
                  }
                  if (dict_AllKey.contains("email")) {
                      str_Email = dict_FBData.value(forKey: "email") as! String
                  }
                 let parameters = [
                                "email": str_Email,
                                "name": str_Fname,
                                "surname": str_Lname,
                                "facebookID": str_FBID] as [String : AnyObject]
                
                            WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.kregsiter, parameters:parameters, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
                                if isSuccess == true {
                                    if let dict = dictData .object(forKey: "data") as? [String:AnyObject] {
                                        if let data = dict["user"] as? [String:AnyObject] {
                                            UserDefaults .standard .set(HelperClass .getValidationstring(dict: data, key: "id"), forKey: kUserid)
                                            UserDefaults .standard .set(true, forKey: isLogged)
                                            UserDefaults.standard.synchronize()
                                            let objtabbar = self.tabbarStoryBoard().instantiateViewController(withIdentifier:"tabbar")
                                            objAppdelegate.window?.rootViewController  = objtabbar
                                        }
                                        
                                    }
                                }else{
                                    HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
                                }
                            }
               
              }
          }
      }


}

