//
//  SortSongsVC.swift
//  ESound
//
//  Created by kirti on 15.12.19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class SortSongsVC: UIViewController {

    @IBOutlet weak var btn_save: UIButton!
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var tbl_sortSongs: UITableView!
    var arr_sortsongsList = [Music]()
    var objPlaylist:PlayListModel! = nil
    var strSortType = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        let cell = UINib(nibName: "cell_songslist", bundle: nil)
        self.tbl_sortSongs.register(cell, forCellReuseIdentifier: "cell_songslist")
        
        let menuButton = UIBarButtonItem(image: UIImage(named: "filter"), style: .plain, target: self, action: #selector(btnActionSortClicked(_:)))
        self.navigationItem.rightBarButtonItem  = menuButton
        
        self.tbl_sortSongs.estimatedRowHeight = 60
        self.tbl_sortSongs.rowHeight = 60
        self.tbl_sortSongs.isEditing = true
        
        self.navigationItem.title = "Sort playlist"
        
        self.btn_Cancel .setButtonWithBorderAndCorner()
        self.btn_save .setButtonWithGreenColor()

        // Do any additional setup after loading the view.
    }
    func getSortType(sortingType:String)->AnyObject? {
//        switch (sortingType) {
//        case "addingDate":
//            return 0 as AnyObject;
//        case "name":
//            return 1 as AnyObject;
//        case "tracksCount":
//            return 2 as AnyObject;
//        default:
//            return nil;
//        }
        
        switch (sortingType) {
        case kFromnewest:
            return 0 as AnyObject;
        case kAlphabetically:
            return 1 as AnyObject;
        case kByTracksCount:
            return 2 as AnyObject;
        default:
            return nil;
        }
    }
    @IBAction func btnActionCancelClicked(_ sender: Any) {
        self .dismiss(animated: true, completion: nil)
    }
    @IBAction func btnActionSaveClicked(_ sender: UIButton) {
        
        
        if Connectivity.connetivityAvailable() == false {
            HelperClass .showAlertMessage(title: errOfflinetitle, message: errChangedLibraryOffline)
            return
        }
        
        var array = [[String:AnyObject]]()
        for i in 0 ..< self.arr_sortsongsList.count {
            let obj = self.arr_sortsongsList[i]
            array .append(["trackID":obj.trackUserID,"pos":i+1] as [String:AnyObject])
        }
        
        let parameters = [
            "playlistID": objPlaylist!.id,
            "TrackPos":array,
            "SortType":self.getSortType(sortingType: strSortType)] as [String : AnyObject]
        WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.kChangeTrackPosition, parameters:parameters, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
            if isSuccess == true {
                if let status = dictData .object(forKey: "status") as? Bool,status == true {
                    self.dismiss(animated: true, completion: nil)
                
                }
            }else{
                HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
            }
        }
    }
    @objc func btnActionSortClicked(_ sender: UIBarButtonItem) {
        let actionsheet = UIAlertController(title: objPlaylist?.title, message: nil, preferredStyle:.actionSheet)
        actionsheet.addAction(UIAlertAction(title: kAlphabetically, style: .default, handler: { (action) -> Void in
            self.strSortType = kAlphabetically
            self.arr_sortsongsList.sort { (obj1, obj2) -> Bool in
                return (obj1.trackTitle.localizedCaseInsensitiveCompare(obj2.trackTitle) == .orderedAscending)
            }
            self.tbl_sortSongs .reloadData()
        }))
        actionsheet.addAction(UIAlertAction(title: kFromnewest, style: .default, handler: { (action) -> Void in
            self.strSortType = kFromnewest
            
           // yyyy-MM-dd'T'HH:mm:ss
            var dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            
            self.arr_sortsongsList.sort { (obj1, obj2) -> Bool in
                return (dateFormatter.date(from:obj1.addedDate)!.compare(dateFormatter.date(from:obj2.addedDate)!) == .orderedDescending)
            }
            self.tbl_sortSongs .reloadData()
        }))
        actionsheet.addAction(UIAlertAction(title: kDefault, style: .default, handler: { (action) -> Void in
            self.strSortType = kDefault
            self.strSortType = kFromnewest
             let dateFormatter = DateFormatter()
             dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
             self.arr_sortsongsList.sort { (obj1, obj2) -> Bool in
            return (dateFormatter.date(from:obj1.addedDate)!.compare(dateFormatter.date(from:obj2.addedDate)!) == .orderedAscending)
            }
            self.tbl_sortSongs .reloadData()
        }))
        actionsheet.addAction(UIAlertAction(title:kCancel, style: .cancel, handler: { (action) -> Void in
            
        }))
        self.present(actionsheet, animated: true, completion: nil)
    }

      

   

}
extension SortSongsVC:UITableViewDelegate,UITableViewDataSource
{
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_sortsongsList.count
    }
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_songslist", for: indexPath) as! cell_songslist
        let objData = self.arr_sortsongsList[indexPath.row]
        cell.lbl_title.text =  objData.trackTitle
        cell.lbl_subtitle.text = objData.authorName
        cell.btn_add .isHidden = true
       
        if let strUrl = objData.trackImageUrl {
            cell.imgvw_photo .sd_setImage(with:URL(string:strUrl)) { (image, error, cacheType, url) in
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let itemToMove = self.arr_sortsongsList[sourceIndexPath.row]
        self.arr_sortsongsList .remove(at:sourceIndexPath.row)
        self.arr_sortsongsList .insert(itemToMove, at: destinationIndexPath.row)
    }
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
   

}
