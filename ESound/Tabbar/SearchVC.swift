//
//  SearchVC.swift
//  ESound
//
//  Created by kirti on 14/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit
import Alamofire
class SearchVC: BaseClassVC {

    @IBOutlet weak var view_footer: UIView!
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var btn_bottomSearch: UIButton!
    @IBOutlet weak var tbl_search: UITableView!
    @IBOutlet weak var segmentview: UISegmentedControl!
    
     let tagEverythinsg = 0
     let tagLibrary = 1
    var arr_bestResult = [Music]()
    var arr_songList = [Music]()
    var arr_artistList = [PlayListModel]()
    var arr_playtList = [PlayListModel]()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addProfilebutton()
       
        let cell = UINib(nibName: "cell_songslist", bundle: nil)
        self.tbl_search.register(cell, forCellReuseIdentifier: "cell_songslist")
        
        let cell_header = UINib(nibName: "Cell_tblheader", bundle: nil)
        self.tbl_search.register(cell_header, forCellReuseIdentifier: "Cell_tblheader")
        
        if UIDevice.isPad {
            view_footer =  UINib(nibName: "SearchHeaderIpad", bundle: nil).instantiate(withOwner: self, options: nil)[0] as? UIView
        }else{
            view_footer =  UINib(nibName: "SearchHeaderIphone", bundle: nil).instantiate(withOwner: self, options: nil)[0] as? UIView
        }
        
        self.tbl_search.tableHeaderView = view_footer
        btn_bottomSearch.titleLabel?.font = UIFont(name: RobotoMedium, size: FontSizeNormal)
        btn_bottomSearch.backgroundColor = UIColor.lightGray.withAlphaComponent(0.4)
        btn_bottomSearch.layer.cornerRadius = 10
        btn_bottomSearch.clipsToBounds = true
        
        self.tbl_search.estimatedRowHeight = 60
        self.tbl_search.rowHeight = 60
        searchbar .setTextField(color: UIColor.clear)
        
        searchbar.backgroundColor = UIColor.lightGray.withAlphaComponent(0.4)
        searchbar.layer.cornerRadius = 15
        searchbar.layer.masksToBounds = true
        self.searchDeezerData(search:"")
        
        NotificationCenter.default.addObserver(forName:ChangeSonagNotification, object: nil, queue: nil) { (objNotification) in
            let obj = objNotification.userInfo
            objAppdelegate.objActiveMusic = obj![kactiveMusic] as? Music
            self.tbl_search .reloadData()
        }
        
      
       
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Search"
        
       
    }
   
    //MARK:Button Action
    @objc func btnActionHeaderClicked(sender:UIButton) {
        
        // self.navigationItem.title = ""
        self.view .endEditing(true)
        if sender.tag == 1 {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
            obj.objPlayList = PlayListModel(title: "All Tracks", subtitle: "", url: "", id: "-1")
            obj.arr_songsList = self.arr_bestResult + self.arr_songList
            obj.isFromTracks = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if sender.tag == 2 {
            let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "TracksListVC") as! TracksListVC
            if segmentview.selectedSegmentIndex == tagEverythinsg {
                obj.tagIdentify = tag_albums
                obj.arr_albumsList = arr_playtList
            }else {
                obj.tagIdentify = tag_Playlits
                obj.arr_playList = arr_playtList
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if sender.tag == 3 {
            let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "TracksListVC") as! TracksListVC
            obj.arr_artistList = arr_artistList
            obj.isFromDeezerArtist = true
            obj.tagIdentify = tag_Artists
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
        
    }
    @IBAction func ActionSwitchChanged(_ sender: UISegmentedControl) {
        searchbar.text = ""
        searchbar.resignFirstResponder()
        self.searchDeezerData(search: "")
    }
    @IBAction func btnActionBottomSearchClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    //MARK:custom Function
    func searchDeezerData(search:String) {
           
        let headerview = tbl_search.tableFooterView
        headerview?.isHidden = true
           self.view.showBlurLoader()
           self.arr_songList .removeAll()
           self.arr_playtList .removeAll()
           self.arr_artistList .removeAll()
           self.arr_bestResult .removeAll()
           self.tbl_search .reloadData()
           if search.count > 0 {
               if segmentview.selectedSegmentIndex == tagEverythinsg {
                   if btn_bottomSearch .isSelected {
                       self.youtubeSearch(search:search)
                   }else {
                       self.getDeezerSearchSongs(search:search)
                   }
               headerview?.isHidden = false
               }else {
                   self.getlibraryData(search:search)
               }
           }else {
               if segmentview.selectedSegmentIndex == tagEverythinsg {
                   self.getDeezerDefaultList()
                 headerview?.isHidden = false
               }else {
                self.getlibraryData(search: "")
               }
               
           }
           
           
       }
    func getDeezerDefaultList() {
        WebserviceHelper.WebapiGetMethod(strurl:"https://api.deezer.com/chart/0?limit=30", ShowProgress: false) { (isSuccess, dictData) in
            self.view .removeBluerLoader()
            if isSuccess == true {
                
                if let dictDataall = dictData["tracks"] as? [String:AnyObject] {
                    if let arrdata = dictDataall["data"] as? [[String:AnyObject]]  {
                        for i in 0 ..< arrdata.count {
                            let dict = arrdata[i]
                            let obj = Music()
                            obj.trackTitle = dict["title"] as? String ?? ""
                            obj.trackImageUrl = dict["album"]?["cover_medium"] as? String ?? ""
                            obj.musictype = .Deezer
                            obj.musicNum = i
                            obj.deezerTrackID = HelperClass .getValidationstring(dict: dict, key: "id")
                            obj.authorName = dict["artist"]?["name"] as? String ?? ""
                            obj.authorImageUrl = dict["artist"]?["picture"] as? String ?? ""
                            obj.authorID = HelperClass .getValidationstring(dict: dict["artist"] as! [String:Any], key: "id")
                            if i<3 {
                                self.arr_bestResult.append(obj)
                            }else {
                                self.arr_songList.append(obj)
                            }
                        }
                    }
                }
              
                if let arrdata = dictData .value(forKeyPath:"artists.data") as? [[String:AnyObject]] {
                        for i in 0 ..< arrdata.count {
                            let dict = arrdata[i]
                            let intid = dict["id"] as? Int64
                            let url  = dict["picture_medium"] as? String ?? ""
                            let objPlayList = PlayListModel(title: dict["name"] as? String, subtitle:dict["artist"]?["name"] as? String ?? "", url: url, id: String(describing: intid!))
                            self.arr_artistList.append(objPlayList)
                        }
                }
               
                if let arrdata =  dictData .value(forKeyPath: "albums.data") as? [[String:AnyObject]] {
                        for i in 0 ..< arrdata.count {
                            let dict = arrdata[i]
                            let intid = dict["id"] as? Int64
                            let url  = dict["cover_medium"] as? String ?? ""
                            let objPlayList = PlayListModel(title: dict["title"] as? String, subtitle:dict["artist"]?["name"] as? String ?? "", url: url, id: String(describing: intid!))
                            self.arr_playtList.append(objPlayList)
                        }
                    
                }
                self.tbl_search.reloadData()
                
            }else{
                HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
            }
        }
    }
    func getDeezerSearchSongs(search:String) {
        let encode = search
       
       var str = "https://api.deezer.com/search/?q="+"\(encode)"+"&index=0&limit=30&output=json"
           WebserviceHelper.WebapiGetMethod(strurl:str, ShowProgress: false) { (isSuccess, dictData) in
               self.view .removeBluerLoader()
               if isSuccess == true {
                   if let arrdata =  dictData .value(forKeyPath:"tracks.data") as? [[String:AnyObject]] {
                           for i in 0 ..< arrdata.count {
                             let dict = arrdata[i]
                            let obj = Music()
                            obj.trackTitle = dict["title"] as? String ?? ""
                            obj.trackImageUrl = dict["album"]?["cover_medium"] as? String ?? ""
                            obj.musictype = .Deezer
                            obj.musicNum = i
                            obj.deezerTrackID = HelperClass .getValidationstring(dict: dict, key: "id")
                            obj.authorName = dict["artist"]?["name"] as? String ?? ""
                            obj.authorImageUrl = dict["artist"]?["picture"] as? String ?? ""
                            obj.authorID = HelperClass .getValidationstring(dict: dict["artist"] as! [String:Any], key: "id")
                            if self.arr_bestResult.count < 4 {
                                self.arr_bestResult.append(obj)
                            }else {
                                self.arr_songList.append(obj)
                            }
                           }
                   }
               
                   if let arrdata =  dictData .value(forKeyPath:"artists.data") as? [[String:AnyObject]] {
                           for i in 0 ..< arrdata.count {
                               let dict = arrdata[i]
                               let intid = dict["id"] as? Int64
                               let url  = dict["picture_medium"] as? String ?? ""
                               let objPlayList = PlayListModel(title: dict["name"] as? String, subtitle:"", url: url, id: String(describing: intid!))
                               self.arr_artistList.append(objPlayList)
                           }
                      
                   }
                   self.tbl_search.reloadData()
                   
               }
           }
        str = "https://api.deezer.com/search/album?q="+"\(encode)"+"&index=0&limit=30&output=json"
        WebserviceHelper.WebapiGetMethod(strurl:str, ShowProgress: false) { (isSuccess, dictData) in
            if isSuccess == true {
                if let arrdata = dictData["data"] as? [[String:AnyObject]] {
                    for i in 0 ..< arrdata.count {
                        let dict = arrdata[i]
                        let intid = dict["id"] as? Int64
                        let url  = dict["cover_medium"] as? String ?? ""
                        let objPlayList = PlayListModel(title: dict["title"] as? String, subtitle:dict["artist"]?["name"] as? String ?? "", url: url, id: String(describing: intid!))
                        self.arr_playtList.append(objPlayList)
                    }
                }
                self.tbl_search.reloadData()
                
            }
        }
        str = "https://api.deezer.com/search/artist?q="+"\(encode)"+"&index=0&limit=30&output=json"
        WebserviceHelper.WebapiGetMethod(strurl:str, ShowProgress: false) { (isSuccess, dictData) in
            if isSuccess == true {
                if let arrdata = dictData["data"] as? [[String:AnyObject]]  {
                    for i in 0 ..< arrdata.count {
                        let dict = arrdata[i]
                        let intid = dict["id"] as? Int64
                        let url  = dict["picture_medium"] as? String ?? ""
                        let objPlayList = PlayListModel(title: dict["name"] as? String, subtitle:"", url: url, id: String(describing: intid!))
                        self.arr_artistList.append(objPlayList)
                    }
                }
                
                self.tbl_search.reloadData()
                
            }
        }
    }
    
    func getlibraryData(search:String){
        
        if search.count > 0 {
            
        }else {
            let str_QuerySongs = String(format: "Select s.* from tbl_playlist_songs as pl left join tbl_songs as s on pl.trackReference = s.trackReference  where pl.p_id = '0'")
            let arrdata = DataManager.initDB().RETRIEVE_Songs(query: str_QuerySongs)
            
            for i in 0 ..< arrdata.count {
                let obj = arrdata[i]
                if i<3 {
                    self.arr_bestResult.append(obj)
                }else {
                    self.arr_songList.append(obj)
                }
            }
            let str_PlayLists = String(format: "Select * from tbl_playlist where id != '0' order by  cast (id as int)")
            self.arr_playtList = DataManager.initDB().RETRIEVE_PlayList(query: str_PlayLists)
            for infoPl in self.arr_playtList {
                let str_QuerySongs = String(format: "Select s.* from tbl_playlist_songs as pl left join tbl_songs as s on pl.trackReference = s.trackReference  where pl.p_id = '%@'", infoPl.id)
                infoPl.arr_songs = DataManager.initDB().RETRIEVE_Songs(query: str_QuerySongs)
            }
            
            let str_QueryAuther = String(format:"SELECT authorID , COUNT(*) ,authorName,authorImageUrl FROM tbl_songs GROUP BY authorName")
            let array = DataManager.initDB().RETRIEVE_Artist(query: str_QueryAuther)
            self.arr_artistList = array
        }
        self.view .removeBluerLoader()
        self.tbl_search .reloadData()
        
    }
    func youtubeSearch(search:String){
//        xios.get('https://www.googleapis.com/youtube/v3/search', {
//          params: {
//            part: 'snippet',
//            type: 'video',
//            maxResults: 30,
//            order: 'viewCount',
//            videoCategoryId: 10,
//            key: store.getState().apiKeys.ytApiKey,
//            safeSearch: store.getState().user.explicitOff ? 'strict' : 'none',
//            q: val.toLowerCase()
//          }, crossdomain: true
//        }).then((res) => {
        if UserDefaults.standard.bool(forKey: kYoutubeKeyLoaded) == false {
            return
        }
        
        let parameter = ["part":"snippet",
                         "type":"video",
                         "maxResults":"30",
                         "order":"viewCount",
                         "videoCategoryId":"10",
                         "safeSearch":"none",
                         "key":UserDefaults.standard.string(forKey: kYoutubeKey),
                         "q":search] as! [String:String]
        
        Alamofire.request(URL(string:"https://www.googleapis.com/youtube/v3/search")!, method: .get, parameters: parameter, encoding:URLEncoding.default, headers: nil).responseJSON { (response) in
            if response.result.isSuccess {
                print(response.result.value)
                 self.view .removeBluerLoader()
                if let dictBig = response.result.value as? [String:AnyObject] {
                    if let arrdata = dictBig["items"] as? NSArray {
                        for i in 0 ..< arrdata.count {
                            let dict = arrdata[i] as! NSDictionary
                          //  let intid = dict .value(forKeyPath: "id.videoId") as? String
                            let url  = dict .value(forKeyPath: "snippet.thumbnails.default.url") as? String
                          //  let objPlayList = PlayListModel(title:dict .value(forKeyPath: "snippet.title") as? String, subtitle:dict .value(forKeyPath: "snippet.channelTitle") as? String, url: url, id: String(describing: intid!))
                            
                            let obj = Music()
                            obj.trackTitle = dict .value(forKeyPath: "snippet.title") as? String ?? ""
                            obj.trackImageUrl = url
                            obj.musictype = .Youtube
                            obj.musicNum = i
                            obj.deezerTrackID = dict .value(forKeyPath: "id.videoId") as? String
                            obj.trackID = (dict .value(forKeyPath: "id.videoId") as? String)!
                            
                            obj.authorName = dict .value(forKeyPath: "snippet.channelTitle") as? String ?? ""
                           // obj.authorImageUrl = dict["artist"]?["picture"] as? String ?? ""
                           // obj.authorID = HelperClass .getValidationstring(dict: dict["artist"] as! [String:Any], key: "id")
                            if i<3 {
                                self.arr_bestResult.append(obj)
                            }else {
                                self.arr_songList.append(obj)
                            }
                            
                            
                            
                        }
                    }
                    else if dictBig["error"] != nil {
                       HelperClass .getYoutubeApiKey(isFirstTime: false)
                    }
                    self.tbl_search .reloadData()
                }
            }
        }
        
    }
    

    

}
extension SearchVC:UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.arr_bestResult.count
        }
        else if section == 1 {
            return self.arr_songList.count > 5 ? 5 :self.self.arr_songList.count
        }
        else if section == 2 {
            return self.arr_playtList.count > 3 ? 3 :self.self.arr_playtList.count
        }
        else if section == 3 {
            return self.arr_artistList.count > 5 ? 5 :self.self.arr_artistList.count
        }
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellsong = tableView.dequeueReusableCell(withIdentifier: "cell_songslist", for: indexPath) as! cell_songslist
        cellsong.imgvw_photo .setImageCornerRound()
        cellsong.lbl_title.font = UIFont.init(name: RobotoMedium, size: FontSizeSmall)
        cellsong.lbl_subtitle.font = UIFont.init(name: RobotoRegular, size: FontSizeSmall)
       
        if indexPath.section == 0 {
            let objData = self.arr_bestResult[indexPath.row]
            cellsong.lbl_title.text = objData.trackTitle
            cellsong.lbl_subtitle.text = objData.authorName
            if let strUrl = objData.trackImageUrl {
                cellsong.imgvw_photo .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
                }
            }
        
            cellsong.btn_add.addTarget(self, action: #selector(btnActionPlusClick(sender:)), for: .touchUpInside)
            self.setTextColor(objModel: objData, label:cellsong.lbl_title)
            self.arr_bestResult[indexPath.row] = self.setButtomImage(objModel: objData, btn: cellsong.imgvw_status)
        }
        else if indexPath.section == 1 {
            let  objData = self.arr_songList[indexPath.row]
            cellsong.lbl_title.text = objData.trackTitle
            cellsong.lbl_subtitle.text = objData.authorName
            if let strUrl = objData.trackImageUrl {
                cellsong.imgvw_photo .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
                }
            }
            self.setTextColor(objModel: objData, label:cellsong.lbl_title)
            cellsong.btn_add.addTarget(self, action: #selector(btnActionPlusClick(sender:)), for: .touchUpInside)
            self.arr_songList[indexPath.row] = self.setButtomImage(objModel: objData, btn:cellsong.imgvw_status)
        }
        else  if indexPath.section == 2 {
            var objData: PlayListModel?
            objData = self.arr_playtList[indexPath.row]
           // cellsong.btn_add.setImage(UIImage(named: "right-arrow"), for: .normal)
            cellsong.imgvw_status.image = UIImage(named: "right-arrow")
            cellsong.lbl_title.text = objData?.title
            cellsong.lbl_subtitle.text = objData?.subtitle
            if let strUrl = objData?.url {
                cellsong.imgvw_photo .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
                }
            }
        }
        else  if indexPath.section == 3 {
            var objData: PlayListModel?
            objData = self.arr_artistList[indexPath.row]
          //  cellsong.btn_add.setImage(UIImage(named: "right-arrow"), for: .normal)
            cellsong.imgvw_status.image = UIImage(named: "right-arrow")
            cellsong.lbl_title.text = objData?.title
            cellsong.lbl_subtitle.text = objData?.subtitle
            if let strUrl = objData?.url {
                cellsong.imgvw_photo .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
                }
            }
            if segmentview.selectedSegmentIndex == tagLibrary {
                cellsong.lbl_subtitle.text = (objData?.subtitle ?? "") + " tracks"
            }
        }
    
        return cellsong
    }
    
    func  tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cellheader = tableView.dequeueReusableCell(withIdentifier: "Cell_tblheader") as! Cell_tblheader
        cellheader.btn_add.tag = section
        cellheader.btn_add .addTarget(self, action: #selector(btnActionHeaderClicked(sender:)), for: .touchUpInside)
        if section == 0 {
            cellheader.lbl_header.text = "Best results"
            cellheader.btn_add.isHidden = true
            
            if arr_bestResult.count > 0 {
                cellheader.lbl_header.isHidden = false
            }
        }
        else if section == 1 {
            cellheader.lbl_header.text = "Songs"
             cellheader.btn_add.isHidden = true
             cellheader.lbl_header.isHidden = true
            if arr_songList.count > 0 {
                cellheader.lbl_header.isHidden = false
                cellheader.btn_add.isHidden = false
            }
        }
        else if section == 2 {
         
            if segmentview.selectedSegmentIndex == tagEverythinsg {
                cellheader.lbl_header.text = "Albums"
            }else {
                cellheader.lbl_header.text = "Playlist"
            }
            cellheader.btn_add.isHidden = false
            
            cellheader.btn_add.isHidden = true
             cellheader.lbl_header.isHidden = true
            if arr_playtList.count > 0 {
                cellheader.lbl_header.isHidden = false
                cellheader.btn_add.isHidden = false
            }
        }
        else if section == 3 {
            cellheader.lbl_header.text = "Artists"
            cellheader.btn_add.isHidden = false
            
            cellheader.btn_add.isHidden = true
            cellheader.lbl_header.isHidden = true
            if arr_artistList.count > 0 {
                cellheader.lbl_header.isHidden = false
                cellheader.btn_add.isHidden = false
            }
        }
        
        if segmentview.selectedSegmentIndex == 1 {
            cellheader.btn_add.isHidden = true
        }
        

       
        return cellheader;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            return arr_bestResult.count > 0 ? 45 :0
        }
        else if section == 1 {
            return  arr_songList.count > 0 ? 45 :0
        }
        if section == 2 {
            return  arr_playtList.count > 0 ? 45 :-2
        }
        if section == 3 {
            return  arr_artistList.count > 0 ? 45 :0
        }
      
        
        return 45
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //self.navigationItem.title = ""
        self.view .endEditing(true)
        if indexPath.section == 0 {
            let array = self.arr_bestResult + self.arr_songList
             gotoOpenPlayer(index:indexPath.row, array: array)
        }
        else if indexPath.section == 1 {
                let array = self.arr_bestResult + self.arr_songList
            gotoOpenPlayer(index: (self.arr_bestResult.count + indexPath.row), array: array)
        }
        else if indexPath.section == 3 {
            let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "ArtistDetailVC") as! ArtistDetailVC
            obj.objPlayList = arr_artistList[indexPath.row]
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if indexPath.section == 2 {
            if segmentview.selectedSegmentIndex == tagEverythinsg{
                let objModel = self.arr_playtList [indexPath.row]
                let obj = self.tabbarStoryBoard().instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
                obj.objPlayList = objModel
                obj.isFromAlbum = true
                self.navigationController?.pushViewController(obj, animated: true)
            }else {
               let objModel = self.arr_playtList[indexPath.row]
               let obj = self.tabbarStoryBoard().instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
               obj.objPlayList = objModel
               obj.isFromLibrarySongs = true
               self.navigationController?.pushViewController(obj, animated: true)
            }
          
        }
    }
    func gotoOpenPlayer(index:Int,array:[Music]) {
        objAppdelegate.objActiveMusic = array[index]
        let object = ["index":index,"object":array] as [String : Any]
        NotificationCenter.default.post(name:OpenPlayerNotification, object: nil, userInfo: object)
        self.tbl_search .reloadData()
       
    }
    @objc func btnActionPlusClick(sender:UIButton) {
        
        let point = sender.convert(CGPoint.zero, to: self.tbl_search)
        let indexPath = self.tbl_search?.indexPathForRow(at: point)
        if indexPath?.section == 0 || indexPath?.section == 1 {
             var objMusic = Music()
            if indexPath?.section == 0 {
                objMusic = self.arr_bestResult[indexPath!.row]
            }else {
                objMusic = self.arr_songList[indexPath!.row]
            }
            
            
            if objMusic.isOffline == "0" , (objMusic.download_status == "" || objMusic.download_status == "0") {
                if Connectivity.connetivityAvailable() == false {
                    HelperClass .showAlertMessage(title: errOfflinetitle, message: errOfflineDownload)
                    return
                }
                self.saveTrackOffline(objMusic: objMusic)
                objMusic.isOffline = "1"
                objMusic.download_status = "0"
                HelperClass.shared().addObjectinGlobal(objMusic: objMusic) { (sucess) in
                    self.tbl_search.reloadData()
                }
                DownloadManager.shared().downloadSong(track: objMusic) { (sucess, objmusic, error) in
                    objMusic.download_status = "1"
                    HelperClass.shared().addObjectinGlobal(objMusic: objMusic) { (sucess) in
                        self.tbl_search .reloadData()
                    }
                }
            }
            else if objMusic.isOffline == "1" ,objMusic.download_status == "0" {
                // waiting to download
                
            }
            else if objMusic.isOffline.count < 1  {
                HelperClass .SaveTrackData(objMusic: objMusic, showProgress: true) { (sucess, objmusic) in
                    self.tbl_search .reloadData()
                }
            }
        }
        
    
    }
}
extension SearchVC:UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let text = searchText.trimmingCharacters(in: .whitespacesAndNewlines)
        if text.count > 0 {
            self.searchDeezerData(search:searchText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        }
    }
  
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
       
    }
    
}
