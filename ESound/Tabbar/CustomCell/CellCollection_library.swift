//
//  CellCollection_library.swift
//  TestApp
//
//  Created by kirti on 14/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class CellCollection_library: UICollectionViewCell {

    @IBOutlet weak var lbl_subtitle: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var imgvw_photo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.lbl_title.font = UIFont.init(name: RobotoRegular, size: FontSizeNormal)
        self.lbl_subtitle.font = UIFont.init(name: RobotoRegular, size: FontSizeSmall)
        
        self.lbl_title.textColor = UIColor.black
        self.lbl_subtitle.textColor = UIColor(hexString: colorGray)
        
        self.imgvw_photo .setImageCornerRound()
    }

}
