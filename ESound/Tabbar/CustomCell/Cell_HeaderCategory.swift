//
//  Cell_HeaderCategory.swift
//  ESound
//
//  Created by kirti on 15/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class Cell_HeaderCategory: UITableViewCell {

    @IBOutlet weak var lbl_subtitle: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lbl_title .SetcategoryTitle()
        self.lbl_subtitle.SetcategorySubTitle()
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.lbl_title.textAlignment = .center
            self.lbl_subtitle.textAlignment = .center
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
