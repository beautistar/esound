//
//  ClcHeader_Library.swift
//  TestApp
//
//  Created by kirti on 14/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class ClcHeader_Library: UICollectionReusableView {

    @IBOutlet weak var btn_favourites: UIButton!
    @IBOutlet weak var btn_recents: UIButton!
    @IBOutlet weak var btn_artist: UIButton!
    @IBOutlet weak var btn_playlist: UIButton!
    @IBOutlet weak var btn_tracks: UIButton!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_sibtitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.lbl_title.SetcategoryTitle()
        self.lbl_sibtitle.SetcategorySubTitle()
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.lbl_title.textAlignment = .center
             self.lbl_sibtitle.textAlignment = .center
        }
    }
    
}
