//
//  ClcBrowseCellIpad.swift
//  ESound
//
//  Created by kirti on 05/10/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class ClcBrowseCellIpad: UICollectionViewCell {

  
    @IBOutlet weak var btn: UIButton!
 
    
    @IBOutlet weak var lbl_subtitle: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var imgvw: UIImageView!
    @IBOutlet weak var imgvw_status: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imgvw .setSmallImageCornerRound()
        self.lbl_title.font = UIFont.init(name: RobotoRegular, size: FontSizeNormal)
        self.lbl_subtitle.font = UIFont.init(name: RobotoRegular, size: FontSizeSmall)
        self.lbl_title.textColor = UIColor.black
        self.lbl_subtitle.textColor = UIColor.darkGray
        
       
        
              
            
        
        
    }

}
