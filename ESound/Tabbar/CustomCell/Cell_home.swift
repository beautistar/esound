//
//  Cell_TopPlaylist.swift
//  TestApp
//
//  Created by kirti on 14/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit
import SDWebImage
import Crashlytics

let kpictureMediuum = "picture_medium"
class Cell_home: UITableViewCell {

    @IBOutlet weak var collectionview_home: UICollectionView!
    @IBOutlet weak var lbl_subtitle: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
   // @IBOutlet weak var btn_more: UIButton!
    var objParentvc = UIViewController()
    var tag_identify = Int()
    var arr_deezerList = [PlayListModel]()
    var arr_dailyMix = [PlayListModel]()
    var arr_PlayListBrowseDetail = [PlayListModel]()
    var arr_TopPlayListBrowseDetail = [PlayListModel]()
    
     var arr_recentList = [Music]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let nib_CellFeatured = UINib(nibName: "CellCollection_home", bundle: nil)
        self.collectionview_home.register(nib_CellFeatured, forCellWithReuseIdentifier: "CellCollection_home")
        
        let nib_CellSongs = UINib(nibName: "CellCollection_library", bundle: nil)
        self.collectionview_home.register(nib_CellSongs, forCellWithReuseIdentifier: "CellCollection_library")
        
        if let flowLayout = self.collectionview_home.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.itemSize = CGSize(width: 180, height: 210)
            flowLayout.sectionInset = .init(top: 0, left: 10, bottom: 0, right: 10)
            self.collectionview_home.collectionViewLayout = flowLayout
        }
        self.lbl_title .SetcategoryTitle()
        self.lbl_subtitle.SetcategorySubTitle()
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.lbl_title.textAlignment = .center
            self.lbl_subtitle.textAlignment = .center
        }
     
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension Cell_home:UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if objParentvc is HomeVC || objParentvc is ArtistDetailVC  {
            if tag_identify == 0  {
                return arr_deezerList.count
            }
           else  if tag_identify == 1  {
                return arr_dailyMix.count
            }
            else if tag_identify == 2  {
                return arr_recentList.count
            }
            
        }
        else  if objParentvc is BrowseDetailVC {
            if tag_identify == 0  {
                return arr_PlayListBrowseDetail.count
            }
            else if tag_identify == 1  {
                return arr_TopPlayListBrowseDetail.count
            }
        }
        else  if objParentvc is ArtistDetailVC {
            if tag_identify == 0  {
                return arr_deezerList.count
            }
            else if tag_identify == 1  {
                return arr_dailyMix.count
            }
        }
        return 0
       
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if objParentvc is HomeVC  || objParentvc is ArtistDetailVC {
            
            if tag_identify == 0 {
                let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollection_home", for: indexPath) as! CellCollection_home
                let obj = arr_deezerList[indexPath.row]
                cell.lbl_name.text = obj.title
                if let strUrl = obj.url {
                    cell.imgvw .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
                    }
                }
                return cell
            }
            else if tag_identify == 1 {
                let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollection_home", for: indexPath) as! CellCollection_home
                let obj = arr_dailyMix[indexPath.row]
                cell.lbl_name.text = obj.title
                if let strUrl = obj.url {
                    cell.imgvw .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
                    }
                }
                return cell
            }
            else   {
                let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollection_library", for: indexPath) as! CellCollection_library
                cell.imgvw_photo .setImageCornerRound()
                let obj = arr_recentList[indexPath.row]
                cell.lbl_title.text = obj.trackTitle
                if let strUrl = obj.trackImageUrl {
                    cell.imgvw_photo .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
                    }
                }
              //  cell.lbl_title.text = "Austria Rankings"
                cell.lbl_subtitle.text = ""
                return cell
                
            }
        }
        else if objParentvc is BrowseDetailVC {
            if tag_identify == 0  {
                let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollection_home", for: indexPath) as! CellCollection_home
                let objPlayList = arr_PlayListBrowseDetail[indexPath.row]
                cell.lbl_name.text = objPlayList.title
                if let strUrl = objPlayList.url {
                    cell.imgvw .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
                    }
                }
                
                return cell
            }else {
                let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollection_home", for: indexPath) as! CellCollection_home
                let objPlayList = arr_TopPlayListBrowseDetail[indexPath.row]
                cell.lbl_name.text = "" //objPlayList.title
                if indexPath.row == 0 {
                    if let strUrl = objPlayList.url {
                        cell.imgvw .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
                        }
                    }
                }else {
                    cell.lbl_center.text = objPlayList.title! + "\n" + objPlayList.subtitle!
                    cell.imgvw.backgroundColor = UIColor.clear
                }
                  return cell
                
            }
        }
        else {
            let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollection_home", for: indexPath) as! CellCollection_home
            cell.lbl_name.text = "Feel Good Dance"
            cell.lbl_name.font = UIFont(name: RobotoRegular, size: FontSizeSmall)
            cell.imgvw.image = UIImage(named: "1")
           
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       // Crashlytics.sharedInstance().crash()
        
        if objParentvc is ArtistDetailVC {
           
            if tag_identify == 0  {
                let obj = self.objParentvc.tabbarStoryBoard().instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
                obj.objPlayList = arr_deezerList[indexPath.row]
                obj.isFromHomeTab = true
                objParentvc.navigationController?.pushViewController(obj, animated: true)
            }
            else if tag_identify == 1  {
                let obj = objParentvc.libraryStoryBoard().instantiateViewController(withIdentifier: "ArtistDetailVC") as! ArtistDetailVC
                obj.objPlayList = arr_dailyMix[indexPath.row]
                 objParentvc.navigationController?.pushViewController(obj, animated: true)
            }
            
         
        }else {
            
            if objParentvc is HomeVC {
                
                if tag_identify == 0  {
                    let obj = self.objParentvc.storyboard?.instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
                    obj.objPlayList = arr_deezerList[indexPath.row]
                    obj.isFromHomeTab = true
                    objParentvc.navigationController?.pushViewController(obj, animated: true)
                }
                else if tag_identify == 1  {
                    let obj = self.objParentvc.storyboard?.instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
                    obj.objPlayList = arr_dailyMix[indexPath.row]
                    obj.isFromDailyMix = true
                    objParentvc.navigationController?.pushViewController(obj, animated: true)
                }else {
                    objAppdelegate.objActiveMusic = self.arr_recentList[indexPath.row]
                    let object = ["index":indexPath.row,"object":self.arr_recentList] as [String : Any]
                    NotificationCenter.default.post(name:OpenPlayerNotification, object: nil, userInfo: object)
                  //  self.cl .reloadData()
                }
            }
            if objParentvc is BrowseDetailVC {
                
                if tag_identify == 0  {
                    let obj = self.objParentvc.storyboard?.instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
                    obj.objPlayList = arr_PlayListBrowseDetail[indexPath.row]
                    obj.isFromBrowse = true
                    objParentvc.navigationController?.pushViewController(obj, animated: true)
                }
                if tag_identify == 1  {
                    let obj = self.objParentvc.storyboard?.instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
                    obj.objPlayList = arr_TopPlayListBrowseDetail[0]
                    obj.isFromBrowse = true
                    objParentvc.navigationController?.pushViewController(obj, animated: true)
                }
            }
        }
       // https://api.deezer.com/playlist/1911222042/tracks
    }
}
