//
//  Cell_header.swift
//  ESound
//
//  Created by kirti on 16/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class Cell_tblheader: UITableViewCell {

    @IBOutlet weak var btn_add: UIButton!
    @IBOutlet weak var lbl_header: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lbl_header .SetcategoryTitle()
        btn_add.setTitleColor(UIColor(hexString: colorGreen), for: .normal)
        btn_add.titleLabel?.font = UIFont(name: RobotoMedium, size: FontSizeSmall)
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.lbl_header.textAlignment = .center
          
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
