//
//  cell_songslist.swift
//  TestApp
//
//  Created by kirti on 14/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class cell_songslist: UITableViewCell {
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var btn_add: UIButton!
    @IBOutlet weak var lbl_subtitle: UILabel!
    @IBOutlet weak var imgvw_photo: UIImageView!
     @IBOutlet weak var imgvw_status: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
      
        self.lbl_title.font = UIFont.init(name: RobotoRegular, size: FontSizeNormal)
        self.lbl_subtitle.font = UIFont.init(name: RobotoRegular, size: FontSizeSmall)
        self.imgvw_photo .setSmallImageCornerRound()
    }
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
