//
//  ClcHeader_browse.swift
//  ESound
//
//  Created by kirti on 15/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class ClcHeader_browse: UICollectionReusableView {
        
    @IBOutlet weak var lbl_bottomSubtitle: UILabel!
    @IBOutlet weak var lbl_bottomTitle: UILabel!
    @IBOutlet weak var clc_ranking: UICollectionView!
    
    @IBOutlet weak var consHeightClcipad: NSLayoutConstraint!
    @IBOutlet weak var clc_ipad: UICollectionView!
    
    
    
    @IBOutlet weak var lbl_topSubtitle: UILabel!
    @IBOutlet weak var btn_more: UIButton!
    @IBOutlet weak var lbl_toptitle: UILabel!
    override func awakeFromNib() {
        btn_more.setTitleColor(UIColor(hexString: colorGreen), for: .normal)
        btn_more.titleLabel?.font = UIFont(name: RobotoMedium, size: FontSizeSmall)
        
        if UIDevice.isPad {
            self.lbl_toptitle.textAlignment = .center
            self.lbl_topSubtitle.textAlignment = .center
            
            self.lbl_bottomTitle.textAlignment = .center
            self.lbl_bottomSubtitle.textAlignment = .center
        }else{
            //self.consHeightClcipad.constant = 0
        }
    }
   
}
