//
//  CellCollection_home.swift
//  TestApp
//
//  Created by kirti on 14/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class CellCollection_home: UICollectionViewCell {

    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_center: UILabel!
    @IBOutlet weak var imgvw: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.lbl_name.textColor = UIColor.black
        self.lbl_name.font = UIFont(name: RobotoRegular, size: FontSizeSmall)
         imgvw .setImageCornerRound()
        // Initialization code
    }

}
