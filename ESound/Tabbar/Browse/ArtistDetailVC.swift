//
//  ArtistDetailVC.swift
//  ESound
//
//  Created by kirti on 17/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class ArtistDetailVC: UITableViewController {
   var stretchyHeader: GSKNibStretchyHeaderView!
   
    var objPlayList:PlayListModel?
    var arr_playList = [PlayListModel]()
    var arr_artistList = [PlayListModel]()
    var isFromPlayer = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.isFromPlayer {
            let menuButton = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(btnActionDismiss))
            self.navigationItem.leftBarButtonItem  = menuButton
        }else {
            self.addBackbutton()
        }
        let cell = UINib(nibName:"Cell_home", bundle: nil)
        self.tableView.register(cell, forCellReuseIdentifier: "Cell_home")
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = CGFloat(constantHeightCell)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            let nibViews = Bundle.main.loadNibNamed("GSKNibStretchyHeaderView", owner: self, options: nil)! as NSArray
            self.stretchyHeader = nibViews.firstObject as? GSKNibStretchyHeaderView
            self.stretchyHeader.userNameLabel.text = self.objPlayList?.title
            self.navigationItem.title = self.objPlayList?.title
            self.stretchyHeader.lbl_description.text = ""
            self.hideArtistView(ishide: true)
            if let url = self.objPlayList?.url {
                self.stretchyHeader.userImage .sd_setImage(with: URL(string:url)) { (image, error, cacheType, url) in
                }
                self.stretchyHeader.backgroundImageView .sd_setImage(with: URL(string:url)) { (image, error, cacheType, url) in
                }
            }
        }
        self .getArtistDetail(playListID: objPlayList!.id)
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }

        // Do any additional setup after loading the view.
    }
    @objc func btnActionDismiss(){
        self .dismiss(animated: true, completion: nil)
    }
    func hideArtistView(ishide:Bool){
      
        if ishide {
            self.stretchyHeader.isHidden = true
            self.tableView.tableHeaderView?.isHidden = true
        }else {
            self.stretchyHeader.isHidden = false
            self.tableView.tableHeaderView?.isHidden = false
            
            self.tableView.parallaxHeader.view = self.stretchyHeader
            self.tableView.parallaxHeader.height = 260
            self.tableView.parallaxHeader.minimumHeight = 0
            self.tableView.parallaxHeader.mode = .centerFill
            self.tableView.parallaxHeader.parallaxHeaderDidScrollHandler = { parallaxHeader in
            }
        }
       
    }
    func getArtistDetail(playListID:String) {
        
        self.view .showBlurLoader()
        var strUrl =  "https://api.deezer.com/artist/\(playListID)/playlists"
        WebserviceHelper.WebapiGetMethod(strurl:strUrl, ShowProgress:false) { (isSuccess, dictData) in
            self.view .removeBluerLoader()
            if isSuccess == true {
                if let arrdata = dictData .value(forKey: "data") as? [[String:AnyObject]] {
                    for i in 0 ..< arrdata.count {
                        let dict = arrdata[i]
                        let intid = dict["id"] as? Int64
                        let url  = dict["picture_medium"] as? String ?? ""
                        let objPlayList = PlayListModel(title: dict["title"] as? String, subtitle: "", url: url, id: String(describing: intid!))
                        self.arr_playList.append(objPlayList)
                    }
                    self.hideArtistView(ishide: false)
                    self.tableView .reloadData()
                }
            }else{
                HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
            }
        }

        strUrl = "https://api.deezer.com/artist/\(playListID)/related"
        WebserviceHelper.WebapiGetMethod(strurl:strUrl, ShowProgress:false) { (isSuccess, dictData) in
            self.view .removeBluerLoader()
            if isSuccess == true {
                if let arrdata = dictData .value(forKey: "data") as? [[String:AnyObject]] {
                    for i in 0 ..< arrdata.count {
                        let dict = arrdata[i]
                        let intid = dict["id"] as? Int64
                        let url  = dict["picture_medium"] as? String ?? ""
                        let objPlayList = PlayListModel(title: dict["name"] as? String, subtitle: "", url: url, id: String(describing: intid!))
                        self.arr_artistList.append(objPlayList)
                    }
                    self.hideArtistView(ishide: false)
                    self.tableView .reloadData()
                }
            }else{
                HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
            }
            
        }
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_home", for: indexPath) as! Cell_home
        if indexPath.row == 0 {
            cell.lbl_title.text = "Playlists"
            cell.lbl_subtitle.text = "Related playlists"
           
            cell.arr_deezerList = self.arr_playList
        }
        else if indexPath.row == 1 {
            cell.lbl_title.text = "Artists"
            cell.lbl_subtitle.text = "Related artists"
            cell.arr_dailyMix = self.arr_artistList
            
        }
        cell.tag_identify = indexPath.row
        cell.objParentvc = self
        cell.collectionview_home .reloadData()
        return cell;
        
    }
    

}

