//
//  BrowseVC.swift
//  TestApp
//
//  Created by kirti on 14/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit



class BrowseVC: BaseClassVC {

    
   let collectionViewHeaderFooterReuseIdentifier = "ClcHeader_browse"
    @IBOutlet weak var clc_browse: UICollectionView!
    var clc_ranking: UICollectionView?
    var clc_ipad: UICollectionView?
    var arr_genres = [PlayListModel]()
    var arr_rankings = [PlayListModel]()
    var arr_Currentrankings = [PlayListModel]()
    var arr_RankersongsList:Array<Music> = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clc_browse.register(UINib(nibName: collectionViewHeaderFooterReuseIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:collectionViewHeaderFooterReuseIdentifier)
        
        if let flowLayout = self.clc_browse.collectionViewLayout as? UICollectionViewFlowLayout {
            if UIDevice.isPad {
                flowLayout.itemSize = CGSize(width:self.getCollectionSize()-CGFloat(15), height: self.getCollectionSize()-CGFloat(60))
                flowLayout.minimumLineSpacing = 30;
                flowLayout.minimumInteritemSpacing = 20
                flowLayout.sectionInset = .init(top: 10, left: 15, bottom: 10, right: 15)
                flowLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height:self.getCollectionSize() + CGFloat(150+180));
            }else {
                flowLayout.itemSize = CGSize(width:self.getCollectionSize()-CGFloat(5), height: self.getCollectionSize()-CGFloat(40))
                flowLayout.minimumLineSpacing = 15;
                flowLayout.sectionInset = .init(top: 0, left: 15, bottom: 0, right: 15)
               // flowLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height:self.getCollectionSize() + CGFloat(150));
                 flowLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height:self.getCollectionSize() + CGFloat(150+180));
            }
          self.clc_browse.collectionViewLayout = flowLayout
        }
        self.addProfilebutton()
        
        self.getGenerListData()
        
        self.getDeezerPlayListData()
        
    }
    override func viewWillAppear(_ animated: Bool) {
       
         self.navigationItem.title = "Browse"
    }
   func getGenerListData() {
       self.view .showBlurLoader()
       WebserviceHelper.WebapiGetMethod(strurl:"https://api.deezer.com/editorial", ShowProgress: false) { (isSuccess, dictData) in
           self.view .removeBluerLoader()
           if isSuccess == true {
               if let arrdata = dictData .value(forKey: "data") as? [[String:AnyObject]] {
                    for i in 0 ..< arrdata.count {
                       let dict = arrdata[i]
                        let intid = dict["id"] as? Int
                        if intid != 0 {
                            let objPlayList = PlayListModel(title:  dict["name"] as? String,subtitle: "", url: dict[kpictureMediuum] as? String, id: String(describing: intid!))
                                    self.arr_genres .append(objPlayList)
                        }
                    }
                   self.clc_browse .reloadData()
               }
           }else{
               HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
           }
       }
   }
    func getDeezerPlayListData() {
     
         self.view .showBlurLoader()
        WebserviceHelper .postWebServiceCall(urlString: kHostPath+kWebApi.kDeezerPlaylist, parameters: [:], encodingType: DefaultEncoding, ShowProgress: false) { (isSuccess, dictData) in
            
             self.view .removeBluerLoader()
            let locale: NSLocale = NSLocale.current as NSLocale
            let countryCode: String? = locale.languageCode
            
            if isSuccess == true {
                if let arrdata = dictData .value(forKey: "data") as? [[String:AnyObject]] {
                    
                    for i in 0 ..< arrdata.count {
                        let dict = arrdata[i]
                        let intid = dict["playlistId"] as? String
                        if let  strRegion = dict["region"] as? String {
                            
                           // var finalregion = strRegion
//                            if finalregion == "en" {
//                               finalregion = "global"
//                            }
                            let name = (regionName[strRegion] ?? "") + " "
                            let subtitle = kConstnatLbl.ktopviralfrom + " " + name
                            
                            let url = regionDict[strRegion]
                            let objPlayList = PlayListModel(title:name+kConstnatLbl.krankings,subtitle: subtitle, url:url, id: String(describing: intid!))
                            
                            if countryCode == strRegion || strRegion == "global"{
                                if strRegion == "global" {
                                    objPlayList.title = strRegion.capitalized + " " + kConstnatLbl.krankings
                                    self .getSongListData(playListid: objPlayList.id)
                                }
                                self.arr_Currentrankings .append(objPlayList)
                            }else{
                                self.arr_rankings .append(objPlayList)
                            }
                        }
                    }
                    self.clc_browse .reloadData()
                }
            }else{
                HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
            }
            
        }
       
    }
    func getSongListData(playListid:String) {
         let strUrl =  "https://api.deezer.com/playlist/\(playListid)/tracks"
            WebserviceHelper.WebapiGetMethod(strurl:strUrl, ShowProgress: false) { (isSuccess, dictData) in
                if isSuccess == true {
                    if let arrdata = dictData .value(forKey: "data") as? [[String:AnyObject]] {
                        for i in 0 ..< arrdata.count {
                            let dict = arrdata[i]
                            let obj = Music()
                            obj.trackTitle = dict["title"] as? String ?? ""
                            obj.trackImageUrl = dict["album"]?["cover_big"] as? String ?? ""
                            obj.musicNum = i
                            obj.deezerTrackID = HelperClass .getValidationstring(dict: dict, key: "id")
                            obj.authorName = dict["artist"]?["name"] as? String ?? ""
                            obj.authorImageUrl = dict["artist"]?["picture"] as? String ?? ""
                            obj.authorID = HelperClass .getValidationstring(dict: dict["artist"] as! [String:Any], key: "id")
                            self.arr_RankersongsList .append(obj)
                        }
                         self.clc_ipad? .reloadData()
                    }
                }else{
                    HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
                }
            }
              
        }
    
    @objc func btnActionPlusClick(sender:UIButton) {
        
        let point = sender.convert(CGPoint.zero, to: self.clc_ipad)
        let indexPath = self.clc_ipad?.indexPathForItem(at: point)
        let objMusic = self.arr_RankersongsList[indexPath!.row]
        if objMusic.isOffline == "0" , (objMusic.download_status == "" || objMusic.download_status == "0") {
            if Connectivity.connetivityAvailable() == false {
                HelperClass .showAlertMessage(title: errOfflinetitle, message: errOfflineDownload)
                return
            }
            self.saveTrackOffline(objMusic: objMusic)
            objMusic.isOffline = "1"
            objMusic.download_status = "0"
             HelperClass.shared().addObjectinGlobal(objMusic: objMusic) { (sucess) in
                self.clc_ipad! .reloadData()
            }
           
            DownloadManager.shared().downloadSong(track: objMusic) { (sucess, objmusic, error) in
                objMusic.download_status = "1"
                 HelperClass.shared().addObjectinGlobal(objMusic: objMusic) { (sucess) in
                    self.clc_ipad! .reloadData()
                }
            }
        }
        else if objMusic.isOffline == "1" ,objMusic.download_status == "0" {
            // waiting to download
            
        }
        else if objMusic.isOffline.count < 1  {
            HelperClass .SaveTrackData(objMusic: objMusic, showProgress: true) { (sucess, objmusic) in
                self.clc_ipad! .reloadData()
            }
        }
    }

    
}

extension BrowseVC:UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.clc_ranking {
            return self.arr_Currentrankings.count
        }
        else if collectionView == self.clc_ipad {
            return self.arr_RankersongsList.count
        }
        return self.arr_genres.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.clc_ranking {
            let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollection_home", for: indexPath) as! CellCollection_home
            
            let obj = self.arr_Currentrankings[indexPath.row]
            cell.lbl_name.text = obj.title
            if let strUrl = obj.url {
                cell.imgvw .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
                }
            }
            return cell
        }
        else if collectionView == self.clc_ipad {
            let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "ClcBrowseCellIpad", for: indexPath) as! ClcBrowseCellIpad
            
            let obj = self.arr_RankersongsList [indexPath.row]
            cell.lbl_title.text = obj.trackTitle
            cell.lbl_subtitle.text = obj.authorName
            if let strUrl = obj.trackImageUrl {
                cell.imgvw .sd_setImage(with:URL(string:strUrl)) { (image, error, cacheType, url) in
                }
            }
            cell.btn.addTarget(self, action: #selector(btnActionPlusClick(sender:)), for: .touchUpInside)
            self .setTextColor(objModel: obj, label:cell.lbl_title)
           self.arr_RankersongsList[indexPath.row] = self.setButtomImage(objModel: obj, btn: cell.imgvw_status)
            return cell
        }
        else {
            let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
            let imgvw = cell.contentView .viewWithTag(10) as! UIImageView
            let lbl_title = cell.contentView .viewWithTag(11) as! UILabel
            imgvw .setImageCornerRound()
            let obj = arr_genres[indexPath.row]
            lbl_title.text = obj.title
            if let strUrl = obj.url {
                imgvw .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
                }
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
        
       // let dict = arr_rankings[111]
        if collectionView == self.clc_ranking {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
            obj.objPlayList = arr_Currentrankings[indexPath.row]
            obj.isFromRankings = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if collectionView == self.clc_ipad {
               objAppdelegate.objActiveMusic = self.arr_RankersongsList[indexPath.row]
               let object = ["index":indexPath.row,"object":self.arr_RankersongsList] as [String : Any]
                NotificationCenter.default.post(name:OpenPlayerNotification, object: nil, userInfo: object)
                self.clc_ipad?.reloadData()
        }
        else {
            let objPlaylist = arr_genres[indexPath.row]
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "BrowseDetailVC") as! BrowseDetailVC
            obj.objPlayList = objPlaylist
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: collectionViewHeaderFooterReuseIdentifier, for: indexPath) as! ClcHeader_browse
        clc_ranking = headerView.viewWithTag(10) as? UICollectionView
        clc_ipad = headerView.viewWithTag(101) as? UICollectionView
        
        let nib_CellFeatured = UINib(nibName: "CellCollection_home", bundle: nil)
        clc_ranking?.register(nib_CellFeatured, forCellWithReuseIdentifier: "CellCollection_home")
        
        let nib_CellFeatured1 = UINib(nibName: "ClcBrowseCellIpad", bundle: nil)
        clc_ipad?.register(nib_CellFeatured1, forCellWithReuseIdentifier: "ClcBrowseCellIpad")
        if let flowLayout = clc_ipad?.collectionViewLayout as? UICollectionViewFlowLayout {
            if UIDevice.isPad {
                 flowLayout.itemSize = CGSize(width: 360, height: 50)
            }else{
                flowLayout.itemSize = CGSize(width: UIScreen.main.bounds.width-10, height: 50)
            }
          
            flowLayout.sectionInset = .init(top: 0, left: 15, bottom: 10, right: 15)
            clc_ipad?.collectionViewLayout = flowLayout
        }
        
        clc_ipad?.delegate = self
        clc_ipad?.dataSource = self
        clc_ipad?.reloadData()
        
        if let flowLayout = clc_ranking?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.itemSize = CGSize(width: self.getCollectionSize(), height: self.getCollectionSize()+CGFloat(25))
            flowLayout.sectionInset = .init(top: 0, left: 15, bottom: 0, right: 15)
             clc_ranking?.collectionViewLayout = flowLayout
        }
        
        headerView.lbl_toptitle .SetcategoryTitle()
        headerView.lbl_topSubtitle .SetcategorySubTitle()
        
        headerView.lbl_bottomTitle .SetcategoryTitle()
        headerView.lbl_bottomSubtitle.SetcategorySubTitle()
        headerView.btn_more .addTarget(self, action: #selector(btnActionMoreClicked), for: .touchUpInside)
        
        clc_ranking?.delegate = self
        clc_ranking?.dataSource = self
        clc_ranking?.reloadData()
  
            return headerView
  
     
    }
    @objc func btnActionMoreClicked() {
        
       // self.navigationItem.title = ""
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "RankingsVC") as! RankingsVC
        obj.arr_rankingList = self.arr_rankings
        self.navigationController?.pushViewController(obj, animated: true)
    }
   
}
