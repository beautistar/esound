//
//  BrowseDetailVC.swift
//  ESound
//
//  Created by kirti on 15/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class BrowseDetailVC:UITableViewController {
    
 var stretchyHeader: GSKNibStretchyHeaderView!
     var dictData = [String:AnyObject]()
    var arr_PlayList = [PlayListModel]()
    var arr_TopPlayList = [PlayListModel]()
    var objPlayList : PlayListModel?
    var arr_TracksList = [PlayListModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        let cell = UINib(nibName: "Cell_home", bundle: nil)
        self.tableView.register(cell, forCellReuseIdentifier: "Cell_home")
        
        let cell_header = UINib(nibName: "Cell_HeaderCategory", bundle: nil)
        self.tableView.register(cell_header, forCellReuseIdentifier: "Cell_HeaderCategory")
        
        let cell_song = UINib(nibName: "cell_songslist", bundle: nil)
        self.tableView.register(cell_song, forCellReuseIdentifier: "cell_songslist")
        
        
        let nibViews = Bundle.main.loadNibNamed("GSKNibStretchyHeaderView", owner: self, options: nil)! as NSArray
        self.stretchyHeader = nibViews.firstObject as? GSKNibStretchyHeaderView
        self.stretchyHeader.userNameLabel.text = objPlayList?.title
        
        if let strUrl = objPlayList?.url {
            self.stretchyHeader.userImage .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
            }
            self.stretchyHeader.backgroundImageView .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
            }
        }

        if #available(iOS 13.0, *) {
            self.tableView.backgroundColor = UIColor.systemBackground
        } else {
            self.tableView.backgroundColor = UIColor.white
        }
        self.navigationItem.title = objPlayList?.title
        self.addBackbutton()
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        self.getGenerDetailListData()
        
    }
    func showParalaxHeader() {
         self.tableView.parallaxHeader.view = self.stretchyHeader
         self.tableView.parallaxHeader.height = 260
         self.tableView.parallaxHeader.minimumHeight = 0
         self.tableView.parallaxHeader.mode = .centerFill
         self.tableView.parallaxHeader.parallaxHeaderDidScrollHandler = { parallaxHeader in
        }
    }
    func getGenerDetailListData() {
        let idData =  (objPlayList?.id)!
        self.view .showBlurLoader()
        WebserviceHelper.WebapiGetMethod(strurl:"https://api.deezer.com/editorial/" + idData + "/charts", ShowProgress: false) { (isSuccess, dictData) in
            self.view .removeBluerLoader()
            self .showParalaxHeader()
            if isSuccess == true {
                if let arrdata = dictData .value(forKeyPath: "playlists.data") as? [[String:AnyObject]] {
                    for i in 0 ..< arrdata.count {
                        let dict = arrdata[i]
                        let intid = dict["id"] as? Int64
                        let url  = dict["picture_medium"] as? String ?? ""
                        let subtitle  = dict["user"]?["name"] as? String ?? ""
                        if i == 0 {
                            var objPlayList = PlayListModel(title: dict["title"] as? String, subtitle: subtitle, url: url, id: String(describing: intid!))
                                self.arr_TopPlayList .append(objPlayList)
                           objPlayList = PlayListModel(title: dict["title"] as? String, subtitle:"\(dict["nb_tracks"]!) tracks", url: nil, id: String(describing: intid!))
                            self.arr_TopPlayList .append(objPlayList)
                        }else{
                            let objPlayList = PlayListModel(title: dict["title"] as? String, subtitle: subtitle, url: url, id: String(describing: intid!))
                             self.arr_PlayList .append(objPlayList)
                        }
                    }
                }
                if let arrdata = dictData .value(forKeyPath: "tracks.data") as? [[String:AnyObject]] {
                    for i in 0 ..< arrdata.count {
                        if i < 3 {
                            let dict = arrdata[i]
                            let intid = dict["id"] as? Int64
                            let url  = dict["album"]?["cover_small"] as? String ?? ""
                            let subtitle  = dict["artist"]?["name"] as? String ?? ""
                            let objPlayList = PlayListModel(title: dict["title"] as? String, subtitle: subtitle, url: url, id: String(describing: intid!))
                            self.arr_TracksList .append(objPlayList)
                        }
                    }
                }
                self.tableView .reloadData()
                
            }else{
                HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
            }
        }
    }
 
 

}

extension BrowseDetailVC
{
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 ||    section == 1  {
            return 1
        }
        return self.arr_TracksList.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 2 {
            
            let cellsong = tableView.dequeueReusableCell(withIdentifier: "cell_songslist", for: indexPath) as! cell_songslist
            let objtracks = arr_TracksList[indexPath.row]
            cellsong.lbl_title.text = objtracks.title
            cellsong.lbl_subtitle.text = objtracks.subtitle
            if let strUrl = objtracks.url {
                cellsong.imgvw_photo .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
                }
            }
            return cellsong
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_home", for: indexPath) as! Cell_home
            if indexPath.section == 0 {
                cell.lbl_title.text = "Playlists"
                cell.lbl_subtitle.text = "Best playlists from " + (objPlayList?.title ?? "")
                cell.arr_PlayListBrowseDetail = self.arr_PlayList
            }
            else if indexPath.section == 1 {
                cell.lbl_title.text = "Top Playlist"
                cell.lbl_subtitle.text = "Most popular playlist from " + (objPlayList?.title ?? "")
                cell.arr_TopPlayListBrowseDetail = self.arr_TopPlayList
            }
            cell.tag_identify = indexPath.section
            cell.objParentvc = self
            cell.collectionview_home .reloadData()
            return cell
        }
        
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_HeaderCategory") as! Cell_HeaderCategory
            cell.lbl_title.text = "Tracks"
            cell.lbl_subtitle.text = "Best tracks from " + (objPlayList?.title ?? "")
            if #available(iOS 13.0, *) {
                cell.backgroundColor = UIColor.systemBackground
            } else {
                cell.backgroundColor = UIColor.white
            }
            return cell
            
        }else {
            let view = UIView.init()
           // view.backgroundColor = UIColor.red
            return view
        }
        
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            return 55
        }else {
            return 0
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 2 {
            return  60
        }else {
            return CGFloat(constantHeightCell)
        }
    }
}
