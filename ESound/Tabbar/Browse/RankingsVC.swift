//
//  RankingsVC.swift
//  TestApp
//
//  Created by kirti on 14/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class RankingsVC: UIViewController {

    @IBOutlet weak var clc_Rankings: UICollectionView!
    var arr_rankingList = [PlayListModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib_CellFeatured = UINib(nibName: "CellCollection_library", bundle: nil)
        self.clc_Rankings.register(nib_CellFeatured, forCellWithReuseIdentifier: "CellCollection_library")
        
        self.navigationItem.title = "Rankings"
        self.addBackbutton()
        
        if let flowLayout = self.clc_Rankings.collectionViewLayout as? UICollectionViewFlowLayout {
            let padding: CGFloat =  35
            flowLayout.itemSize = CGSize(width:self.getCollectionSize(), height: self.getCollectionSize() + padding)
            flowLayout.sectionInset = .init(top: 0, left: 10, bottom: 0, right: 10)
            self.clc_Rankings.collectionViewLayout = flowLayout
        }
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
       

       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
       
    }


}
extension RankingsVC:UIGestureRecognizerDelegate
{
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
           return true
       }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
extension RankingsVC:UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arr_rankingList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollection_library", for: indexPath) as! CellCollection_library
        cell.lbl_title.textAlignment = .center
        cell.lbl_subtitle.textAlignment = .center
      
        let objData = self.arr_rankingList[indexPath.row]
        cell.lbl_title.text =  objData.title //"Austria Rankings"
        cell.lbl_subtitle.text = objData.subtitle
        if let strUrl = objData.url {
//            cell.imgvw_photo.sd_setImage(with:  URL(string: strUrl), placeholderImage: nil, options: .progressiveLoad) { (image, error, cacheType, url) in
//
//            }
            cell.imgvw_photo .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let objData = self.arr_rankingList[indexPath.row]
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
        obj.isFromRankings = true
        obj.objPlayList = objData
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
}
