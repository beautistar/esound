//
//  AddSongsVC.swift
//  ESound
//
//  Created by kirti on 17/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class AddSongsVC: UIViewController {

    @IBOutlet weak var searchBar_songs: UISearchBar!
    @IBOutlet weak var tbl_addSongs: UITableView!
    var arr_songsList = [Music]()
    var arr_globalsongsList = [Music]()
    var arr_selected = [Music]()
    var strPlayListID = String()
    var objParentVC:SongsListVC!
    @IBOutlet weak var btn_add: UIButton!
    @IBOutlet weak var btn_cancel: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cell = UINib(nibName: "cell_songslist", bundle: nil)
        self.tbl_addSongs.register(cell, forCellReuseIdentifier: "cell_songslist")
        self.tbl_addSongs.tableFooterView = UIView.init()
        
        self.tbl_addSongs.estimatedRowHeight = 55
        self.tbl_addSongs.rowHeight = 55
        
        self.navigationItem.title = "Songs to add"
        self.btn_add.setButtonWithGreenColor()
        self.btn_cancel.setButtonWithBorderAndCorner()
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        self.getSongList()

    }
    func getSongList() {
        
        self.arr_songsList = HelperClass .getTracklistArray()
        arr_globalsongsList = self.arr_songsList
        self.tbl_addSongs .reloadData()
       
    }
    @IBAction func btnActioncancelClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnActionAddClicked(_ sender: UIButton) {
        
        if Connectivity.connetivityAvailable() == false {
            HelperClass .showAlertMessage(title: errOfflinetitle, message: errChangedLibraryOffline)
            return
        }
        
        var arrdata = [[String:AnyObject]]()
        var arrdataFinal = [Music]()
        
        for i in 0..<arr_selected.count {
            let obj = arr_selected[i]
            if self.objParentVC.arr_songsList.contains(where: { (objMUSIC) -> Bool in
                return objMUSIC.trackUserID == obj.trackUserID
            })  == false {
                
                let common = [
                    "trackUserID": obj.trackUserID,
                    "pos":self.objParentVC.arr_songsList.count + i] as [String : AnyObject]
                arrdata .append(common)
                arrdataFinal .append(obj)
            }
        }
        
        if arrdataFinal.count == 0 {
            self .dismiss(animated: true, completion: nil)
            return
        }
        
//        for obj in arr_selected {
//            let common = [
//                "trackUserID": obj.trackUserID,
//                "pos":0] as [String : AnyObject]
//            arrdata .append(common)
//        }
        
        let parameter = ["playlistID":strPlayListID,
                         "tracks":arrdata] as [String : AnyObject]
        WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.kinsertSingleinPlaylist, parameters:parameter, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
            if isSuccess == true {
                self.objParentVC.arr_songsList .append(contentsOf:arrdataFinal)
                self.objParentVC.tableView .reloadData()
                self .dismiss(animated: true, completion: nil)
            }else{
                HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
            }
        }
    }
  
}
extension AddSongsVC:UITableViewDelegate,UITableViewDataSource
{
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_songsList.count
     }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellsong = tableView.dequeueReusableCell(withIdentifier: "cell_songslist", for: indexPath) as! cell_songslist
        cellsong.tintColor = UIColor(hexString:colorGreen)
        let objData = self.arr_songsList[indexPath.row]
        cellsong.imgvw_photo.image = UIImage(named: "10")
        cellsong.btn_add.isHidden = true
        cellsong.lbl_title.text = objData.trackTitle
        cellsong.lbl_subtitle.text = objData.authorName
        if let strUrl = objData.trackImageUrl {
            cellsong.imgvw_photo .sd_setImage(with:URL(string:strUrl)) { (image, error, cacheType, url) in
            }
        }
         cellsong.accessoryType = .checkmark

        if arr_selected.contains(where: { (objtrackList) -> Bool in
            return objtrackList.deezerTrackID == objData.deezerTrackID
        }) {
          cellsong.accessoryType = .checkmark
        }else {
           cellsong.accessoryType = .none
        }
        return cellsong
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let objData = self.arr_songsList[indexPath.row]
        if arr_selected.contains(where: { (objtrackList) -> Bool in
            return objtrackList.deezerTrackID == objData.deezerTrackID
        }) {
            arr_selected .removeAll { (objtrackList) -> Bool in
                return objtrackList.deezerTrackID == objData.deezerTrackID
            }
        }else {
            arr_selected .append(self.arr_songsList[indexPath.row])
        }
        self.tbl_addSongs .reloadRows(at: [indexPath], with: .none)
    }
    
    
}
extension AddSongsVC:UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let arrSong = self.arr_globalsongsList.filter { (objTrackList) -> Bool in
            return objTrackList.trackTitle.lowercased() == searchText.lowercased()
        }
        self.arr_songsList = arrSong
        self.tbl_addSongs .reloadData()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.arr_songsList = self.arr_globalsongsList
        self.tbl_addSongs .reloadData()
    }
    
}
