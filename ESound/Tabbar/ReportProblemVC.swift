//
//  ReportProblemVC.swift
//  ESound
//
//  Created by kirti on 15.10.19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class ReportProblemVC: UIViewController {

    
    let trackProblem0 = "Name and song don't match"
    let trackProblem1 = "Can't get this song"
    let trackProblemOther = "Other"
    let trackProblemMorePlaceholder = "Write more details, if you want..."
    let trackProblemSend = "Send"
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var btn_save: UIButton!
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var tbl_problem: UITableView!
    @IBOutlet weak var textview: KMPlaceholderTextView!
    var arr_error = [String]()
    
    var objActiveMusic :Music?
    var strReason = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Report a problem with"
        
        arr_error .append(trackProblem0)
        arr_error .append(trackProblem1)
        arr_error .append(trackProblemOther)
        strReason = 0
        
        let cell = UINib(nibName: "cell_songslist", bundle: nil)
        self.tbl_problem.register(cell, forCellReuseIdentifier: "cell_songslist")
        self.textview.placeholder = trackProblemMorePlaceholder
        
        self.btn_save .setTitle(trackProblemSend, for: .normal)
        
        self.btn_Cancel .setButtonWithBorderAndCorner()
        self.btn_save .setButtonWithGreenColor()
        
    }
    @IBAction func btnActionCancelClicked(_ sender: Any) {
           self .dismiss(animated: true, completion: nil)
       }
       @IBAction func btnActionSaveClicked(_ sender: UIButton) {
           
           let parameters = [
               "userID": UserDefaults.standard.string(forKey: kUserid)!,
               "deezerTrackID":objActiveMusic?.deezerTrackID!,
               "reason": strReason,
               "message": textview.text!] as [String : AnyObject]
           WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.kReportSongs, parameters:parameters, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
               if isSuccess == true {
                   if let dict = dictData .object(forKey: "data") as? [String:AnyObject]{
                       if let data = dict["user"] as? [String:AnyObject] {
                           self.dismiss(animated: true, completion: nil)
                       }
                       
                   }
               }else{
                   HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
               }
           }
       }


}
extension ReportProblemVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_songslist", for: indexPath) as! cell_songslist
        cell.lbl_title.text =  objActiveMusic?.trackTitle
        cell.lbl_subtitle.text = objActiveMusic?.authorName
        if let strUrl = objActiveMusic?.trackImageUrl {
            cell.imgvw_photo .sd_setImage(with:URL(string:strUrl)) { (image, error, cacheType, url) in
            }
        }
        cell.btn_add.isHidden = true
        return cell
    }
}
extension ReportProblemVC:UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arr_error.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.arr_error[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
         strReason = row
    }
}
