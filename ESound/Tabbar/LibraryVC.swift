//
//  LibraryVC.swift
//  TestApp
//
//  Created by kirti on 14/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit
import AVFoundation

let tag_Tracks = 10
let tag_Playlits = 11
let tag_Artists = 12
let tag_Recents = 13
let tag_Favourites = 14
let tag_FromSearchSongs = 15
let tag_albums = 16

class LibraryVC: BaseClassVC {

     let ReuseIdentifier = "ClcHeader_Library"
    var arr_playLists = [PlayListModel]()
    var arr_trackLists = [Music]()
  
    @IBOutlet weak var clc_library: UICollectionView!
    var objClcHeader = ClcHeader_Library()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib_CellFeatured = UINib(nibName: "CellCollection_library", bundle: nil)
        self.clc_library.register(nib_CellFeatured, forCellWithReuseIdentifier: "CellCollection_library")
        self.clc_library.register(UINib(nibName: ReuseIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:ReuseIdentifier)
        
        if let flowLayout = self.clc_library.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.itemSize = CGSize(width: self.getCollectionSize(), height:self.getCollectionSize() + 45)
            flowLayout.sectionInset = .init(top: 0, left: 10, bottom: 0, right: 10)
            flowLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 310);
            self.clc_library.collectionViewLayout = flowLayout
        }
       
        self.addProfilebutton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationItem.title = "Library"
        
        let queue1 = DispatchQueue(label:"com.appcoda.queue1", qos: DispatchQoS.background)
        queue1.async {
            self.loadData()
//            HelperClass .getUserPlayList { (sucess) in
//                self.loadData()
//            }
        }
       
    
    }
    override func viewDidAppear(_ animated: Bool) {
        
        HelperClass .getUserPlayList { (sucess) in
            self.loadData()
        }
    }
    
    func loadData() {
        
        let str_PlayLists = String(format: "Select * from tbl_playlist where id != '0' order by  cast (id as int) DESC")
        self.arr_playLists = DataManager.initDB().RETRIEVE_PlayList(query: str_PlayLists)
        
       // self.arr_playLists = HelperClass .getPlaylistArray()
        DispatchQueue.main.async {
            self.clc_library .reloadData()
        }
    }
    
    @objc func btnActionClicked(sender:UIButton) {
                
        if sender.tag == tag_Playlits {
            let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "TracksListVC") as! TracksListVC
            obj.tagIdentify = tag_Playlits
            obj.arr_playList = self.arr_playLists
            self.navigationController?.pushViewController(obj, animated: true)
        }else if sender.tag == tag_Tracks {
            self.arr_trackLists = HelperClass.getTracklistArray()
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
            obj.objPlayList = PlayListModel(title: "Tracks", subtitle: "", url: "", id: "-1")
            obj.arr_songsList = self.arr_trackLists
            obj.isFromTracks = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if sender.tag == tag_Recents {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
            obj.objPlayList = PlayListModel(title: "Recents", subtitle: "", url: "", id: "-1")
            obj.arr_songsList = DataManager.initDB().RETRIEVE_Songs(query:recentDataQuery)
            obj.isFromTracks = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if sender.tag == tag_Artists {
         
             let str_QuerySongs = String(format:"SELECT authorID , COUNT(*) ,authorName,authorImageUrl FROM tbl_songs GROUP BY authorID")
            let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "TracksListVC") as! TracksListVC
            obj.tagIdentify = tag_Artists
            obj.arr_artistList = DataManager.initDB().RETRIEVE_Artist(query: str_QuerySongs)
            obj.isFromLibraryPage = true
            self.navigationController?.pushViewController(obj, animated: true)
            
//            DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
//                let str_QuerySongs = String(format:"SELECT authorID , COUNT(*) ,authorName,authorImageUrl FROM tbl_songs GROUP BY authorID")
//                let array = DataManager.initDB().RETRIEVE_Artist(query: str_QuerySongs)
//                obj.arr_artistList = array
//                obj.tbl_tracks .reloadData()
//            }
            
            
        }
        else if sender.tag == tag_Favourites {
            let str_QuerySongs = String(format:"SELECT *FROM tbl_songs ORDER BY cast (listenTimes as int)  DESC LIMIT 0,25")
            let array = DataManager.initDB().RETRIEVE_Songs(query: str_QuerySongs)
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
            obj.objPlayList = PlayListModel(title: "Tracks", subtitle: "", url: "", id: "-1")
            obj.arr_songsList = array
            obj.isFromTracks = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
}

extension LibraryVC:UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr_playLists.count + 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollection_library", for: indexPath) as! CellCollection_library
        cell.lbl_title.textAlignment = .center
        cell.lbl_subtitle.textAlignment = .center
        cell.imgvw_photo.backgroundColor = UIColor.clear
        
        if indexPath.row == arr_playLists.count {
            cell.imgvw_photo.backgroundColor = UIColor(hexString: "D0D1D1")
            cell.imgvw_photo.image = UIImage(named: "plus-black-Big")
            cell.lbl_title.text = "New playlist"
            cell.lbl_subtitle.text = "Whatever you want"
        }else{
            let obj = arr_playLists[indexPath.row]
            cell.lbl_title.text = obj.title
            cell.lbl_subtitle.text = obj.subtitle
            cell.imgvw_photo!.sd_setImage(with: URL(string:obj.url ?? ""), placeholderImage: UIImage(named: "emptyPlaceholder"), options:[]) { (image, error, cacheType, url) in
                }
       
        }
        
        cell.imgvw_photo .setImageCornerRound()
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == arr_playLists.count {
            let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "AddPlaylistVC") as! AddPlaylistVC
            obj.fromtype = .Playlist
            self.present(obj.setNavigationBarController(), animated: true, completion: nil)
        }else {
            let objModel = self.arr_playLists[indexPath.row]
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
            obj.objPlayList = objModel
            obj.isFromLibrarySongs = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        objClcHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: ReuseIdentifier, for: indexPath) as! ClcHeader_Library
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
            self.objClcHeader.btn_tracks.addTopBorderWithColor(color:UIColor.lightGray, width: 0.5)
            self.objClcHeader.btn_playlist.addTopBorderWithColor(color:UIColor.lightGray, width: 0.5)
            self.objClcHeader.btn_artist.addTopBorderWithColor(color:UIColor.lightGray, width: 0.5)
            self.objClcHeader.btn_recents.addTopBorderWithColor(color:UIColor.lightGray, width: 0.5)
            self.objClcHeader.btn_favourites.addTopBorderWithColor(color:UIColor.lightGray, width: 0.5)
        }
        objClcHeader.btn_tracks .addTarget(self, action: #selector(btnActionClicked(sender:)), for: .touchUpInside)
        objClcHeader.btn_playlist .addTarget(self, action: #selector(btnActionClicked(sender:)), for: .touchUpInside)
        objClcHeader.btn_artist .addTarget(self, action: #selector(btnActionClicked(sender:)), for: .touchUpInside)
        objClcHeader.btn_recents .addTarget(self, action: #selector(btnActionClicked(sender:)), for: .touchUpInside)
        objClcHeader.btn_favourites .addTarget(self, action: #selector(btnActionClicked(sender:)), for: .touchUpInside)
        
        return objClcHeader
    }
    
}
