//
//  ViewController.swift
//  TestApp
//
//  Created by kirti on 18/07/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit
import PKHUD


let constantHeightCell = 280
let recentDataQuery = "SELECT *FROM tbl_songs ORDER BY  listenDate DESC LIMIT 0,25"
class HomeVC: BaseClassVC {
    
      @IBOutlet weak var tbl_home: UITableView!
      var musicPlayByTableViewCell: ((Int) -> Void)?
   
      var arr_dailyMix = [PlayListModel]()
    var arr_recentData = [Music]()
      override func viewDidLoad() {
        super.viewDidLoad()
        
        let cell = UINib(nibName: "Cell_home", bundle: nil)
        self.tbl_home.register(cell, forCellReuseIdentifier: "Cell_home")
        
        self.tbl_home.estimatedRowHeight = 100
        self.tbl_home.rowHeight = CGFloat(constantHeightCell)
        
        self.addProfilebutton()
      //  musicPlayByTableViewCell?(0)
        self.navigationItem.title = "Home"
        
        self .getPlayListData()
        arr_dailyMix .append(PlayListModel(title: "Daily mix 1", subtitle:kConstnatLbl.dm1, url: "https://esound.app/images/dailymixes/dailymix1.png", id: "0"))
        
         arr_dailyMix .append(PlayListModel(title:"Daily mix 2", subtitle:kConstnatLbl.dm2, url: "https://esound.app/images/dailymixes/dailymix2.png", id: "1"))
        
         arr_dailyMix .append(PlayListModel(title: "Daily mix 3", subtitle:kConstnatLbl.dm2, url: "https://esound.app/images/dailymixes/dailymix3.png", id: "2"))
     
        
    }
    override func viewWillAppear(_ animated: Bool) {
      
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.arr_recentData = DataManager.initDB().RETRIEVE_Songs(query:recentDataQuery)
            self.tbl_home .reloadData()
        }
        
        let path = Bundle.main.path(forResource: "buffering", ofType: "gif")
          let path1 = Bundle.main.url(forResource: "buffering", withExtension: "gif")
        
    }
    func getPlayListData() {
        
        self.view .showBlurLoader()
        WebserviceHelper.WebapiGetMethod(strurl:"https://api.deezer.com/chart/playlists/playlists", ShowProgress: false) { (isSuccess, dictData) in
            self.view .removeBluerLoader()
            if isSuccess == true {
                if let arrdata = dictData .value(forKey: "data") as? [[String:AnyObject]] {
                    
                    for i in 0 ..< arrdata.count {
                        let dict = arrdata[i]
                        let intid = dict["id"] as? Int64
                        let url  = dict["picture_medium"] as? String ?? ""
                        let objPlayList = PlayListModel(title: dict["title"] as? String, subtitle: "", url: url, id: String(describing: intid!))
                        objAppdelegate.arr_topPlayList .append(objPlayList)
                        
                    }
                    
//                  self.arr_topPlayList = arrdata
                    self.tbl_home .reloadData()
                }
                
            }else{
                HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
            }
        }
    }
   
}
extension HomeVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_home", for: indexPath) as! Cell_home
      
        if indexPath.row == 0 {
            cell.lbl_title.text = "Top playlists"
            cell.lbl_subtitle.text = "Popular playlists and for the moment"
            cell.arr_deezerList = objAppdelegate.arr_topPlayList
        }
        else if indexPath.row == 1 {
            cell.lbl_title.text = "Daily mixes"
            cell.lbl_subtitle.text = "Playlists made for you"
            cell.arr_dailyMix = self.arr_dailyMix
        }
        else if indexPath.row == 2 {
            cell.lbl_title.text = "Recently played"
            cell.lbl_subtitle.text = "Some saved songs that you have recently listened"
            cell.arr_recentList = self.arr_recentData
        }
        cell.tag_identify = indexPath.row
        cell.objParentvc = self
        cell.collectionview_home .reloadData()
        return cell;
        
    }
}

