//
//  AddToPlaylisVC.swift
//  ESound
//
//  Created by kirti on 07.12.19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class AddToPlaylistVC: UIViewController {
    @IBOutlet weak var searchBar_songs: UISearchBar!
        @IBOutlet weak var tbl_addSongs: UITableView!
         var arr_playList = [PlayListModel]()
         var arr_globalPlayList = [PlayListModel]()
         var arr_selected = [PlayListModel]()
         var objMusic = Music()
        @IBOutlet weak var btn_add: UIButton!
        @IBOutlet weak var btn_cancel: UIButton!
        override func viewDidLoad() {
            super.viewDidLoad()
            
            let cell = UINib(nibName: "cell_songslist", bundle: nil)
            self.tbl_addSongs.register(cell, forCellReuseIdentifier: "cell_songslist")
            self.tbl_addSongs.tableFooterView = UIView.init()
              searchBar_songs.placeholder = "Search playlists in your library"
            self.tbl_addSongs.estimatedRowHeight = 55
            self.tbl_addSongs.rowHeight = 55
            
              self.navigationItem.title = kAddtoPlayList
            self.btn_add.setButtonWithGreenColor()
            self.btn_cancel.setButtonWithBorderAndCorner()
            
            if #available(iOS 11.0, *) {
                navigationItem.largeTitleDisplayMode = .never
            }
            self.getPlayList()

        }
        func getPlayList() {
            
            self.arr_playList = HelperClass .getPlaylistArray()
            self.arr_globalPlayList = self.arr_playList
            self.tbl_addSongs .reloadData()
        }
        @IBAction func btnActioncancelClicked(_ sender: UIButton) {
            self.dismiss(animated: true, completion: nil)
        }
        
    @IBAction func btnActionAddClicked(_ sender: UIButton) {
        
        if objMusic.trackUserID.count < 1 {
            HelperClass .SaveTrackData(objMusic: objMusic, showProgress: true) { (sucess, objmusic) in
                if sucess {
                    self .saveData()
                }else {
                     HelperClass.showAlertMessage(message:"Error in save to server")
                }
            }
        }else {
            self .saveData()
        }
   
    }
    func saveData() {
        
        
        if Connectivity.connetivityAvailable() == false {
            HelperClass .showAlertMessage(title: errOfflinetitle, message: errChangedLibraryOffline)
            return
        }
        
        var arrplaylistID = [String]()
        var arrlistPos = [Int]()
        var index = 0
        for obj in arr_selected {
            if obj.arr_songs .contains(where:{ (objc) -> Bool in
                return objc.deezerTrackID == objMusic.deezerTrackID
            }) == false {
                arrplaylistID .append(obj.id)
                arrlistPos .append(obj.arr_songs.count + index)
                index = index + 1
                SyncManager.shared().insertIntoPlaylist(playlistID: obj.id, trackReference:objMusic.trackReference)
            }
        }
        
        let common = [
            "trackUserID":objMusic.trackUserID,
            "pos":"0"] as [String : AnyObject]
        
        let parameter = ["playlistIDs":arrplaylistID,
                         "listPos":arrlistPos,
                         "track":common] as [String : AnyObject]
        
        WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.kinsertMultipleinPlaylist, parameters:parameter, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
            if isSuccess == true {
                self .dismiss(animated: true, completion: nil)
            }else{
                HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
            }
        }
    }
}
    
    extension AddToPlaylistVC:UITableViewDelegate,UITableViewDataSource
    {
         func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.arr_playList.count
         }
        
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cellsong = tableView.dequeueReusableCell(withIdentifier: "cell_songslist", for: indexPath) as! cell_songslist
            cellsong.tintColor = UIColor(hexString:colorGreen)
            let objData = self.arr_playList[indexPath.row]
            cellsong.imgvw_photo.image = UIImage(named: "10")
            cellsong.btn_add.isHidden = true
            cellsong.lbl_title.text = objData.title
            cellsong.lbl_subtitle.text = "\(objData.arr_songs.count)" + " tracks"
            if let strUrl = objData.url {
                cellsong.imgvw_photo .sd_setImage(with:URL(string:strUrl)) { (image, error, cacheType, url) in
                }
            }
             cellsong.accessoryType = .checkmark

            if arr_selected.contains(where: { (objtrackList) -> Bool in
                return objtrackList.id == objData.id
            }) {
              cellsong.accessoryType = .checkmark
            }else {
               cellsong.accessoryType = .none
            }
            return cellsong
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            let objData = self.arr_playList[indexPath.row]
            if arr_selected.contains(where: { (objtrackList) -> Bool in
                return objtrackList.id == objData.id
            }) {
                arr_selected .removeAll { (objtrackList) -> Bool in
                    return objtrackList.id == objData.id
                }
            }else {
                arr_selected .append(self.arr_playList[indexPath.row])
            }
            self.tbl_addSongs .reloadRows(at: [indexPath], with: .none)
        }
        
        
    }
    extension AddToPlaylistVC:UISearchBarDelegate {
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            let arrSong = self.arr_globalPlayList.filter { (objTrackList) -> Bool in
                return objTrackList.title?.lowercased() == searchText.lowercased()
            }
            self.arr_playList = arrSong
            self.tbl_addSongs .reloadData()
        }
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            self.arr_playList = self.arr_globalPlayList
            self.tbl_addSongs .reloadData()
        }
   

}
