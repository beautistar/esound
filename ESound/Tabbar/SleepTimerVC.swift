//
//  SleepTimerVC.swift
//  ESound
//
//  Created by kirti on 15.12.19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

class SleepTimerVC: UIViewController {
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var btn_save: UIButton!
    @IBOutlet weak var btn_Cancel: UIButton!
    var arr_hours = [String]()
    var arr_Minutes = [String]()
    var hourtime = Int()
    var minutetime = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in stride(from: 0, to: 24, by: 1) {
            print(i)
            arr_hours .append("\(i)" + " hours")
        }
        for i in stride(from: 0, to: 60, by: 1) {
            
            arr_Minutes .append("\(i)" + " minutes")
        }
        self.btn_Cancel .setButtonWithBorderAndCorner()
        self.btn_save .setButtonWithGreenColor()
        self.navigationItem.title = kSleepTimer
        

    }
    @IBAction func btnActionCancelClicked(_ sender: Any) {
        self .dismiss(animated: true, completion: nil)
    }
    @IBAction func btnActionSaveClicked(_ sender: UIButton) {
        
        if hourtime > 0  || minutetime > 0 {
            let second  = hourtime > 0 ? hourtime * 60 * 60 : 0 + minutetime * 60
            
           // let date = Calendar.current.date(bySettingHour: hourtime, minute:minutetime, second: 0, of: Date())!
            let object = ["second":Double(second)] as [String : Any]
            NotificationCenter.default.post(name:TimerforSleep, object: nil, userInfo: object)
            self .dismiss(animated: true, completion: nil)
            
        }
        
    }
}
extension SleepTimerVC:UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return self.arr_hours.count
        }else {
            return self.arr_Minutes.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return self.arr_hours[row]
        }else {
            return self.arr_Minutes[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if component == 0 {
            hourtime = row
        }else {
            minutetime = row
        }
       
    }
}
