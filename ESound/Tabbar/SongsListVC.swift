//
//  SongsListVC.swift
//  TestApp
//
//  Created by kirti on 14/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit
import ParallaxHeader
import PKHUD
import SwiftGifOrigin
//import DropDown
class SongsListVC: UITableViewController {

    
    @IBOutlet weak var lbl_header: UILabel!
    @IBOutlet weak var btn_add: UIButton!
     @IBOutlet weak var btn_addSongs: UIButton!
    @IBOutlet weak var btn_shuffle: UIButton!
    @IBOutlet weak var btn_play: UIButton!
    var stretchyHeader: GSKNibStretchyHeaderView!
    var objPlayList:PlayListModel!
    var isFromBrowse = Bool()
    var isFromHomeTab = Bool()
    var isFromDailyMix = Bool()
    var isFromRankings = Bool()
    var isFromTracks = Bool()
    var isFromLibrarySongs = Bool()
    var isFromAlbum = Bool()
    var isFromArtist = Bool()
    var arr_songsList = [Music]()
    var isVisisbleVC : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cell = UINib(nibName: "cell_songslist", bundle: nil)
        self.tableView.register(cell, forCellReuseIdentifier: "cell_songslist")
      
        if isFromLibrarySongs  {
            self.lbl_header.text = "Available offline"
            self.btn_add .setImage(UIImage(named: "selected"), for: .selected)
            self.btn_add .setImage(UIImage(named: "Unselected"), for: .normal)
            let moreButton = UIBarButtonItem(image: UIImage(named: "more"), style: .plain, target: self, action: #selector(btnActionSortClicked(sender:)))
            self.navigationItem.rightBarButtonItem  = moreButton
        }else {
            self.tableView.tableFooterView = UIView.init()
        }
        
        if self.isFromTracks || self.isFromArtist  {
            self.lbl_header.isHidden = true
            self.btn_add.isHidden = true
            let view = tableView.tableHeaderView
            var frame = view?.frame
            frame?.size.height = 70
            view?.frame = frame!
            tableView.tableHeaderView = view    
        }
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = 60
        let navtitle = objPlayList.title
        let nibViews = Bundle.main.loadNibNamed("GSKNibStretchyHeaderView", owner: self, options: nil)! as NSArray
        self.stretchyHeader = nibViews.firstObject as? GSKNibStretchyHeaderView
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            self.stretchyHeader.userNameLabel.text = navtitle
            self.stretchyHeader.userNameLabel.text = self.objPlayList.title
          //  self.stretchyHeader.lbl_description.text = self.objPlayList.subtitle
            self .hideView(ishide: true)
            if let url = self.objPlayList.url {
                self.stretchyHeader.userImage .sd_setImage(with: URL(string:url)) { (image, error, cacheType, url) in
                }
                self.stretchyHeader.backgroundImageView .sd_setImage(with: URL(string:url)) { (image, error, cacheType, url) in
                }
            }
            
            if self.isFromArtist {
                self.stretchyHeader.lbl_description.text = "All your saved tracks by " + (self.objPlayList.title ?? "")
            }
            
        }
        
        NotificationCenter.default.addObserver(forName:ChangeSonagNotification, object: nil, queue: nil) { (objNotification) in
            let obj = objNotification.userInfo
             objAppdelegate.objActiveMusic = obj![kactiveMusic] as? Music
          
            self.tableView .reloadData()
        }
        DispatchQueue.main.async {
             HelperClass .getlocalTrack()
            self.tableView .reloadData()
        }
      
        self.btn_addSongs.titleLabel?.font = UIFont(name: RobotoRegular, size: FontSizeNormal)
        self.btn_addSongs.setTitleColor(UIColor(hexString: colorGreen), for: .normal)

        self.navigationItem.title = navtitle
        self.btn_shuffle .setButtonWithBorderAndCorner()
        self.btn_play .setButtonWithGreenColor()
        lbl_header.font = UIFont(name: RobotoRegular, size: FontSizeNormal)
        self.addBackbutton()
        
        let array = objAppdelegate.dict_TempMemory[objPlayList.id]
        
        if isFromArtist || isFromTracks {
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                self .hideView(ishide: false)
            }
            self.tableView .reloadData()
        }
        else if array != nil  {
            self.arr_songsList = array as! Array<Music>
               self .checkIsLoading()
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                self .hideView(ishide: false)
            }
            self.tableView .reloadData()
            
        }
        else {
            if self.isFromDailyMix {
                self.getDailyMixData()
            }
            else if self.isFromLibrarySongs {
                self .getLibraryData()
                DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                    self.hideView(ishide: false)
                }
            }
            else{
                self.getHomeDetail(playListID: objPlayList.id, isDailyMix: false)
            }
        }
       
        
     
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let url = self.objPlayList.url ,url.count < 1 {
            if arr_songsList.count > 0 {
                if let url = arr_songsList.first?.trackImageUrl {
                    self.stretchyHeader.userImage .sd_setImage(with: URL(string:url)) { (image, error, cacheType, url) in
                    }
                    self.stretchyHeader.backgroundImageView .sd_setImage(with: URL(string:url)) { (image, error, cacheType, url) in
                    }
                }
            }
        }
        
        
        self.isVisisbleVC = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.isVisisbleVC = false
        HUD.hide()
    }
    func hideView(ishide:Bool){
        if self.isFromTracks{
            return
        }
        
        if ishide {
            self.stretchyHeader.isHidden = true
            self.tableView.tableHeaderView?.isHidden = true
        }else {
            self.stretchyHeader.isHidden = false
            self.tableView.tableHeaderView?.isHidden = false
            
            self.tableView.parallaxHeader.view = self.stretchyHeader
            self.tableView.parallaxHeader.height = 260
            self.tableView.parallaxHeader.minimumHeight = 0
            self.tableView.parallaxHeader.mode = .centerFill
            self.tableView.parallaxHeader.parallaxHeaderDidScrollHandler = { parallaxHeader in
            }
        }
       
    }
    func getLibraryData() {
        
        let str_QuerySongs = String(format: "Select s.* from tbl_playlist_songs as pl left join tbl_songs as s on pl.trackReference = s.trackReference  where pl.p_id = '%@'", objPlayList.id)
       let array = DataManager.initDB().RETRIEVE_Songs(query: str_QuerySongs)
      
         self.arr_songsList = array
//        var i  = 0
//        for  objPlay in array {
//            let obj = Music()
//            obj.trackTitle = objPlay.trackTitle
//            obj.trackImageUrl = objPlay.trackImageUrl
//            obj.trackID = objPlay.trackID
//            obj.musictype = .Library
//            obj.musicNum = i
//            obj.deezerTrackID = objPlay.deezerTrackID ?? "0"
//            obj.authorName = objPlay.authorName
//            obj.authorImageUrl = objPlay.authorImageUrl
//            obj.authorID = objPlay.authorID
//            self.arr_songsList .append(obj)
//            i = i+1
//        }
        
        let arrcount = self.arr_songsList.filter { (objmusic) -> Bool in
            return objmusic.isOffline == "1"
        }
        if arrcount.count == self.arr_songsList.count,self.arr_songsList.count > 0 {
            btn_add.isSelected = true
        }else {
             btn_add.isSelected = false
        }
    }
    func checkIsLoading(){
        if objAppdelegate.arr_EntirePlayList.contains(objPlayList.id) {
            self.btn_add .setImage(UIImage(named: "spiner"), for: .normal)
        }
    }
    func getHomeDetail(playListID:String,isDailyMix:Bool) {
        
       self .checkIsLoading()
    
        if isDailyMix == false {
            self.view .showBlurLoader()
        }
        if self.isFromAlbum {
              let strUrl =  "https://api.deezer.com/album/\(playListID)/tracks"
            WebserviceHelper.WebapiGetMethod(strurl:strUrl, ShowProgress:false) { (isSuccess, dictData) in
                self.view .removeBluerLoader()
                if isSuccess == true {
                    if let arrdata = dictData .value(forKey: "data") as? [[String:AnyObject]] {
                        for i in 0 ..< arrdata.count {
                            let dict = arrdata[i]
                            let obj = Music()
                            obj.trackTitle = dict["title"] as? String ?? ""
                            obj.trackImageUrl = self.objPlayList.url
                            obj.musictype = .Deezer
                            obj.musicNum = i
                            obj.deezerTrackID = HelperClass .getValidationstring(dict: dict, key: "id")
                            obj.authorName = dict["artist"]?["name"] as? String ?? ""
                            obj.authorID = HelperClass .getValidationstring(dict: dict["artist"] as! [String:Any], key: "id")
                            self.arr_songsList .append(obj)
                            
                        }
                        objAppdelegate.dict_TempMemory[self.objPlayList.id] = self.arr_songsList
                        self .hideView(ishide: false)
                        self.tableView .reloadData()
                        
                    }
                }else{
                    HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
                }
            }
        }else {
              let strUrl =  "https://api.deezer.com/playlist/\(playListID)/tracks"
            WebserviceHelper.WebapiGetMethod(strurl:strUrl, ShowProgress:false) { (isSuccess, dictData) in
                self.view .removeBluerLoader()
                if isSuccess == true {
                    if var arrdata = dictData .value(forKey: "data") as? [[String:AnyObject]] {
                        var indexCount:Int = 0
                        if isDailyMix {
                            arrdata = arrdata.suffix(30-self.arr_songsList.count)
                            indexCount = self.arr_songsList.count-1
                        }
                        for i in 0 ..< arrdata.count {
                            let dict = arrdata[i]
                            let url  = dict["album"]?["cover_big"] as? String ?? ""
                            let obj = Music()
                            obj.trackTitle = dict["title"] as? String ?? ""
                            obj.trackImageUrl = url
                            obj.musictype = .Deezer
                            obj.musicNum = i + indexCount
                            obj.deezerTrackID = HelperClass .getValidationstring(dict: dict, key: "id")
                            obj.authorName = dict["artist"]?["name"] as? String ?? ""
                            obj.authorImageUrl = dict["artist"]?["picture"] as? String ?? ""
                            obj.authorID = HelperClass .getValidationstring(dict: dict["artist"] as! [String:Any], key: "id")
                            self.arr_songsList .append(obj)
                        }
                        if isDailyMix {
                            // self.arr_songsList.shuffle()
                        }
                        objAppdelegate.dict_TempMemory[self.objPlayList.id] = self.arr_songsList
                        self .hideView(ishide: false)
                        self.tableView .reloadData()
                        
                    }
                }else{
                    HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
                }
            }
        }
        
    }
    func getDailyMixData() {
        self.view .showBlurLoader()
        let strUrl =  kHostPath+kWebApi.kDailyMixTopNSongs + UserDefaults.standard.string(forKey: kUserid)! + "/15"
        WebserviceHelper.postWebServiceCall(urlString: strUrl, parameters: [:], encodingType: DefaultEncoding, ShowProgress: false) {(isSuccess, dictData) in
               self.view .removeBluerLoader()
               if isSuccess == true {
                   if let arrdata = dictData .value(forKey: "data") as? [[String:AnyObject]] {
//                    {
//                               "trackReference": 3657799,
//                               "trackUserID": 97388645,
//                               "trackID": "O7q11IULRII",
//                               "trackTitle": "Scintillations",
//                               "imageUrl": "https://e-cdns-images.dzcdn.net/images/cover/0bf9bb72bbe2a5122da0c8bf287e05f8/1000x1000-000000-80-0-0.jpg",
//                               "authorID": "9334066",
//                               "authorName": "Piano Novel",
//                               "authorImageUrl": "https://e-cdns-images.dzcdn.net/images/artist/bc568307f9a48db15fca3e820b222f04/1000x1000-000000-80-0-0.jpg",
//                               "type": 1,
//                               "deezerTrackID": "674207052",
//                               "isOffline": 1,
//                               "ytMusic": null
//                           },
                    
                       for i in 0 ..< arrdata.count {
                           let dict = arrdata[i]
                        
                           let url  = dict["imageUrl"] as? String ?? ""
                           let authorName  = dict["authorName"] as? String ?? ""
                          
                               let obj = Music()
                               obj.trackTitle = dict["trackTitle"] as? String ?? ""
                               obj.trackImageUrl = url
                               obj.musictype = .Library
                               obj.trackID = dict["trackID"] as? String ?? ""
                               obj.authorID = dict["authorID"] as? String ?? "0"
                        
                               obj.musicNum = i
                               obj.deezerTrackID = dict["deezerTrackID"] as? String ?? "0"
                               obj.authorName = authorName
                              
                               self.arr_songsList .append(obj)
                       }
                    
                    var index = 0;
                    if(self.objPlayList.id == "1"){
                        index = 1
                    }
                    else if(self.objPlayList.id == "2"){
                        index = 2
                    }
                    
                   
                    
                    if(objAppdelegate.arr_topPlayList.count <= index) {
                     //   self.arr_songsList.shuffle()
                        self .hideView(ishide: false)
                        self.tableView .reloadData()
                    }else {
                        let objPlaylistData = objAppdelegate.arr_topPlayList[index]
                        self.getHomeDetail(playListID: objPlaylistData.id, isDailyMix: true)
                    }
                       
                   }
               }else{
                   HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
               }
           }
             
       }
    func saveTrackData(objmodel:Music) {
        HelperClass .saveTrackApi(obj: objmodel, inUserAccount: true , showProgress: true) { (sucess, objmusic) in
            self.tableView .reloadData()
        }
    }
    func gotoOpenPlayer(index:Int) {
        objAppdelegate.objActiveMusic = self.arr_songsList[index]
        let object = ["index":index,"object":self.arr_songsList] as [String : Any]
        NotificationCenter.default.post(name:OpenPlayerNotification, object: nil, userInfo: object)
        self.tableView .reloadData()
    }
    //MARK: UIButton Method
    @objc func btnActionSortClicked(sender:UIBarButtonItem) {
        
        
        let actionsheet = UIAlertController(title: "song name", message: nil, preferredStyle:.actionSheet)
        actionsheet.addAction(UIAlertAction(title: kAddSongs, style: .default, handler: { (action) -> Void in
            self.gotoAddSongsVC()
        }))
        
        actionsheet.addAction(UIAlertAction(title: kSortSongs, style:.default, handler: { (action) -> Void in
            let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "SortSongsVC") as! SortSongsVC
            obj.objPlaylist = self.objPlayList
            obj.arr_sortsongsList = self.arr_songsList
            self.present(obj.setNavigationBarController(), animated: true, completion: nil)
            
        }))
        actionsheet.addAction(UIAlertAction(title: kRenamePlaylist, style: .default, handler: { (action) -> Void in
            // self.navigationItem.title = ""
            let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "AddPlaylistVC") as! AddPlaylistVC
            obj.isEdit = true
            obj.objParentList = self
            obj.fromtype = .Playlist
            obj.objPlayList = self.objPlayList
            self.present(obj.setNavigationBarController(), animated: true, completion: nil)
        }))
        actionsheet.addAction(UIAlertAction(title: kRemovePlaylist, style: .default, handler: { (action) -> Void in
            
            let alertController = UIAlertController(title: "Do you want to delete this playlist ?", message: "Are you sure to want to delete this playlist forever?(It's a lot of time.)", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: .default) { (aciton) in
                let parameters = [
                    "playlistID": self.objPlayList.id,
                    "userID": UserDefaults.standard.string(forKey: kUserid)] as [String : AnyObject]
                WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.kDeletePlaylist, parameters:parameters, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
                    if isSuccess == true {
                        DataManager.initDB().EXECUTE_Query(query:  String(format: "DELETE FROM  tbl_playlist WHERE id = '%@'",self.objPlayList.id))
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
                    }
                }
            }
            let cancelAction = UIAlertAction(title: kCancel, style: .cancel) { (aciton) in
                
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }))
        actionsheet.addAction(UIAlertAction(title: kCancel, style: .cancel, handler: { (action) -> Void in
            
        }))
        self.present(actionsheet, animated: true, completion: nil)
        
    }
    @IBAction func btnActionShuffleClicked(_ sender: UIButton) {
        if self.arr_songsList.count > 0 {
           self .gotoOpenPlayer(index: 0)
           // AudioPlayer.musicRandomPlay()
              
        }
    }
    @IBAction func btnActionPlayClicked(_ sender: UIButton) {
        if self.arr_songsList.count > 0 {
           self .gotoOpenPlayer(index: 0)
        }
        
    }
    @IBAction func btnActionAddClicked(_ sender: UIButton) {
        if isFromLibrarySongs {
            sender.isSelected = !sender.isSelected
            let parameters = [
                "playlistID": objPlayList.id,
                "isOffline": sender.isSelected ? 1:0,
                "userID": UserDefaults.standard.string(forKey: kUserid)!] as [String : AnyObject]
            WebserviceHelper.postWebServiceCall(urlString:kHostPath+kWebApi.kSavePlayListOffline, parameters: parameters, encodingType: DefaultEncoding, ShowProgress: true) { (isSuccess, dictData) in
                HelperClass .getUserPlayList { (sucess) in
                 HelperClass  .getlocalTrack()
                  self.tableView .reloadData()
                }
            }
        }else {
             // deezer entire playlist clicked
            
            if objAppdelegate.arr_EntirePlayList.contains(objPlayList.id) == false {
                HelperClass.SaveAllTrackFrom(album: self.arr_songsList, index: 0) { (sucess, objmusic) in
                    if self.isVisisbleVC == true {
                        self.tableView .reloadData()
                    }
                }
                objAppdelegate.arr_EntirePlayList.append(objPlayList.id)
                
                 self.btn_add .setImage(UIImage(named: "spiner"), for: .normal)
            }
          
            
        }
   
    }
    func gotoAddSongsVC(){
        let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "AddSongsVC") as! AddSongsVC
        obj.strPlayListID = objPlayList.id
        obj.objParentVC = self
        self.present(obj.setNavigationBarController(), animated: true, completion: nil)
    }
    @IBAction func btnActionFooterClicked(_ sender: UIButton) {
        self.gotoAddSongsVC()
    }
     @objc func btnActionPlusMoreClicked(sender:UIButton) {
            
            let point = sender.convert(CGPoint.zero, to: self.tableView)
            let indexPath = self.tableView.indexPathForRow(at: point)
            let objMusic = self.arr_songsList[indexPath!.row]
            
        if objMusic.isOffline == "0" , (objMusic.download_status == "" || objMusic.download_status == "0") {
            
            if Connectivity.connetivityAvailable() == false {
                HelperClass .showAlertMessage(title: errOfflinetitle, message: errOfflineDownload)
                return
            }
            self.saveTrackOffline(objMusic: objMusic)
            objMusic.isOffline = "1"
            objMusic.download_status = "0"
             HelperClass.shared().addObjectinGlobal(objMusic: objMusic) { (sucess) in
                self.tableView .reloadData()
            }
           
            DownloadManager.shared().downloadSong(track: objMusic) { (sucess, objmusic, error) in
                objMusic.download_status = "1"
                 HelperClass.shared().addObjectinGlobal(objMusic: objMusic) { (sucess) in
                    self.tableView .reloadData()
                }
                
            }
        }
        else if objMusic.isOffline == "1" ,objMusic.download_status == "0" {
            // waiting to download
            
        }
        else if objMusic.isOffline.count < 1  {
            HelperClass .SaveTrackData(objMusic: objMusic, showProgress: true) { (sucess, objmusic) in
                self.tableView .reloadData()
            }
        }
            
    //        if isFromTracks || isFromLibrarySongs {
    //
    //            let actionsheet = UIAlertController(title: "", message: nil, preferredStyle:.actionSheet)
    //            if isFromLibrarySongs {
    //                actionsheet.addAction(UIAlertAction(title: kRemove, style: .default, handler: { (action) -> Void in
    //                }))
    //            }
    //            actionsheet.addAction(UIAlertAction(title: kAddtoPlayerQueue, style:.default, handler: { (action) -> Void in
    //            }))
    //            actionsheet.addAction(UIAlertAction(title: kRenameSongs, style: .default, handler: { (action) -> Void in
    //
    //                let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "AddPlaylistVC") as! AddPlaylistVC
    //                obj.fromtype = .Songs
    //                obj.objmusic = objMusic
    //                self.present(obj.setNavigationBarController(), animated: true, completion: nil)
    //
    //            }))
    //
    //            actionsheet.addAction(UIAlertAction(title: kAddtoPlayList, style: .default, handler: { (action) -> Void in
    //                let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "AddToPlaylistVC") as! AddToPlaylistVC
    //                obj.objMusic = objMusic
    //                self.present(obj.setNavigationBarController(), animated: true, completion: nil)
    //            }))
    //
    //            actionsheet.addAction(UIAlertAction(title: kAddtoPlayerQueue, style:.default, handler: { (action) -> Void in
    //            }))
    //
    //            if isFromLibrarySongs {
    //                actionsheet.addAction(UIAlertAction(title: kremoveFromThisPlaylist, style: .default, handler: { (action) -> Void in
    //                    let parameters = ["playlistID": self.objPlayList.id,
    //                                      "trackUserID":objMusic.trackUserID] as [String : AnyObject]
    //                    WebserviceHelper.postWebServiceCall(urlString: kHostPath+kWebApi.kRemoveSongFromPlaylist, parameters:parameters, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
    //                        if isSuccess == true {
    //
    //                        }else{
    //                            HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
    //                        }
    //                    }
    //                }))
    //            }
    //
    //
    //            actionsheet.addAction(UIAlertAction(title: kfixTheSongs, style: .default, handler: { (action) -> Void in
    //            }))
    //            actionsheet.addAction(UIAlertAction(title: kViewArtistPage, style: .default, handler: { (action) -> Void in
    //                let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "ArtistDetailVC") as! ArtistDetailVC
    //                obj.objPlayList = PlayListModel(title: objMusic.authorName, subtitle: "", url: objMusic.authorImageUrl, id: objMusic.authorID ?? "")
    //                self.navigationController?.pushViewController(obj, animated: true)
    //            }))
    //            actionsheet.addAction(UIAlertAction(title: kCancel, style: .cancel, handler: { (action) -> Void in
    //            }))
    //            self .present(actionsheet, animated: true, completion: nil)
    //        }else {
               
                
               
                
           // }
            
            
        }
    
       

}
extension SongsListVC
{
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_songsList.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_songslist", for: indexPath) as! cell_songslist
        let objData = self.arr_songsList[indexPath.row]
        cell.lbl_title.text =  objData.trackTitle
        cell.lbl_subtitle.text = objData.authorName
        cell.btn_add .addTarget(self, action: #selector(btnActionPlusMoreClicked(sender:)), for: .touchUpInside)
        self.arr_songsList[indexPath.row] = self.setButtomImage(objModel: objData, btn: cell.imgvw_status)
        if let strUrl = objData.trackImageUrl {
            cell.imgvw_photo.sd_setImage(with:URL(string:strUrl)) { (image, error, cacheType, url) in
            }
        }
        self.setTextColor(objModel: objData, label:cell.lbl_title)
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self .gotoOpenPlayer(index: indexPath.row)
        self.tableView .reloadRows(at: [indexPath], with: .none)
        
        
    }
   
}
