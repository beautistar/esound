//
//  TracksListVC.swift
//  ESound
//
//  Created by kirti on 15/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit
//import DropDown






class TracksListVC: UIViewController {
   
    @IBOutlet weak var btn_play: UIButton!
    @IBOutlet weak var btn_shuffle: UIButton!
     @IBOutlet weak var btn_addPlaylist: UIButton!
     @IBOutlet weak var tbl_tracks: UITableView!
    var tagIdentify = Int()
    var arr_playList = [PlayListModel]()
    var arr_artistList = [PlayListModel]()
    var arr_albumsList = [PlayListModel]()
    var arr_trackList = [Music]()
    var isFromLibraryPage = Bool()
    var isFromDeezerArtist = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
      
        let cell = UINib(nibName: "cell_songslist", bundle: nil)
        self.tbl_tracks.register(cell, forCellReuseIdentifier: "cell_songslist")
        
        self.btn_shuffle .setButtonWithBorderAndCorner()
        self.btn_play .setButtonWithGreenColor()
        
         self.btn_addPlaylist.titleLabel?.font = UIFont(name: RobotoRegular, size: FontSizeNormal)
         self.btn_addPlaylist .setTitleColor(UIColor(hexString: colorGreen), for: .normal)
        
        self.tbl_tracks.estimatedRowHeight = 60
        self.tbl_tracks.rowHeight = 60
        
       if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        
        let menuButton = UIBarButtonItem(image: UIImage(named: "filter"), style: .plain, target: self, action: #selector(btnActionSortClicked(_:)))
        self.navigationItem.rightBarButtonItem  = menuButton
        if self.isFromLibraryPage || self.isFromDeezerArtist {
            self.navigationItem.rightBarButtonItem = nil
        }
       
        self.addBackbutton()
     
    }
  
    override func viewWillAppear(_ animated: Bool) {
         self .getNavigationtitle()
    }
    func getNavigationtitle() {
        if tagIdentify == tag_Tracks {
            self.tbl_tracks.tableFooterView = UIView.init()
            self.navigationItem.title = "Tracks"
        }
        else if tagIdentify == tag_Playlits {
            self.tbl_tracks.tableHeaderView = UIView.init()
            self.navigationItem.title = "Playlists"
            //  self .getPlayList()
        }
        else if tagIdentify == tag_Artists {
            self.tbl_tracks.tableHeaderView = UIView.init()
            self.tbl_tracks.tableFooterView = UIView.init()
            self.navigationItem.title = "Artists"
        }
        else if tagIdentify == tag_albums {
            self.tbl_tracks.tableHeaderView = UIView.init()
            self.tbl_tracks.tableFooterView = UIView.init()
            self.navigationItem.title = "All albums"
        }
        else if tagIdentify == tag_Recents {
            self.tbl_tracks.tableFooterView = UIView.init()
            self.navigationItem.title = "Recent tracks"
        }
        else if tagIdentify == tag_Favourites {
            self.tbl_tracks.tableFooterView = UIView.init()
            self.navigationItem.title = "Favourite tracks"
        }
        else if tagIdentify == tag_FromSearchSongs {
            self.tbl_tracks.tableFooterView = UIView.init()
            self.tbl_tracks.tableHeaderView = UIView.init()
            self.navigationItem.title = "All tracks"
            
        }
    }
    @objc func btnActionSortClicked(_ sender: UIBarButtonItem) {

        if tagIdentify == tag_Playlits {
            let actionsheet = UIAlertController(title: "PlayList", message: nil, preferredStyle:.actionSheet)
            actionsheet.addAction(UIAlertAction(title: kAlphabetically, style: .default, handler: { (action) -> Void in
                self.arr_playList.sort { (obj1, obj2) -> Bool in
                    return (obj1.title!.localizedCaseInsensitiveCompare(obj2.title!) == .orderedAscending)
                }
                self.tbl_tracks .reloadData()
            }))
            
            actionsheet.addAction(UIAlertAction(title: kByTracksCount, style:.default, handler: { (action) -> Void in
                self.arr_playList.sort { (obj1, obj2) -> Bool in
                    return obj1.arr_songs.count > obj2.arr_songs.count
                }
                self.tbl_tracks .reloadData()
            }))
            actionsheet.addAction(UIAlertAction(title: kFromnewest, style: .default, handler: { (action) -> Void in
                self.arr_playList.sort { (obj1, obj2) -> Bool in
                    return Int(obj1.id) ?? 0  > Int(obj2.id) ?? 0
                }
                self.tbl_tracks .reloadData()
            }))
            actionsheet.addAction(UIAlertAction(title: kDefault, style: .default, handler: { (action) -> Void in
                self.arr_playList.sort { (obj1, obj2) -> Bool in
                    return Int(obj1.id) ?? 0  < Int(obj2.id) ?? 0
                }
                self.tbl_tracks .reloadData()
            }))
            actionsheet.addAction(UIAlertAction(title:kCancel, style: .cancel, handler: { (action) -> Void in
                
            }))
            self.present(actionsheet, animated: true, completion: nil)
        }

    }
    @IBAction func btnActioShuffleClicked(_ sender: UIButton) {
        
    }
    @IBAction func btnActionPlayClicked(_ sender: UIButton) {
        
    }
    @IBAction func btnActionAddPlayListClicked(_ sender: UIButton) {
        let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "AddPlaylistVC") as! AddPlaylistVC
        obj.fromtype = .Playlist
        self.present(obj.setNavigationBarController(), animated: true, completion: nil)
    }
   @objc func btnActionAddClicked(_ sender: UIButton) {
    
    let buttonPosition = sender.convert(CGPoint.zero, to: self.tbl_tracks)
    let indexPath = self.tbl_tracks.indexPathForRow(at:buttonPosition)
    

    if tagIdentify == tag_Playlits {
    
    }
    else if tagIdentify == tag_Tracks {
       
        
        
    }
    
    }

   

}
extension TracksListVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tagIdentify == tag_Playlits {
            return self.arr_playList.count
        }
        else if tagIdentify == tag_Tracks {
            return self.arr_trackList.count
        }
        else if tagIdentify == tag_Artists {
            return self.arr_artistList.count
        }
        else if tagIdentify == tag_albums {
            return self.arr_albumsList.count
        }
        else{
            return 10
        }
      
       // https://api.deezer.com/album/107206282/tracks
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellsong = tableView.dequeueReusableCell(withIdentifier: "cell_songslist", for: indexPath) as! cell_songslist
        cellsong.imgvw_photo.image = UIImage(named: "10")
        cellsong.lbl_title.text = "Walking By"
        cellsong.lbl_subtitle.text = "Felix Cartal"
        cellsong.btn_add .addTarget(self, action: #selector(btnActionAddClicked(_:)), for: .touchUpInside)
        if tagIdentify == tag_Playlits {
        
        }
        if tagIdentify == tag_Artists {
            let obj = self.arr_artistList [indexPath.row]
            cellsong.lbl_title.text = obj.title
            cellsong.btn_add .setImage(UIImage(named: "right-arrow"), for: .normal)
            if self.isFromLibraryPage {
                cellsong.lbl_subtitle.text = (obj.subtitle ?? "") + " tracks"
            }else {
                cellsong.lbl_subtitle.text = obj.subtitle
            }
           
            cellsong.btn_add.imageView?.contentMode = .scaleAspectFit
            if let strUrl = obj.url {
                cellsong.imgvw_photo .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
                }
            }
        }
        else if tagIdentify == tag_Playlits {
             cellsong.btn_add .setImage(UIImage(named: "more"), for: .normal)
            let obj = self.arr_playList[indexPath.row]
            cellsong.lbl_title.text = obj.title
            cellsong.lbl_subtitle.text = "\(obj.arr_songs.count)" + " tracks"
            cellsong.imgvw_photo.image = UIImage(named: "emptyPlaceholder")
            if let strUrl = obj.url {
                cellsong.imgvw_photo .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
                }
            }
            
        }
        else if tagIdentify == tag_Tracks {
             cellsong.btn_add .setImage(UIImage(named: "more"), for: .normal)
            let obj = self.arr_trackList [indexPath.row]
            cellsong.lbl_title.text = obj.trackTitle
            cellsong.lbl_subtitle.text = obj.authorName
            if let strUrl = obj.trackImageUrl {
                cellsong.imgvw_photo .sd_setImage(with: URL(string: strUrl)) { (image, error, cacheType, url) in
                }
            }
        }
        else if tagIdentify == tag_albums {
            cellsong.btn_add .setImage(UIImage(named: "right-arrow"), for: .normal)
            let obj = self.arr_albumsList [indexPath.row]
            cellsong.lbl_title.text = obj.title
            cellsong.lbl_subtitle.text = obj.subtitle
            if let strUrl = obj.url {
                cellsong.imgvw_photo .sd_setImage(with:URL(string: strUrl)) { (image, error, cacheType, url) in
                }
            }
        }

        
        return cellsong
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
        if tagIdentify == tag_Artists {
            let objModel = arr_artistList[indexPath.row]
            if self.isFromLibraryPage {
                let str_QuerySongs = String(format:"SELECT *FROM tbl_songs WHERE authorID = '%@'",objModel.id)
                let array = DataManager.initDB().RETRIEVE_Songs(query: str_QuerySongs)
                let obj = self.tabbarStoryBoard().instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
                obj.objPlayList = objModel
                obj.arr_songsList = array
                obj.isFromArtist = true
                self.navigationController?.pushViewController(obj, animated: true)
            }else {
                let obj = self.libraryStoryBoard().instantiateViewController(withIdentifier: "ArtistDetailVC") as! ArtistDetailVC
                obj.objPlayList = objModel
                self.navigationController?.pushViewController(obj, animated: true)
            }
            
        }
        else  if tagIdentify == tag_Playlits {
            let objModel = self.arr_playList[indexPath.row]
            let obj = self.tabbarStoryBoard().instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
            obj.objPlayList = objModel
            obj.isFromLibrarySongs = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else  if tagIdentify == tag_albums {
            let objModel = self.arr_albumsList[indexPath.row]
            let obj = self.tabbarStoryBoard().instantiateViewController(withIdentifier: "SongsListVC") as! SongsListVC
            obj.objPlayList = objModel
            obj.isFromAlbum = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
}
