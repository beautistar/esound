//
//  AddPlaylistVC.swift
//  ESound
//
//  Created by kirti on 15/09/19.
//  Copyright © 2019 kirti. All rights reserved.
//

import UIKit

enum FromPageType {
    case Playlist
    case Songs
    
}

class AddPlaylistVC: UIViewController {
    
  
    @IBOutlet weak var btn_create: UIButton!
    @IBOutlet weak var btn_cancel: UIButton!
    @IBOutlet weak var text_BioName: UITextField!
    @IBOutlet weak var text_PlaylistName: UITextField!
    var isEdit = Bool()
    var objPlayList:PlayListModel!
    var objmusic = Music()
    var fromtype:FromPageType?
    var objParentList:SongsListVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if fromtype == .Playlist {
            if isEdit {
                self.navigationItem.title = "Edit Playlist"
                self.btn_create .setTitle("Save", for: .normal)
            }else {
                self.navigationItem.title = "Create Playlist"
            }
        }else {
            self.navigationItem.title = "Rename song"
            text_PlaylistName.placeholder = "Song title"
            text_BioName.placeholder = "Artist name"
            text_PlaylistName.text = objmusic.trackTitle
            text_BioName.text = objmusic.authorName
            
             self.btn_create .setTitle("Save", for: .normal)
            
        }
      
        
        self.btn_create.setButtonWithGreenColor()
        self.btn_cancel.setButtonWithBorderAndCorner()
        
        self.text_BioName .addButtonWithCornerBorder()
        self.text_PlaylistName .addButtonWithCornerBorder()
        
        self.text_BioName .Leftview()
        self.text_PlaylistName .Leftview()
        
        if self.isEdit {
            self.text_PlaylistName.text = objPlayList.title
            self.text_BioName.text = objPlayList.subtitle
        }
        
        
        //self.addBackbutton()
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        
    }
    @IBAction func btnActionCancelClicked(_ sender: UIButton) {
       self .dismiss(animated: true, completion: nil)
    }
    @IBAction func btnActionCreateClicked(_ sender: UIButton) {
        
//        {
//          "title": "string",
//          "description": "string",
//          "userID": 0
//        }
        if fromtype == .Playlist{
            if (text_PlaylistName.text?.trim().count)! < 1 {
                HelperClass .showAlertMessage(message:"Please provide a cool name for your playlist")
            }else {
                self .submitApicall()
            }
            
        }else {
            if (text_PlaylistName.text?.trim().count)! < 1 {
                HelperClass .showAlertMessage(message:"Please provide song name")
            }
            else if (text_BioName.text?.trim().count)! < 1 {
                HelperClass .showAlertMessage(message:"Please provide artist name")
            }else {
                self .submitApicall()
            }
        }
        
       
//        else if (self.text_BioName.text?.trim().count)! < 1 {
//            HelperClass .showAlertMessage(message:"Enter password")
//        }
        
             
    }
    func submitApicall() {
        
        if Connectivity.connetivityAvailable() == false {
            HelperClass .showAlertMessage(title: errOfflinetitle, message: errChangedLibraryOffline)
            return
        }
        
         var strUrl = ""
        var parameters = [
            "title": text_PlaylistName.text!,
            "userID": UserDefaults.standard.string(forKey: kUserid),
            "description": text_BioName.text!] as [String : AnyObject]
        
        if fromtype == .Playlist {
            strUrl = kHostPath+kWebApi.kCreatePlaylist
            if isEdit {
                strUrl = kHostPath+kWebApi.kRenamePlaylist + objPlayList.id
            }
        }else {
            parameters = [
                "title": text_PlaylistName.text!,
                "userID": UserDefaults.standard.string(forKey: kUserid),
                "trackUserID":objmusic.trackUserID,
                "trackID": objmusic.trackID,
                "authorName": text_BioName.text!] as [String : AnyObject]
            strUrl = kHostPath+kWebApi.kRenameSonag
            
        }
       
        WebserviceHelper.postWebServiceCall(urlString: strUrl, parameters:parameters, encodingType: DefaultEncoding,ShowProgress:true) { (isSuccess, dictData) in
            if isSuccess == true {
                if self.fromtype == .Playlist,self.isEdit == true, self.objParentList != nil  {
                    self.objParentList.navigationItem.title = self.text_PlaylistName.text!
                    self.objParentList.stretchyHeader.userNameLabel.text = self.text_PlaylistName.text!
                }
                
                self .dismiss(animated: true, completion: nil)
            }else{
                HelperClass.showAlertMessage(message:dictData["data"] as? String ?? "")
            }
        }
    }

   

}
