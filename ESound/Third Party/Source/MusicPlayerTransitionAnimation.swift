//
//  MusicPlayerTransitionAnimation.swift
//  MusicPlayerTransition
//
//  Created by xxxAIRINxxx on 2016/11/05.
//  Copyright © 2016 xxxAIRINxxx. All rights reserved.
//

import Foundation
import UIKit

final class MusicPlayerTransitionAnimation : TransitionAnimatable {
    
    fileprivate weak var rootVC: MainMusicViewController!
    fileprivate weak var modalVC: MusicPlayerVC!
    
    var completion: ((Bool) -> Void)?
    
    private var miniPlayerStartFrame: CGRect
    private var tabBarStartFrame: CGRect
    
    private var containerView: UIView?
    
    deinit {
        print("deinit MusicPlayerTransitionAnimation")
    }
    
    init(rootVC: MainMusicViewController, modalVC: MusicPlayerVC) {
        
        self.rootVC = rootVC
        self.modalVC = modalVC
        
        self.miniPlayerStartFrame = rootVC.miniPlayerView.frame
        self.tabBarStartFrame = rootVC.tabBar.frame
    }
    
    // @see : http://stackoverflow.com/questions/25588617/ios-8-screen-blank-after-dismissing-view-controller-with-custom-presentation
    func prepareContainer(_ transitionType: TransitionType, containerView: UIView, from fromVC: UIViewController, to toVC: UIViewController) {
        self.containerView = containerView
       
        
        if transitionType.isPresenting {
          
            self.rootVC.view.insertSubview(self.modalVC.view, belowSubview: self.rootVC.tabBar)
        } else {
           
            self.rootVC.view.insertSubview(self.modalVC.view, belowSubview: self.rootVC.tabBar)
        }
        
        self.rootVC.view.setNeedsLayout()
        self.rootVC.view.layoutIfNeeded()
        self.modalVC.view.setNeedsLayout()
        self.modalVC.view.layoutIfNeeded()
    
        
        self.miniPlayerStartFrame = self.rootVC.miniPlayerView.frame
        self.tabBarStartFrame = self.rootVC.tabBar.frame
    }
    
    func willAnimation(_ transitionType: TransitionType, containerView: UIView) {
          self.rootVC.isDraging = true
    
        if transitionType.isPresenting {
          
            self.rootVC.beginAppearanceTransition(true, animated: false)
            self.modalVC.view.frame.origin.y = self.rootVC.miniPlayerView.frame.origin.y + self.rootVC.miniPlayerView.frame.size.height
        } else {
          
            self.rootVC.beginAppearanceTransition(false, animated: false)
            self.rootVC.miniPlayerView.frame.origin.y = -self.rootVC.miniPlayerView.bounds.size.height
             print("will mini y:",self.rootVC.miniPlayerView.frame.origin.y)
            self.rootVC.tabBar.frame.origin.y = containerView.bounds.size.height
              print("will y:",self.rootVC.tabBar.frame.origin.y)
        }
    }
    
    func updateAnimation(_ transitionType: TransitionType, percentComplete: CGFloat) {
        
         self.rootVC.isDraging = true
  
        if transitionType.isPresenting {
            
            // miniPlayerView
            let startOriginY = self.miniPlayerStartFrame.origin.y
            let endOriginY = -self.miniPlayerStartFrame.size.height
            let diff = -endOriginY + startOriginY
            // tabBar
            let tabStartOriginY = self.tabBarStartFrame.origin.y
            let tabEndOriginY = self.modalVC.view.frame.size.height
            let tabDiff = tabEndOriginY - tabStartOriginY
            
            let playerY = startOriginY - (diff * percentComplete)
            self.rootVC.miniPlayerView.frame.origin.y = max(min(playerY, self.miniPlayerStartFrame.origin.y), endOriginY)
            var modelFrame = self.modalVC.view.frame
            modelFrame.origin.y =  self.rootVC.miniPlayerView.frame.origin.y + self.rootVC.miniPlayerView.frame.size.height
            self.modalVC.view.frame = modelFrame
            print("frame",self.modalVC.view.frame)
            let tabY = tabStartOriginY + (tabDiff * percentComplete)
            self.rootVC.tabBar.frame.origin.y = min(max(tabY, self.tabBarStartFrame.origin.y), tabEndOriginY)
            
            if percentComplete > 0.95 {
                 self.rootVC.tabBar.alpha = 0.0
            }else {
                 self.rootVC.tabBar.alpha = 1.0
            }
         

        } else {
           
               print("y:",self.rootVC.tabBar.frame.origin.y)
            // miniPlayerView
            let startOriginY = 0 - self.rootVC.miniPlayerView.bounds.size.height
            let endOriginY = self.miniPlayerStartFrame.origin.y
            let diff = -startOriginY + endOriginY
            // tabBar

            let tabStartOriginY = self.tabBarStartFrame.origin.y
            let tabEndOriginY = self.modalVC.view.frame.size.height
            let tabDiff = tabEndOriginY - tabStartOriginY
            
            self.rootVC.miniPlayerView.frame.origin.y = startOriginY + (diff * percentComplete)
               print("Tab mini y:",self.rootVC.miniPlayerView.frame.origin.y ,diff,percentComplete)
            
        //   self.modalVC.view.translatesAutoresizingMaskIntoConstraints = false
            var modelFrame = self.modalVC.view.frame
          //  modelFrame.size.height = UIScreen.main.bounds.height
            modelFrame.origin.y = self.rootVC.miniPlayerView.frame.origin.y + self.rootVC.miniPlayerView.frame.size.height
            self.modalVC.view.frame = modelFrame
          //  self.modalVC.view.translatesAutoresizingMaskIntoConstraints = true
            
            if percentComplete < 0.2 {
                 self.rootVC.tabBar.alpha = 0.0
            }else {
                 self.rootVC.tabBar.alpha = 1.0
            }
            
            let tabY = tabStartOriginY + (tabDiff *  (1.0 - percentComplete))
               print("Tab y:",tabY,tabDiff,percentComplete)
            
            self.rootVC.tabBar.frame.origin.y = min(max(tabY, self.tabBarStartFrame.origin.y), tabEndOriginY)
        
            print("y:",self.rootVC.tabBar.frame.origin.y)

        }
    }
    
    func finishAnimation(_ transitionType: TransitionType, didComplete: Bool) {
         self.rootVC.endAppearanceTransition()
         self.rootVC.isDraging = false
        
     
        if transitionType.isPresenting {
            
            if didComplete {
                self.modalVC.view.removeFromSuperview()
                self.containerView?.addSubview(self.modalVC.view)
                self.completion?(transitionType.isPresenting)
            } else {
                self.rootVC.beginAppearanceTransition(true, animated: false)
                self.rootVC.endAppearanceTransition()
            }
              print("finish y:",self.rootVC.tabBar.frame.origin.y)
        } else {
            
            if didComplete {
               // self.modalVC.tbl_musicPlayer .setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                self.modalVC.view.removeFromSuperview()
                self.completion?(transitionType.isPresenting)
                
                
            } else {
                
                self.modalVC.view.removeFromSuperview()
                self.containerView?.addSubview(self.modalVC.view)
                self.rootVC.beginAppearanceTransition(false, animated: false)
                self.rootVC.endAppearanceTransition()
            }
        }
    }
}

extension MusicPlayerTransitionAnimation {
    
    func sourceVC() -> UIViewController { return self.rootVC }
    
    func destVC() -> UIViewController { return self.modalVC }
}


